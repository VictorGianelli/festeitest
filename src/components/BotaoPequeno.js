import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity
} from 'react-native';

export default class Botao extends Component {
  
  constructor(props){
    super(props);
    this.state = {};

    this.styles = StyleSheet.create({
      botao:{
        width: 30,
        height: 30,
        borderWidth: 2,
        borderColor: props.bdColor,
        borderRadius: 15,
        backgroundColor: props.bgColor,
        margin: 15,
      },
      btnArea:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
      },
      btnTexto:{
        fontSize: 18,
        //fontWeight: 'bold',
        color: props.cor,
        width: 280,
        textAlign: 'center'
      }
    });
  }

  render() {

    return (
    <View style = {this.styles.botao} >
      <View style = {this.styles.btnArea}>
      
        <Text style = {this.styles.btnTexto} 
         placeholder= "+"// placeholder= {this.props.escrito}
        placeholderTextColor="#fff"
        underlineColorAndroid="transparent"></Text>
      
      </View>
    </View>
    );
  }
}