import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    ImageBackground,
    Button,
    StatusBar,
    View,
    ToastAndroid
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Botao from './Botao';
import BotaoComTexto from './BotaoComTexto';
import BotaoPequeno from './BotaoPequeno';
import BotaoTeste from './BotaoTeste';

export default class Login extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            eventos: this.props.data
        };

        this.total= 0;
        this.mais = this.mais.bind(this);
        this.menos = this.menos.bind(this);
        // this.recalcular = this.recalcular.bind(this);
    }

    mais() {
        let state = this.state;
        this.state.eventos.numero += 1;

        var value = (this.state.eventos.preco + this.state.eventos.taxa) * this.state.eventos.numero;

        // for (var i = 0; i < this.state.eventos.length; i++) {
        //     total += (this.state.eventos[i].preco + this.state.eventos[i].taxa) * this.state.eventos[i].numero;
        // }
        this.state.total = value.toFixed(2);
        this.state.total = this.state.total.replace(/\./g, ',');
        // alert(this.state.total+" "+this.state.eventos.numero);

        this.setState(state);
        // { this.recalcular() };
    }

    menos() {
        let state = this.state;
        if (this.state.eventos.numero > 0) {
            this.state.eventos.numero -= 1;

            var value = (this.state.eventos.preco + this.state.eventos.taxa) * this.state.eventos.numero;

            // for (var i = 0; i < this.state.eventos.length; i++) {
            //     total += (this.state.eventos[i].preco + this.state.eventos[i].taxa) * this.state.eventos[i].numero;
            // }
            this.state.total = value.toFixed(2);
            this.state.total = this.state.total.replace(/\./g, ',');
            // alert(this.state.total+" "+this.state.eventos.numero);

            this.setState(state);
            // { this.recalcular() };
        }
    }

    // recalcular() {
    //     let state = this.state;

    //     var total = 0;
    //     for (var i = 0; i < this.state.eventos.length; i++) {
    //         total += (this.state.eventos[i].preco + this.state.eventos[i].taxa) * this.state.eventos[i].numero;
    //     }
    //     // alert("ingresso");
    //     this.state.total = total.toFixed(2);
    //     this.state.total = this.state.total.replace(/\./g, ',');
    //     this.setState(state);
    // };

    ////////////////////////////////////////////
    render() {

        // let state = this.state;

        // this.state.eventos.total = this.state.eventos.numero * (this.state.eventos.preco + this.state.eventos.taxa);
        // this.state.eventos.total = this.state.eventos.total.toFixed(2);
        // this.state.eventos.total = this.state.eventos.total.replace(/\./g, ',');

        // this.setState(state);

        // const { navigate } = this.props.navigation
        return (
            <View style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0 }}>
                {/* Parte de cima */}
                <View style={{ borderWidth: this.state.eventos.border, borderColor: '#FF9D00', borderBottomLeftRadius: 17.23, borderBottomRightRadius: 15 }}>
                    <Image source={require('../img/fundo_tela_evento.png')}
                        style={styles.fotoRedonda} />
                    <Image source={require('../img/filtro_background.png')}
                        style={styles.filtroRedondo} />
                    <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={styles.textoDia}>22</Text>
                            <Text style={styles.textoBarra}>___</Text>
                            <Text style={styles.textoMes}>JUN</Text>
                        </View>
                        <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                            <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                            <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                            <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>
                        </View>
                    </View>
                </View>
                {/* Parte de baixo */}
                <View style={{ borderWidth: this.state.eventos.border, borderColor: '#FF9D00', padding: 20, height: 150, width: 300, backgroundColor: '#262626', borderRadius: 15, flexDirection: 'row' }}>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100 }}>
                        <Text style={{ color: '#fff', fontFamily: "SFUIText-Bold", fontSize: 14 }}>{this.state.eventos.tipo}</Text>
                        <Text style={{ color: '#fff', fontFamily: "SFUIText-Light", fontSize: 16, paddingTop: 10 }}>R$ 50,00 (+2,50 taxa)</Text>
                        <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Bold", fontSize: 14 }}>Pague em até 12x</Text>
                        <View style={{ justifyContent: 'flex-end', }}>
                            <Text style={{ color: '#fff', fontFamily: "SFUIText-LightItalic", fontSize: 14, paddingTop: 20 }}>Vendas até às 23h</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <BotaoTeste cor="#fff" bdColor="#4c4c4c"
                                bgColor="transparent" nome="+"
                                onPress={this.mais}//() => navigate('NovaConta', { name: 'NovaConta' })
                            />
                            <Text style={{ color: '#fff', fontSize: 20, fontFamily: "SFUIDisplay-Bold", marginRight: 14 }}>
                                {this.state.eventos.numero}
                            </Text>
                            <BotaoTeste cor="#fff" bdColor="#4c4c4c"
                                bgColor="transparent" nome="-"
                                onPress={this.menos}//() => navigate('NovaConta', { name: 'NovaConta' })
                            />
                        </View>
                    </View>
                </View>

                <View>
                    <Text style={{ color: '#fff', fontSize: 50, marginVertical: -189 }}>- - - - - - - - - -</Text>
                </View>
                <View style={{ height: 50, width: 300 }}></View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#1c1c1c',
        color: '#fff'
    },
    margem: {
        flex: 1
    },
    switchView: {
        width: 350,
        height: 450
    },
    texto: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        marginStart: 50,
        textAlign: "left",
    },
    textoTotal: {
        color: '#fff',
        fontSize: 18,
        marginStart: 20,
        textAlign: "left",
        fontFamily: "SFuidisplay-medium"

    },
    textoValor: {
        color: '#F15A24',
        fontSize: 35,
        marginStart: 20,
        textAlign: "left",
        fontFamily: "SFuidisplay-medium"
    },
    textoGrande: {
        color: '#fff',
        // fontFamily: 'rockwell extra bold',
        // fontSize: 50,
        textAlign: "left",
        // marginStart: 50,
        // marginEnd: 50,
        fontWeight: 'bold',
    },
    backgroundImage: {
        width: '100%',
        flex: 1,
        resizeMode: 'stretch',
        //backgroundColor: 'rgba(0, 0, 0, 1.0)', 
    },
    fotoRedonda: {
        height: 533.6,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 17.23,

        marginTop: -333.6,
        // marginVertical: 20,
        // borderWidth: 2,borderColor: '#FF9D00'
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    filtroRedondo: {
        height: 533.6,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -533.6,
        // marginVertical: 20,

        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },

    textoMes: {
        color: '#fff',
        fontFamily: "SFuidisplay-Black",
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoBarra: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: "SFuidisplay-Medium",
        fontSize: 40,
        textAlign: "center",
        fontWeight: 'bold',
        marginBottom: -35,
        justifyContent: 'center',
    },
    textoNomeEvento: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
    },
    textoLocalizacao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        textAlign: "left",
    },
    textoCidade: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
    },

});

