import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  BackHandler,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  TouchableHighlight,
  View
} from 'react-native';

export default class Lista extends Component {
  
  toBio() {
    this.props.navigation.navigate('BioEvento', { name: 'BioEvento' })
    // this.props.navigator.push({
    //   component: BioEvento
    // });
  }

  constructor(props) {
    super(props);
    this.state = {
      feed: this.props.data,
    };
  }

  carregaIcone(like) {
    // alert("Mais 1 voto");
    return like ? require('../img/coracao_vazio_white.png') :
      require('../img/coracao_cheio.png');
  }

  toLike = () => {
    let state = this.state;

    if (state.feed.like == true) {
      state.feed.like = false;
      // alert("A");
    } else {
      state.feed.like = true;
      // alert("B");
    }
    // alert("B");
    this.setState(state);
    // return like ? require('../img/coracao_vazio_white.png') : 
    // require('../img/coracao_vazio_black.png');
  }

  render() {
    return (
      <View style={styles.container}>
      {/* onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  */}
        <TouchableOpacity style={{ height: 210, width: 170 }} onPress={() => this.props.navigation.navigate('AceitarTermos', { name: 'AceitarTermos' })} >
          <View style={{ paddingHorizontal: 20, marginStart: 0, height: 210, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
            <View style={{ marginTop: -10, }} />
            <View>
              <Image source={require('../img/fundo_tela_1.png')}
                style={styles.fotoRedondaVert} />
              <Image source={require('../img/filtro_background.png')}
                style={styles.filtroRedondoVert} />

            </View>

            <View style={{ marginTop: -30, flexDirection: 'row', marginTop: -200 }} >
              <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={styles.textoDia}>{(this.state.feed.data)}</Text>
                <Text style={styles.textoData}>_____</Text>
                <Text style={styles.textoData}>{(this.state.feed.mes)}</Text>
              </View>

              <View style={{ width: 40 }} />
              <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                <TouchableHighlight onPress={this.toLike} underlayColor="transparent">
                  <Image
                    style={styles.fotoMenu} source={this.carregaIcone(this.state.feed.like)} />

                </TouchableHighlight>

              </View>
            </View>

            <View style={{ height: 65 }} />

            <View style={{ width: 170, }}>

              <Text style={styles.textoNomeEvento}>{(this.state.feed.nomeEvento)}</Text>
              <Text style={styles.textoLocalizacao}>{(this.state.feed.localizacao)}</Text>
              <Text style={styles.textoCidade}>{(this.state.feed.cidade)}</Text>

            </View>

          </View>
        </TouchableOpacity>


      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    //padding: 20,
    flex: 1,
    // justifyContent: 'space-between',
    // paddingHorizontal: 15,
    // paddingTop: 48,
    // paddingBottom: 20,color: '#fff'
    // alignItems: 'center',
    backgroundColor: '#1c1c1c', //#026
  },
  textoNomeEvento: {
    marginStart: 10,
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 20,
    textAlign: "left",
  },
  textoLocalizacao: {
    marginStart: 10,
    color: '#fff',
    fontFamily: 'arial',
    fontSize: 10,
    textAlign: "left",
  },
  textoCidade: {
    marginStart: 10,
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 10,
    textAlign: "left",
    fontWeight: 'bold',
  },
  textoData: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 18,
    textAlign: "left",
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  textoDia: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 26,
    textAlign: "left",
    fontWeight: 'bold',
    marginBottom: -15,
    justifyContent: 'flex-start',
  },
  foto: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginRight: -25,
    marginVertical: 20,
    // elevation: 10,
    // shadowColor: '#202020',
    // shadowOffset: { width: 10, height: 10 },
    // shadowRadius: 40,
  },
  fotoRedondaVert: {
    height: 200,
    width: 150,
    justifyContent: 'center',
    borderRadius: 17.23,
    marginTop: -0,
  },
  filtroRedondoVert: {
    height: 200,
    width: 150,
    justifyContent: 'center',
    borderRadius: 17.23,
    marginTop: -200,
  },
  fotoRedondaHoriz: {
    height: 150,
    width: 200,
    justifyContent: 'center',
    borderRadius: 17.23,
    marginTop: -0,
  },
  filtroRedondoHoriz: {
    height: 150,
    width: 200,
    justifyContent: 'center',
    borderRadius: 17.23,
    marginTop: -150,
  },
  textoTitulo: {
    color: '#fff',
    // fontFamily: 'rockwell extra bold',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    fontFamily: "SFUIDisplay-Black",
    opacity: 1,
    marginTop: -40,
    paddingTop: -20
  },
  textoMenor: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    marginStart: 23,
    fontSize: 21,
    textAlign: 'left',
    fontWeight: 'bold',
    fontFamily: "SFUIDisplay-Black",
    marginTop: 20,
  },


});
