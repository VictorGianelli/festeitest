import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

export default class Botao extends Component {
  
  constructor(props){
    super(props);
    this.state = {};

    this.styles = StyleSheet.create({
      botao:{
        width: 300,
        height: 50,
        borderWidth: 2,
        borderColor: props.bdColor,
        borderRadius: 25,
        backgroundColor: props.bgColor,
        margin: 15,
      },
      btnArea:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },
      btnTexto:{
        fontSize: 18,
        fontWeight: 'bold',
        color: props.cor,
      }
    });
  }

  render() {

    return (
    <TouchableOpacity style = {this.styles.botao} onPress = {this.props.onPress} disabled = {this.props.disable}>
      <View style = {this.styles.btnArea}>
      
        <Text style = {this.styles.btnTexto}>{this.props.nome}</Text>
      
      </View>
    </TouchableOpacity>
    );
  }
}