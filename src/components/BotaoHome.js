import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

export default class Botao extends Component {

  constructor(props) {
    super(props);
    this.state = {};

    this.styles = StyleSheet.create({
      botao: {
        // width: 300,
        // height: 120,
        margin: 15,
      },
      btnArea: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },
      btnTexto: {
        fontSize: 18,
        fontWeight: 'bold',
        color: props.cor,
      },
      textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 30,
        textAlign: "left",
        fontWeight: 'bold',
        opacity: 1,
        marginStart: -20,
      },
    });
  }

  render() {

    return (

      <TouchableOpacity style={{ height: 70, marginBottom: 30 }} onPress={this.props.onPress}  >
        <View style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
          <View style={{
            height: 60, width: 300, backgroundColor: this.props.color, borderRadius: 15,
            flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginBottom: 20, opacity: 0.5
          }}>
          </View>
          <View style={{
            height: 60, width: 300,  borderRadius: 15,
            flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: -75, opacity: 1
          }}>
            <Text style={this.styles.textoTitulo}>{this.props.titulo}</Text>
          </View>
        </View>
      </TouchableOpacity>
      // <TouchableOpacity style={this.styles.botao} onPress={this.props.onPress} disabled={this.props.disable}>
      //     <View style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
      //       <View style={{ height: 120, width: 300, backgroundColor: '#f00', borderRadius: 15, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginBottom: 20 }}>
      //       </View>
      //     </View>
      //     <View style={{ flexDirection: 'column', alignItems: 'flex-start', paddingStart: 75, marginTop: -90 }}>
      //       <Text style={this.styles.textoTitulo}>NOTÍCIAS</Text>
      //     </View>
      // </TouchableOpacity>
    );
  }
}