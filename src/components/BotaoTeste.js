import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

export default class Botao extends Component {
  
  constructor(props){
    super(props);
    this.state = {};

    this.styles = StyleSheet.create({
      botao:{
        width: 30,
        height: 30,
        borderWidth: 2,
        borderColor: props.bdColor,
        borderRadius: 15,
        backgroundColor: props.bgColor,
        margin: 5,
      },
      btnArea:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },
      btnTexto:{
        fontSize: 25,
        justifyContent: 'center',
        alignItems: 'center',
        color: props.cor,
        fontFamily: "SFUIDisplay-Light"
      }
    });
  }

  render() {

    return (
    <TouchableOpacity style = {this.styles.botao} onPress = {this.props.onPress}>
      <View style = {this.styles.btnArea}>
      
        <Text style = {this.styles.btnTexto}>{this.props.nome}</Text>
      
      </View>
    </TouchableOpacity>
    );
  }
}