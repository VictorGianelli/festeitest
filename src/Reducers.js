import { combineReducers } from 'redux';
import AuthReducer from './reducers/AuthReducers';
import ticketReducer from './reducers/ticketReducer';

const Reducers = combineReducers({

    auth:AuthReducer,
    ticket:ticketReducer,
     
});

export default Reducers;