import { StyleSheet } from 'react-native';
export default StyleSheet.create({
  largeButtonText: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  largeHeaderText:{
    fontSize: 16
  },
  mediumHeaderText: {
    fontSize: 14,
    color:'blue'
  }
});