
export const CpfValue = (cpfVal) => {
    return{
        type: 'CpfValue',
        payload: {
            cpfVal:cpfVal
        }
    };
};

export const editDesconto = (cpfVal) => {
    return{
        type: 'editDesconto',
        payload: {
            desconto:desconto
        }
    };
}; 

export const editEmail = (email) => {
    return{
        type: 'editEmail',
        payload: {
            email:email
        }
    };
}; 


export const editSenha = (senha) => {
    return{
        type: 'editSenha',
        payload: {
            senha:senha
        }
    };
}; 

export const editNome = (nome) => {
    return{
        type: 'editNome',
        payload: {
            nome:nome
        }
    };
}; 

export const inserirStrValTotal = (strValueTotal) => {
    return{
        type: 'inserirStrValTotal',
        payload: {
            strValueTotal:strValueTotal
        }
    };
};  

export const valoresIntermediarios = (valoresIntermediarios) => {
    return{
        type: 'valoresIntermediarios',
        payload: {
            valoresIntermediarios:valoresIntermediarios
        }
    };
};  

// export const inserirDadosIngressosDetalhes = (dadosIngressosDetalhes) => {
//     return{
//         type: 'dadosIngressosDetalhes',
//         payload: {
//             dadosIngressosDetalhes:dadosIngressosDetalhes
//         }
//     };
// }; 

export const inserirDadosIngressos = (dadosIngressos) => {
    return{
        type: 'inserirDadosIngressos',
        payload: {
            dadosIngressos:dadosIngressos
        }
    };
}; 