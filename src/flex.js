import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    ImageBackground,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Botao from './components/Botao';
import EsqueciMinhaSenha from './js_Inicio/EsqueciMinhaSenha';

export default class Login extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white'
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.state = { textEmail: '' };
        this.state = { textSenha: '' };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start', width: 200 }}>
                    <View>
                        <Text style={styles.textoData}>22</Text>
                        <Text style={styles.textoData}>---</Text>
                        <Text style={styles.textoData}>JUN</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={require('./img/FotoQuadrada/foto1.jpg')}
                                style={styles.fotoRedonda} />
                            <Image source={require('./img/FotoQuadrada/foto2.jpg')}
                                style={styles.fotoRedonda} />
                            <Image source={require('./img/FotoQuadrada/foto3.jpg')}
                                style={styles.fotoRedonda} />
                            <Image source={require('./img/FotoQuadrada/foto4.jpg')}
                                style={styles.fotoRedonda} />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start' }}>
                    <ScrollView>
                        <View style={{ width: 200 }}>

                            <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                            <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                            <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>
                            <Text style={styles.textoEndereco}>Av. Antônio Abrahão Caran, 1001 São José, Belo Horizonte - MG Portão 6</Text>
                            <Text style={styles.textoDescricao}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</Text>

                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.styleFotoFamoso}>
                                    <Image source={require('./img/FotoQuadrada/foto1.jpg')}
                                        style={styles.fotoRedondaFamoso} />
                                    <Text style={styles.textoNomeFamoso}>Yes, u can!</Text>
                                </View>
                                <View style={styles.styleFotoFamoso}>
                                    <Image source={require('./img/FotoQuadrada/foto2.jpg')}
                                        style={styles.fotoRedondaFamoso} />
                                    <Text style={styles.textoNomeFamoso}>Criss</Text>
                                </View>
                                <View style={styles.styleFotoFamoso}>
                                    <Image source={require('./img/FotoQuadrada/foto3.jpg')}
                                        style={styles.fotoRedondaFamoso} />
                                    <Text style={styles.textoNomeFamoso}>Gloria Groove</Text>
                                </View>
                            </View>

                            <Text style={styles.textoTituloPromocoes}>Promoções</Text>
                            <Text style={styles.textoPromocoes}>- Rodada Dupla de SHOT</Text>
                        </View>
                    </ScrollView>
                    <View style={{width: 50 }}>
                        <Text style={{ color: '#fff' }}>Holi</Text>
                    </View>
                </View>

            </View>
        );
    }

};


const styles = StyleSheet.create({
    container: {
        padding: 36,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around',
        // justifyContent: 'space-between',
        // alignItems: 'center',
        backgroundColor: '#2c2c2c'
    },
    fotoRedonda: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: 15,
        marginVertical: 20,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    fotoRedondaFamoso: {
        height: 80,
        width: 80,
        borderRadius: 40,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    styleFotoFamoso: {
        flexDirection: 'column',
        marginRight: 10,
        marginVertical: 20,
        width: 80,
    },
    textoNomeFamoso: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'space-between',
        marginTop: 5,
    },
    textoTituloPromocoes: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
        justifyContent: 'space-between',
        margin: 5,
    },
    textoPromocoes: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 10,
        textAlign: "left",
        justifyContent: 'space-between',
        marginTop: 5,
    },
    textoData: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoNomeEvento: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
    },
    textoLocalizacao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        textAlign: "left",
    },
    textoCidade: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoEndereco: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
        marginVertical: 20,
        // marginDown: 20,
    },
    textoDescricao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 10,
        textAlign: "left",
        // marginDown: 20,
    },

    // textoMenor: {
    //     color: '#fff',
    //     fontFamily: 'rockwell extra bold',
    //     fontSize: 20,
    // },
    // inputNome: {
    //     width: 300,
    //     //   borderWidth: 1,
    //     //   borderColor: '#999999',
    //     //   backgroundColor: '#EEEEEE',
    //     color: '#000',
    //     height: 38,
    //     margin: 20,
    //     padding: 10,
    // },
    // textInputFormat: {
    //     height: 60,
    //     width: 300,
    //     color: 'white',
    //     fontSize: 18
    // },
    backgroundImage: {
        width: '100%',
        flex: 1,
        resizeMode: 'stretch',
        //backgroundColor: 'rgba(0, 0, 0, 1.0)', 
    },

});

