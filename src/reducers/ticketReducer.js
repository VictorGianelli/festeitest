import { ADD_TICKET,SELECT_PLACE } from '../actions/types';

const initialState = {
  ticket: [],
};

const ticketReducer = (state = initialState, action) => {
  switch(action.type) {
    case ADD_TICKET:
      console.log("quickly reducer")
      return {
        ...state,
        ticket: state.ticket.concat({
          key: action.keyValue,
          count: action.payloadCont,
          number: action.payloadNumber,
          //value: action.payload,
          // value2: action.payloadCont
        })
      };
    // case SELECT_PLACE:
    //   console.log("quickly reducer")
    //   return {
    //     ...state,
    //     ticket: state.ticket.concat({
    //       key: action.keyValue,
    //       count: action.payloadCont,
    //       number: action.payloadNumber,
    //       //value: action.payload,
    //       // value2: action.payloadCont
    //     })
    //   };
    default:
      return state;
  }
}

export default ticketReducer;