const initialState = {
    // email: '',
    // senha: '',
    // cpfValue: '',
//    desconto: '10',
    // strValueTotal: '0,00',
    dadosIngressos: [{
    }],
    dadosIngressosDetalhes: [{
    }],
    nome: '',
};

// const placeReducer = (state = initialState, action) => {
//     switch(action.type) {
//       case ADD_PLACE:
//         return {
//           ...state,
//           places: state.places.concat({
//             key: action.payloadCont,
//             value: action.payload,
//             number: action.payloadNumber,
//           })
//         };
//         case 'DELETE_COMMENT':
//           return state.filter(({ id }) => id !== action.data);
//         case SELECT_PLACE:
//         return {
//           ...state,
//           selectedPlace: state.places.find(place => {
//             return place.key === action.placeKey;
//           })
//         };
//       default:
//         return state;
//     }
//   }

const AuthReducer = (state = [], action) => {

    if (state.length == 0) {
        return initialState;
    }

    if (action.type == 'CpfValue') {
        return {
            ...state, cpfValue: action.payload.cpfValue
        };
    }

    // if (action.type == 'editDesconto') {
    //     return {
    //         ...state, desconto: action.payload.esconto
    //     };
    // }

    if (action.type == 'editEmail') {
        return {
            ...state, email: action.payload.email
        };
    }

    if (action.type == 'editSenha') {
        return {
            ...state, senha: action.payload.senha
        };
    }

    if (action.type == 'inserirStrValTotal') {
        return {
            ...state, strValueTotal: action.payload.strValueTotal
        };
    }

    if (action.type == 'editNome') {
        return {
            ...state, nome: action.payload.nome
        };
    }

    if (action.type == 'valoresIntermediarios') {
        return {
            ...state, nome: action.payload.nome
        };
    }

    // if (action.type == 'inserirDadosIngressosDetalhes') {
    //     return {
    //         ...state, dadosIngressosDetalhes: action.payload.dadosIngressosDetalhes
    //     };
    // }

    // if (action.type == 'inserirDadosIngressos') {
    //     return {
    //         ...state, dadosIngressos: action.payload.dadosIngressos
    //     };
    // }
    return state;
};

export default AuthReducer