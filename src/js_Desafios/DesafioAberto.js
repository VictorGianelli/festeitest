import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { editEmail, editSenha } from '../actions/AuthActions';

import Botao from '../components/Botao';

export class FeedDesafios extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    render() {
        const { navigate } = this.props.navigation
        let ih = 450;
        let mB = (ih + 80) * -1
        return (
            <View style={styles.container}>
                {/* <ScrollView> */}
                {/* <View style={{ alignItems: 'flex-start', marginTop: 20, marginVertical: 0, }}> */}
                    {/* <View style={{ flexDirection: 'row' }}> */}
                        <ScrollView>
                            {/* <View> */}
                                <View style={{  justifyContent: 'flex-start', alignItems: 'center'}}>

                                    <View style={{ height: 400 }} />

                                    <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: mB, borderBottomStartRadius: 10, borderBottomEndRadius: 10, width: '90%', 
                                        height: 100, backgroundColor: '#fff' }} >
                                        <Text style={{ color: '#000', fontSize: 42, fontWeight: 'bold' }}>05:00</Text>
                                        <Text style={{ color: '#000', fontSize: 12 }}>HORAS RESTANTES</Text>
                                    </View>

                                    <View>
                                        <Image source={require('../img/fundo_tela_5.png')}
                                            style={{height: ih,
                                                width: 450,
                                                justifyContent: 'center',
                                                // marginLeft: -10,
                                                marginTop: -0,}} />
                                        <Image source={require('../img/filtro_background.png')}
                                            style={{height: ih,
                                                width: 450,
                                                justifyContent: 'center',
                                                // marginLeft: -10,
                                                marginTop: -ih,}} />
                                    </View>
 
                                    {/* <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: -185.19, borderRadius: 39, width: 78, height: 30, backgroundColor: '#EF3D4E' }} >
                                        <Text style={{ color: '#fff' }}>180xp</Text>
                                    </View> */}

                                    <View style={{ width: '90%',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: -185.19}} >
                                        <View style={{ justifyContent: 'center', alignItems: 'center',borderRadius: 39, width: 78, height: 30, backgroundColor: '#EF3D4E' }} >
                                            <Text style={{ color: '#fff' }}>180xp</Text>
                                        </View>
                                    </View>

                                    <View style={{ width: '90%', flexDirection: 'row', marginTop: 0, marginBottom: -20 }} >
                                        <View style={{ width: 100, flexDirection: 'row'}} >
                                            <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                style={styles.foto} />
                                            <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                style={styles.foto} />
                                        </View>
                                        <View style={{ width: 125, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}} >
                                            <Text style={{ color: '#fff'}}>7 PESSOAS</Text>
                                        </View>
                                    </View>

                                    <View style={{width: '90%', height: 80,flexDirection: 'row',justifyContent: 'center',alignItems: 'flex-start' }} >
                                        <Text style={{ color: '#fff', fontSize: 34, fontWeight: 'bold' }}>DRESS LIKE HALLOWEEN</Text>
                                    </View> 
                                    
                                    <View style={{width: '90%', flexDirection: 'row',justifyContent: 'flex-start',alignItems: 'center', marginTop: 135 }} >
                                        <Text style={{ color: '#fff', fontSize: 25, fontWeight: 'bold' }}>DESAFIO</Text>
                                    </View> 

                                    <View style={{width: '90%', flexDirection: 'row',justifyContent: 'flex-start',alignItems: 'center', marginTop: 15 }} >
                                        <Text style={{ color: '#fff', fontSize: 15, fontWeight: 'bold' }}>Venha para a festa fantasiado e tire foto em nosso cenário dentro do evento.
Os mais votados ganharão um combo de 10 cervejas para dividir com seus amigos!</Text>
                                    </View> 

                                    <Image source={require('../img/inserir_foto.png')}
                                    style={{ width: 120, height: 120, borderRadius: 60 }} />
                                    <View style={{ marginTop: 30 }} >
                                        
                                    </View> 
                                </View>
                            {/* </View> */}
                        </ScrollView>
                    {/* </View> */}
                    
                {/* </View> */}
                {/* </ScrollView> */}
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // justifyContent: 'space-between',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    foto: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: -15,
        marginVertical: 20,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    fotoRedonda: {
        height: 350.19,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    filtroRedondo: {
        height: 350.19,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -350.19,
    },
    fotoRedonda2: {
        height: 280,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    filtroRedondo2: {
        height: 280,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -280,
    },
    textoTitulo: {
        color: '#fff',
        // fontFamily: 'rockwell extra bold',
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
        opacity: 1,
        marginTop: -40,
        paddingTop: -20
    },
    textoMenor: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        marginStart: 23,
        fontSize: 21,
        textAlign: 'left',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
    },
    textoMenor2: {
        color: '#fff',
        //fontFamily: 'rockwell extra bold',
        marginHorizontal: 23,
        fontSize: 14,
        textAlign: 'left',
        fontFamily: "SFUIDisplay-Lighr",
    },
    inputNome: {
        width: 300,
        //   borderWidth: 1,
        //   borderColor: '#999999',
        //   backgroundColor: '#EEEEEE',
        color: '#000',
        height: 38,
        margin: 20,
        padding: 10,
    },
    textInputFormat: {
        height: 60,
        width: 300,
        color: 'white',
        fontSize: 18
    }

});

const mapStateToProps = (state) => {
    return {
    }
};

const FeedDesafiosConnct = connect(mapStateToProps, {})(FeedDesafios);

export default FeedDesafiosConnct;