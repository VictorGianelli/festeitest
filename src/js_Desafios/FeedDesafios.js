import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { editEmail, editSenha } from '../actions/AuthActions';

import Botao from '../components/Botao';

export class FeedDesafios extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Mais 1 voto");
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View>
                    <View style={{
                        alignItems: 'center', justifyContent: 'center',
                        paddingTop: 0, backgroundColor: '#00FFBA', height: 60, opacity: 0.5
                    }} >
                    </View>
                    <Text style={styles.textoTitulo}>DESAFIOS</Text>
                    <View style={{ marginBottom: 13 }} />
                </View>
                {/* ////////////////////////////////// */}
                <ScrollView>
                    <View style={{ alignItems: 'flex-start', marginTop: 20, marginVertical: 0, }}> 
                        <Text style={styles.textoMenor}>DESAFIOS DISPONÍVEIS</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <ScrollView horizontal={true}>

                                <View>
                                    <TouchableOpacity style={{ height: 460, width: 300 }} onPress={() => navigate('DesafioAberto', { name: 'DesafioAberto' })}  >
                                        <View style={{ height: 460, justifyContent: 'flex-start', alignItems: 'center', paddingStart: 7.5, paddingEnd: 7.5 }}>

                                            <View style={{ height: 345.19 }} />

                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: -430, borderRadius: 10, width: 245, height: 100, backgroundColor: '#fff' }} >
                                                <Text style={{ color: '#000', fontSize: 40, fontWeight: 'bold' }}>05:00</Text>
                                                <Text style={{ color: '#000', fontSize: 10 }}>HORAS RESTANTES</Text>
                                            </View>

                                            <View>
                                                <Image source={require('../img/fundo_tela_5.png')}
                                                    style={styles.fotoRedonda} />
                                                <Image source={require('../img/filtro_background.png')}
                                                    style={styles.filtroRedondo} />
                                            </View>

                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: -355.19, borderRadius: 39, width: 78, height: 30, backgroundColor: '#EF3D4E' }} >
                                                <Text style={{ color: '#fff' }}>180xp</Text>
                                            </View>

                                            <View style={{ width: 250, flexDirection: 'row', marginTop: 190.19, marginBottom: -20 }} >
                                                <View style={{ width: 100, flexDirection: 'row' }} >
                                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                        style={styles.foto} />
                                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                        style={styles.foto} />
                                                </View>
                                                <View style={{ width: 125, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >
                                                    <Text style={{ color: '#fff' }}>7 PESSOAS</Text>
                                                </View>
                                            </View>

                                            <View style={{ width: 250, height: 80, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start' }} >{/* , backgroundColor: '#EF3D4E' */}
                                                <Text style={{ color: '#fff', fontSize: 30, fontWeight: 'bold' }}>DRESS LIKE HALLOWEEN</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                
                                {/* ////////////////////////////////////////////////////////// */}
                                <View>
                                    <TouchableOpacity style={{ height: 460, width: 300 }} onPress={() => alert("Nenhuma função atribuida")}  >
                                        <View style={{ height: 460, justifyContent: 'flex-start', alignItems: 'center', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                            <View style={{ height: 345.19 }} />

                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: -430, borderRadius: 10, width: 245, height: 100, backgroundColor: '#fff' }} >
                                                <Text style={{ color: '#000', fontSize: 40, fontWeight: 'bold' }}>05:00</Text>
                                                <Text style={{ color: '#000', fontSize: 10 }}>HORAS RESTANTES</Text>
                                            </View>

                                            <View>
                                                <Image source={require('../img/fundo_tela_2.png')}
                                                    style={styles.fotoRedonda} />
                                                <Image source={require('../img/filtro_background.png')}
                                                    style={styles.filtroRedondo} />
                                            </View>

                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: -355.19, borderRadius: 39, width: 78, height: 30, backgroundColor: '#EF3D4E' }} >
                                                <Text style={{ color: '#fff' }}>180xp</Text>
                                            </View>

                                            <View style={{ width: 250, flexDirection: 'row', marginTop: 190.19, marginBottom: -20 }} >
                                                <View style={{ width: 100, flexDirection: 'row' }} >
                                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                        style={styles.foto} />
                                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                        style={styles.foto} />
                                                </View>
                                                <View style={{ width: 125, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >
                                                    <Text style={{ color: '#fff' }}>7 PESSOAS</Text>
                                                </View>
                                            </View>

                                            <View style={{ width: 250, height: 80, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start' }} >{/* , backgroundColor: '#EF3D4E' */}
                                                <Text style={{ color: '#fff', fontSize: 30, fontWeight: 'bold' }}>DRINKS ON FIRE</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>    
                                </View>
                            </ScrollView>
                        </View>


                        <Text style={styles.textoMenor}>VOTAÇÃO ABERTA</Text>
                        <Text style={styles.textoMenor2}>DRESS LIKE HALLOWEEN - MELHOR FANTASIA</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <ScrollView horizontal={true}>
                                <View>
                                    <View style={{ height: 355, justifyContent: 'flex-start', alignItems: 'center', paddingStart: 7.5, paddingEnd: 7.5, paddingTop: 17.5 }}>

                                        <View style={{ height: 0 }} />

                                        <View>
                                            <Image source={require('../img/fundo_tela_3.png')}
                                                style={styles.fotoRedonda2} />
                                            <Image source={require('../img/filtro_background.png')}
                                                style={styles.filtroRedondo2} />
                                        </View>

                                        <View style={{ width: 250, height: 80, marginTop: -120, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end' }} >
                                            <Text style={{ color: '#fff', fontSize: 30, fontWeight: 'bold', width: 250, }}>ARLEQUINA</Text>
                                        </View>
                                        <View style={{ width: 250, height: 30, marginTop: -0 }} >
                                            <Text style={{ color: '#fff', width: 250 }}>LUIZA ABREU</Text>
                                        </View>

                                        <View style={{ width: 250, flexDirection: 'row', marginTop: 0, marginBottom: -0 }} >
                                            <View style={{ width: 75, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >
                                                <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                    style={styles.foto} />
                                                <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                    style={styles.foto} />
                                            </View>
                                            <View style={{ width: 95, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                                                <Text style={{ color: '#fff', fontSize: 13 }}>7 PESSOAS</Text>
                                            </View>
                                            <View style={{ width: 80, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }} >
                                                <TouchableOpacity onPress={this.alert.bind(this)}  >
                                                    <View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 39, width: 85, height: 40, backgroundColor: '#EF3D4E' }} >
                                                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 17 }}>VOTAR</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                    </View>

                                </View>
                                {/* ///////////////////////////////// */}
                                <View>
                                    <View style={{ height: 355, justifyContent: 'flex-start', alignItems: 'center', paddingStart: 7.5, paddingEnd: 7.5, paddingTop: 17.5 }}>

                                        <View style={{ height: 0 }} />

                                        <View>
                                            <Image source={require('../img/fundo_tela_4.png')}
                                                style={styles.fotoRedonda2} />
                                            <Image source={require('../img/filtro_background.png')}
                                                style={styles.filtroRedondo2} />
                                        </View>

                                        <View style={{ width: 250, height: 80, marginTop: -120, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end' }} >
                                            <Text style={{ color: '#fff', fontSize: 30, fontWeight: 'bold', width: 250, }}>LA CASA DE PAPEL</Text>
                                        </View>
                                        <View style={{ width: 250, height: 30, marginTop: -0 }} >
                                            <Text style={{ color: '#fff', width: 250 }}>PEDRO HENRIQUE & BRUNO PRATES </Text>
                                        </View>

                                        <View style={{ width: 250, flexDirection: 'row', marginTop: 0, marginBottom: -0 }} >
                                            <View style={{ width: 75, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >
                                                <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                    style={styles.foto} />
                                                <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                    style={styles.foto} />
                                            </View>
                                            <View style={{ width: 95, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                                                <Text style={{ color: '#fff', fontSize: 13 }}>7 PESSOAS</Text>
                                            </View>
                                            <View style={{ width: 80, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }} >
                                                <TouchableOpacity onPress={this.alert.bind(this)}  >
                                                    <View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 39, width: 85, height: 40, backgroundColor: '#EF3D4E' }} >
                                                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 17 }}>VOTAR</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                    </View>

                                </View>

                            </ScrollView>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // justifyContent: 'space-between',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    foto: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: -25,
        marginVertical: 20,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    fotoRedonda: {
        height: 350.19,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    filtroRedondo: {
        height: 350.19,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -350.19,
    },
    fotoRedonda2: {
        height: 280,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    fotoRedonda3: {
        height: 280,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginLeft: -10,
    },
    filtroRedondo2: {
        height: 280,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -280,
    },
    textoTitulo: {
        color: '#fff',
        // fontFamily: 'rockwell extra bold',
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
        opacity: 1,
        marginTop: -40,
        paddingTop: -20
    },
    textoMenor: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        marginStart: 23,
        fontSize: 21,
        textAlign: 'left',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
    },
    textoMenor2: {
        color: '#fff',
        //fontFamily: 'rockwell extra bold',
        marginHorizontal: 23,
        fontSize: 14,
        textAlign: 'left',
        fontFamily: "SFUIDisplay-Lighr",
    },
    inputNome: {
        width: 300,
        //   borderWidth: 1,
        //   borderColor: '#999999',
        //   backgroundColor: '#EEEEEE',
        color: '#000',
        height: 38,
        margin: 20,
        padding: 10,
    },
    textInputFormat: {
        height: 60,
        width: 300,
        color: 'white',
        fontSize: 18
    }

});

const mapStateToProps = (state) => {
    return {
    }
};

const FeedDesafiosConnct = connect(mapStateToProps, {})(FeedDesafios);

export default FeedDesafiosConnct;