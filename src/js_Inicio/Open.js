import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  BackHandler,
  Colors,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  View
} from 'react-native';

class SplashScreen extends Component {

  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: 'white',
  };

  constructor(props) {
    super(props);
    this.state = {};

  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  // handleBackButtonClick() {
  //   this.props.navigation.goBack(null);
  //   return true;
  // }


  render() {
    const { navigate } = this.props.navigation;
    setTimeout(() => {
      navigate('News', { name: 'News' }); //this.props.navigation.navigate('Login')
    }, 2000);  //5000 milliseconds
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />

        <Image
          style={{ width: 89, height: 165, resizeMode: 'stretch', }}
          source={require('../img/logoFestei.png')}
        />


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1c1c1c'
  }
});

export default SplashScreen;
