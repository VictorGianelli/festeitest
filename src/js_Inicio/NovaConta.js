import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert,
    ToastAndroid,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Botao from '../components/Botao';

class NovaConta extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            TextInputName: '',
            TextInputCPF: '',
            TextInputEmail: '',
            TextInputPhone: '',
            TextInputBirthDate: '',
            TextInputPassword: '',
            TextInputConfirmPassword: '',
            TextInputGenre: '',
            TextInputState: '',
            TextInputCity: '',
        };


        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }


    validarCPF = () => {
        const { cpf } = this.state;

        strCPF = cpf;

        var Soma;
        var Resto;
        Soma = 0;

        if (strCPF == "00000000000") return false;
        if (strCPF == "") return false;

        for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10))) return false;

        Soma = 0;

        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11))) return false;
        return true;
    }


    cadastrar = () => {
        const { TextInputName } = this.state;
        const { TextInputCPF } = this.state;
        const { TextInputEmail } = this.state;
        const { TextInputPhone } = this.state;
        const { TextInputBirthDate } = this.state;
        const { TextInputPassword } = this.state;
        const { TextInputConfirmPassword } = this.state;
        const { TextInputGenre } = this.state;
        const { TextInputState } = this.state;
        const { TextInputCity } = this.state;

        // let cpfNovo = false;
        this.validarCPF.bind(this);
        if (TextInputName.length == 0) {
            Alert.alert("", "Insira o seu nome");
        } else if (TextInputCPF.length != 11) {
            Alert.alert("", "CPF Incompleto");
        } else if (TextInputEmail.length == 0) {
            Alert.alert("", "Insira o seu E-mail");
        } else if (TextInputPassword <= 5 || TextInputPassword != TextInputConfirmPassword) {
            Alert.alert("Senha incorreta", "Elas devem ser iguais e ter pelo menos 6 digitos");
        } else {

            fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/cpf_unico.php', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: TextInputName,
                        cpf: TextInputCPF,
                        email: TextInputEmail,
                        phone_number: TextInputPhone,
                        birth_date: TextInputBirthDate,
                        password: TextInputPassword,
                        genre: TextInputGenre,
                        state: TextInputState,
                        city: TextInputCity,
                })
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson === 'Data Matched') {

                        ToastAndroid.show('Esse CPF já esta cadastrado...', ToastAndroid.LONG);
                        // this.props.navigation.navigate('Login', { name: 'Login' });

                    } else {

                        ToastAndroid.show('Cadastro efetuado com sucesso!', ToastAndroid.SHORT);
                            this.props.navigation.navigate('Login', { name: 'Login' });
                    }

                }).catch((error) => {
                    console.error(error);
                })

            // if (cpfNovo) {
            //     fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/cadastrar_usuario.php', {
            //         method: 'POST',
            //         headers: {
            //             'Accept': 'application/json',
            //             'Content-Type': 'application/json'
            //         },
            //         body: JSON.stringify({
                        
            //         })
            //     }).then((response) => response.json())
            //         .then((responseJson) => {
            //             if (responseJson === 'Done') {

            //                 ToastAndroid.show('Cadastro efetuado com sucesso!', ToastAndroid.SHORT);
            //                 this.props.navigation.navigate('Login', { name: 'Login' });

            //             } else {

            //                 Alert.alert("Erro", responseJson);

            //             }
            //             // Alert.alert(responseJson);

            //         }).catch((error) => {
            //             console.error(error);
            //         })
            // }
        }

    }

    render() {
        // let state = this.state;
        // if (state.TextInputBirthDate == undefined ? state.TextInputBirthDate = "" : this.BirthDate.bind(this));
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'column', alignItems: 'flex-start', paddingStart: 20 }}>
                    <Text style={styles.textoTitulo}>NOVA CONTA</Text>
                </View>

                <ScrollView>
                    <View>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={styles.textoMenor}>Fazer login com...</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={require('../img/facebook_logo.png')}
                                    style={{ width: 50, height: 50, margin: 30 }} />
                                <Image source={require('../img/google_logo.png')}
                                    style={{ width: 50, height: 50, margin: 30 }} />
                            </View>
                        </View>
                        <View style={{ alignItems: 'center', marginLeft: 20, marginRight: 20 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={require('../img/inserir_foto.png')}
                                    style={{ width: 120, height: 120, borderRadius: 60 }} />
                                <View style={{ flexDirection: 'column', paddingStart: 20 }}>
                                    <TextInput
                                        placeholder=" Nome"
                                        placeholderTextColor="#fff"
                                        underlineColorAndroid="white"
                                        style={styles.textInputFormatSmall}
                                        onChangeText={(TextInputValue) => this.setState({ TextInputName: TextInputValue })}
                                    //value={this.state.textNome}
                                    />
                                    <TextInput
                                        placeholder=" CPF"
                                        placeholderTextColor="#fff"
                                        underlineColorAndroid="white"
                                        keyboardType='numeric'
                                        maxLength={11}
                                        style={styles.textInputFormatSmall}
                                        onChangeText={(TextInputValue) => this.setState({ TextInputCPF: TextInputValue })}
                                    // value={this.state.textCpf}
                                    />
                                </View>
                            </View>
                            <TextInput
                                placeholder=" E-mail"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                                style={styles.textInputFormat}
                                onChangeText={(TextInputValue) => this.setState({ TextInputEmail: TextInputValue })}
                            //value={this.state.textEmail}
                            />
                            <TextInput
                                placeholder=" Senha"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                                style={styles.textInputFormat}
                                secureTextEntry={true}
                                onChangeText={(TextInputValue) => this.setState({ TextInputPassword: TextInputValue })}
                            //value={this.state.textSenha}
                            />
                            <TextInput
                                placeholder=" Confirmar Senha"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                                style={styles.textInputFormat}
                                secureTextEntry={true}
                                onChangeText={(TextInputValue) => this.setState({ TextInputConfirmPassword: TextInputValue })}
                            //value={this.state.textConfirmarSenha}
                            />
                            <TextInput
                                placeholder=" Nascimento"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                                style={styles.textInputFormat}
                                onChangeText={(TextInputValue) => this.setState({ TextInputBirthDate: TextInputValue })}
                            // value={this.state.textNascimento}
                            />
                            <TextInput
                                placeholder=" Genero"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                                style={styles.textInputFormat}
                                onChangeText={(TextInputValue) => this.setState({ TextInputGenre: TextInputValue })}
                            //value={this.state.text}
                            />
                            <TextInput
                                placeholder=" Estado"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                                style={styles.textInputFormat}
                                maxLength={2}
                                onChangeText={(TextInputValue) => this.setState({ TextInputState: TextInputValue })}
                            //value={this.state.textEstado}
                            />
                            <TextInput
                                placeholder=" Cidade"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                                style={styles.textInputFormat}
                                onChangeText={(TextInputValue) => this.setState({ TextInputCity: TextInputValue })}
                            //value={this.state.textCidade}
                            />
                            <TextInput
                                placeholder=" Telefone DDD 9.9999-9999"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                                style={styles.textInputFormat}
                                onChangeText={(TextInputValue) => this.setState({ TextInputPhone: TextInputValue })}
                            //value={this.state.textTelefone}
                            />
                        </View>
                    </View>
                </ScrollView>

                <View style={{ alignItems: 'center' }}>
                    <Botao cor="#fff" bdColor="#F15A24"
                        bgColor="#F15A24" nome="Cadastrar"
                        //onPress={() => alert("Nenhuma função atribuida")}
                        onPress={this.cadastrar.bind(this)} />

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingTop: 48,
        paddingBottom: 20,
        // alignItems: 'center',
        backgroundColor: '#1c1c1c'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoMenor: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 20,
    },
    inputNome: {
        width: 300,
        //   borderWidth: 1,
        //   borderColor: '#999999',
        //   backgroundColor: '#EEEEEE',
        color: '#000',
        height: 38,
        margin: 20,
        padding: 10,
    },
    textInputFormat: {
        height: 60,
        width: 300,
        color: 'white',
        fontSize: 18
    },
    textInputFormatSmall: {
        height: 60,
        width: 200,
        color: 'white',
        fontSize: 18
    },

});

export default NovaConta;