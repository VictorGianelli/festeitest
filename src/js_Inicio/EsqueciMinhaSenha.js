import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    Alert,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Botao from '../components/Botao';

export default class EsqueciMinhaSenha extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            TextInputCPF: '',
            boo: false
        };

        this.state = { textCpf: '' };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    // validarCPF = () => {
    //     const { TextInputCPF } = this.state;
    //     let cpf = TextInputCPF;

    //     let val1=0,valMenos=0;
    //     for(i=11;i>=2;i--){
    //         let v1 =  Number.parseInt(cpf/10^i);
    //         valMenos = valMenos*10 + v1; 
    //     }
    // }
    validarCPF = () => {
        const { TextInputCPF } = this.state;
        
        var strCPF = TextInputCPF;

        var Soma;
        var Resto;
        Soma = 0;

        if (strCPF == "00000000000") return false;

        for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10))) return false;

        Soma = 0;

        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11))) return false;
        return true;
    }

    recuperar = () => {
        const { TextInputCPF } = this.state;

        // Alert.alert("!!", "CPF completo");

        //this.validarCPF.bind(this);

        var strCPF = TextInputCPF;

        var Soma;
        var Resto1 = 0, Resto2 = 0;
        var ok1 = parseInt(('' + strCPF)[9]), ok2 = parseInt(('' + strCPF)[10]);
        Soma = 0;

        // if (strCPF == "00000000000") 

        for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto1 = (Soma * 10) % 11;

        if ((Resto1 == 10) || (Resto1 == 11)) Resto1 = 0;

        Soma = 0;

        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto2 = (Soma * 10) % 11;

        if ((Resto2 == 10) || (Resto2 == 11)) Resto2 = 0;

        // Alert.alert(ok1 + " e " + ok2, "Valores: " + Resto1 + " e " + Resto2);

        if (TextInputCPF.length != 11) {
            Alert.alert("!", "CPF Incompleto");


        } else if ((ok1 == Resto1) && (ok2 == Resto2)) {
            // Alert.alert("!!", "Funcionou");

            var pCode = (Math.floor(Math.random() * 1000000));
            //Alert.alert("!!", "" + (Math.floor(Math.random() * 1000000)));
            // Math.floor(Math.random() * 10) + Math.random() * 10) + Math.random() * 10))

            fetch('https://www.surferweb.com.br/testes/festeiPHP/recuperar_senha.php', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    cpf: strCPF,
                    code: pCode,
                })
            }).then((response) => response.json())
                .then((responseJson) => {
                    // If server response message same as Data Matched
                    if (responseJson === 'Done') {

                        Alert.alert("Solução a curto prazo", "Nova senha = "+pCode);
                        // this.props.navigation.navigate('Home', { name: 'Home' });

                    }
                    else {

                        // Alert.alert("Erro", responseJson);
                        Alert.alert("!!!", "CPF Incorreto");
                    }
                }).catch((error) => {
                    console.error(error);
                });

            // } else {
            //     Alert.alert("!!!", "CPF Incorreto");
            // }

        } else {
            Alert.alert("!!!", "CPF Incorreto");
        }
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'column', alignItems: 'flex-start', paddingStart: 20 }}>
                    <Text style={styles.textoTitulo}>RECUPERAR SENHA</Text>
                </View>

                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.textoMenor}>Informe seu CPF abaixo que enviaremos a{"\n"}recuperação de senha no e-mail cadastrado!</Text>

                    <TextInput
                        placeholder="  CPF"
                        placeholderTextColor="#fff"
                        underlineColorAndroid="white"
                        maxLength={11}
                        style={{ height: 60, width: 300, color: 'white', fontSize: 18 }}
                        onChangeText={(TextInputValue) => this.setState({ TextInputCPF: TextInputValue })}
                    // value={this.state.textCpf}
                    />
                </View>

                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity>
                        <Botao cor="#fff" bdColor="#F15A24"
                            bgColor="#F15A24" nome="Enviar"
                            onPress={this.recuperar.bind(this)}
                        //onPress={() => alert("Nenhuma função atribuida")}
                        />
                    </TouchableOpacity>

                    <View style={{ padding: 40 }}></View>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingTop: 48,
        paddingBottom: 20,
        // alignItems: 'center',
        backgroundColor: '#1c1c1c'
    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoMenor: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
    },
    inputNome: {
        width: 300,
        //   borderWidth: 1,
        //   borderColor: '#999999',
        //   backgroundColor: '#EEEEEE',
        color: '#000',
        height: 38,
        margin: 20,
        padding: 10,
    }

});