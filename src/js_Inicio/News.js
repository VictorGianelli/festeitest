import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Colors,
  TextInput,
  BackHandler,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Botao from '../components/Botao';

class PreLogin extends Component {

  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props);
    this.state = {};

    // this.novaConta = this.novaConta.bind(this);
  }

  componentDidMount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
    return false;
  }

  //   logar(){
  //     this.props.navigation.navigate(Login);
  //   }

  // novaConta() {
  //   alert("Nova Conta");
  //   this.props.navigation.navigate(screen1);
  // }

  render() {
    const { navigate } = this.props.navigation
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />

        <Text style={styles.texto}>A{"\n"}FESTA{"\n"}CONTINUA{"\n"}EM{"\n"}SUAS{"\n"}MÃOS</Text>

        {/* <TextInput style=
          {{
            height: 40, borderColor: 'gray', borderWidth: 1, color : "#fff"
          }}
          placeholder="E-mail"
          underlineColor="#fff"
          // onChangeText={this.nomeForm}
        /> */}

        {/* <Text style={styles.texto}>{"\n"}{"\n"}</Text> */}

        <View style={{ alignItems: 'center' }}>
          <TouchableOpacity>
            <Botao cor="#fff" bdColor="#F15A24"
              bgColor="#F15A24" nome="Entrar"
              onPress={() => navigate('Login', { name: 'Login' })}
            // onPress={() => navigate('Home', { name: 'Home' })} 
            />
          </TouchableOpacity>

          <TouchableOpacity >
            <Botao cor="#fff" bdColor="transparent"
              bgColor="transparent" nome="Nova Conta"
              onPress={() => navigate('NovaConta', { name: 'NovaConta' })}
              />
          </TouchableOpacity>
        </View>
        
        {/* <TouchableOpacity >
          <Botao cor="#fff" bdColor="#F15A24"
            bgColor="#F15A24" nome="Login"
            onPress={() => navigate('Login', { name: 'Login' })} />
        </TouchableOpacity>

        <TouchableOpacity >
          <Botao cor="#fff" bdColor="#F15A24"
            bgColor="transparent" nome="Nova Conta"
            onPress={() => navigate('NovaConta', { name: 'NovaConta' })}
          />
        </TouchableOpacity> */}

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingTop: 48,
    paddingBottom: 20,
    // alignItems: 'center',
    backgroundColor: '#1c1c1c', //#026
  },
  texto: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 50,
    textAlign: "left",
    marginStart: 50,
    marginEnd: 50,
    fontWeight: 'bold',
  }

});

//   const Navegador = StackNavigator({
//     TelaLogo: { screen: TelaLogo,
//         navigationOptions: {
//           headerTitle: '',
//         },
//      },
//     PreLogin: { screen: PreLogin,
//     navigationOptions: {
//       headerTitle: '',
//     },
//  }
//   });

export default PreLogin;
