import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    Alert,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { editEmail, editSenha } from '../actions/AuthActions';
import { AsyncStorage } from 'react-native';
import { NetInfo } from 'react-native'
import Botao from '../components/Botao';
import OfflineNotice from '../components/OfflineNotice';

export class Login extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            TextInputEmail: this.props.email,
            TextInputPassword: this.props.senha,

            showIndicator: false,
            isConnected: true
        };

        this.state = { textEmail: '' };
        this.state = { textSenha: '' };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        // NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        // NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    handleBackButtonClick() {
        // this.setState({ isConnected });
        this.props.navigation.goBack(null);
        return true;
    }

    // async componentDidMount() {
    //     const response = await fetch('https://www.surferweb.com.br/testes/festeiPHP/login.php');
    //     const json = await response.json();
    //     this.setState({ data: json });
    //     console.warning(data.length);
    // }

    login = () => {
        // const { TextInputEmail } = this.props.email;
        // const { TextInputPassword } = this.props.senha;
        // textEmail = '';
        this.state = {
              showIndicator: true,
        };
        // if (TextInputPassword <= 5 || TextInputPassword != TextInputConfirmPassword) {
        //     Alert.alert("Senha incorreta","Elas devem ser iguais e ter pelo menos 6 digitos");
        // }else{
        // fetch('https://www.festei.fun/php/User_Login.php', {
        fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/user_login.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.props.email,
                password: this.props.senha,
            })
        }).then((response) => response.json())
            .then((responseJson) => {

                // If server response message same as Data Matched
                //Alert.alert("", responseJson);
                if (responseJson === 'Data Matched') {

                    //Then open Profile activity and send user email to profile activity.
                    // this.props.email = ;

                    this.state = {
                        showIndicator: false,
                    };
                    this.props.navigation.navigate('Home', { name: 'Home' });

                }
                else {
                    this.state = {
                        showIndicator: false,
                    };
                    Alert.alert("Erro", responseJson);

                }

            }).catch((error) => {
                Alert.alert("Erro", error);
            });
        //this.props.navigation.navigate('Login',{ name: 'Login' });
        //     if(responseJson=="OK"){
        //         this.props.navigation.navigate('Login',{ name: 'Login' });
        //     }else{
        //         Alert.alert(responseJson);
        //     }
        // }).catch((error) => {
        //     console.warning(error);
        // })

        // 
    }

    render() {
        // let state = this.state;
        // if (state.textEmail.length == 0 ? Alert.alert("a") : Alert.alert("b"));
        // Alert.alert("react",this.props.email+" "+this.props.senha);
        const { navigate } = this.props.navigation
        if (this.state.showIndicator) {
            return (
                <View style={styles.container}>
                    <View style={styles.containerLoading}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    
                    {/* <OfflineNotice isConnected={true}/> */}
                    <View style={{ flexDirection: 'column', alignItems: 'flex-start', paddingStart: 20 }}>
                        <Text style={styles.textoTitulo}>Login</Text>
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.textoMenor}>Fazer login com...</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={require('../img/facebook_logo.png')}
                                style={{ width: 50, height: 50, margin: 30 }} />
                            <Image source={require('../img/google_logo.png')}
                                style={{ width: 50, height: 50, margin: 30 }} />
                        </View>
                        <TextInput //use the color style to change the text color
                            placeholder=" E-mail"
                            placeholderTextColor="#fff"
                            underlineColorAndroid="white"
                            style={styles.textInputFormat}
                            onChangeText={(texto) => this.props.editEmail(texto)}
                            // onChange={(TextInputValue) => this.props.editEmail(TextInputValue)}
                            value={this.props.email}
                        />
                        <TextInput //use the color style to change the text color
                            placeholder=" Senha"
                            placeholderTextColor="#fff"
                            underlineColorAndroid="white"
                            style={styles.textInputFormat}
                            secureTextEntry={true}
                            onChangeText={(texto) => this.props.editSenha(texto)}
                            value={this.props.senha}
                        />
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity>
                            <Botao cor="#fff" bdColor="#F15A24"
                                bgColor="#F15A24" nome="Login"
                                onPress={this.login.bind(this)}
                            // onPress={() => navigate('Home', { name: 'Home' })} 
                            />
                        </TouchableOpacity>

                        <TouchableOpacity >
                            <Botao cor="#fff" bdColor="transparent"
                                bgColor="transparent" nome="Esqueci minha senha!"
                                onPress={() => navigate('EsqueciMinhaSenha', { name: 'EsqueciMinhaSenha' })} />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingTop: 48,
        paddingBottom: 20,
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026


    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoMenor: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 20,
    },
    inputNome: {
        width: 300,
        //   borderWidth: 1,
        //   borderColor: '#999999',
        //   backgroundColor: '#EEEEEE',
        color: '#000',
        height: 38,
        margin: 20,
        padding: 10,
    },
    textInputFormat: {
        height: 60,
        width: 300,
        color: 'white',
        fontSize: 18
    }

});

const mapStateToProps = (state) => {
    return {
        email: state.auth.email,
        senha: state.auth.senha
    }
};

const EntrarConnct = connect(mapStateToProps, { editEmail, editSenha })(Login);

export default EntrarConnct;