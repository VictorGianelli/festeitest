import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  ScrollView,
  Platform,
  Image,
  FlatList,
  TouchableOpacity,
  Button
} from 'react-native';
import BotaoTeste from './components/BotaoTeste';
import BotaoComTexto from './components/BotaoComTexto';
import Botao from './components/Botao';

import Swiper from 'react-native-swiper';

export default class App extends Component {

  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: 'white',
  };

  constructor(props) {
    super(props);
    this.state = {
      tmpArray: [

        { name: "Pankaj", age: 10, class: "M.C.A" },
        { name: "Rita", age: 11, class: "B.C.A" },
        { name: "Mohan", age: 12, class: "M.C.A" },
        { name: "Amit", age: 13, class: "M.C.A" },
        { name: "Babulal", age: 14, class: "B.TECH" },
        { name: "Mohit", age: 15, class: "B.C.A" },
        { name: "Amit", age: 16, class: "B.C.A" },
        { name: "Ramkishan", age: 17, class: "B.C.A" },
        { name: "Gopal", age: 18, class: "B.C.A" },
        { name: "Jogesh", age: 19, class: "B.C.A" },
        { name: "Yogesh", age: 20, class: "B.C.A" }
      ],
      eventos: [
        {
          id: '0',
          data: 22,
          mes: "JUN",
          numero: 0,
          preco: 25,
          taxa: 2.5,
          total: 0,
          tipo: "1º LOTE",
          border: 0
        },
        {
          id: '1',
          data: 22,
          mes: "JUN",
          numero: 0,
          preco: 30,
          taxa: 2.25,
          total: 0,
          tipo: "VIP",
          border: 2
        }
      ],
      cont: 0,
      valTotal: 0,
      strValTotal: '0,00'

    };
  }

  comprar = (valTotal) => {
    let state = this.state;

    // this.state.cont += 1
    // this.state.valTotal = valTotal + 17.5;

    // this.state.strValTotal = this.state.valTotal.toFixed(2);
    // this.state.strValTotal = this.state.strValTotal.replace(/\./g, ',');
    // this.state.strValTotal = this.state.strValTotal;

    if (state.valTotal > 0) {
      alert("Vc selecionou um ingresso");
    } else {
      alert("Vc não selecionou um ingresso");
    }

    // Alert.alert(" " + this.state.strValTotal + " \n " + this.state.strValTotal);

    this.setState(state);
  }

  vender = (valTotal) => {
    let state = this.state;

    if (this.state.cont > 0) {
      this.state.cont -= 1
      this.state.valTotal = valTotal - 17.5;

      this.state.strValTotal = this.state.valTotal.toFixed(2);
      this.state.strValTotal = this.state.strValTotal.replace(/\./g, ',');
      this.state.strValTotal = this.state.strValTotal;
    }

    this.setState(state);

  }

  mais = (item) => {
    let state = this.state;
    this.state.eventos[item.id].numero += 1;

    var value = 0;
    for (var i = 0; i < this.state.eventos.length; i++) {
      value += (this.state.eventos[i].preco + this.state.eventos[i].taxa) * this.state.eventos[i].numero;
    }

    this.state.eventos[item.id].total = value.toFixed(2);
    this.state.valTotal = this.state.eventos[item.id].total;

    this.setState(state);

    this.state.strValTotal = this.state.valTotal;
    this.state.strValTotal = this.state.strValTotal.replace(/\./g, ',');
  }

  menos = (item) => {
    let state = this.state;
    if (this.state.eventos[item.id].numero > 0) {
      this.state.eventos[item.id].numero -= 1;

      var value = 0;
      for (var i = 0; i < this.state.eventos.length; i++) {
        value += (this.state.eventos[i].preco + this.state.eventos[i].taxa) * this.state.eventos[i].numero;
      }

      this.state.eventos[item.id].total = value.toFixed(2);
      this.state.valTotal = this.state.eventos[item.id].total;

      this.setState(state);

      this.state.strValTotal = this.state.valTotal;
      this.state.strValTotal = this.state.strValTotal.replace(/\./g, ',');
    }
  }
  showArrayItem = (item) => {

    Alert.alert(item);

  }

  render() {
    return (
      <View style={styles.MainContainer}>

        <View style={styles.margem}>

          <Swiper
            horizontal='true'
            paginationStyle={{ bottom: -1 }}
            dot={<View style={{ backgroundColor: '#555', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
            activeDot={<View style={{ backgroundColor: '#ED1E79', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
          >

            {
              this.state.eventos.map((item, key) => (
                // onPress={this.showArrayItem.bind(this, String(item.age))}

                <View style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0 }}>
                  {/* Parte de cima */}
                  <View style={{ borderWidth: item.border, borderColor: '#FF9D00', borderBottomLeftRadius: 17.23, borderBottomRightRadius: 15 }}>
                    <Image source={require('./img/fundo_tela_evento.png')}
                      style={styles.fotoRedonda} />
                    <Image source={require('./img/filtro_background.png')}
                      style={styles.filtroRedondo} />
                    <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                      <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.textoDia}>{item.data}</Text>
                        <Text style={styles.textoBarra}>___</Text>
                        <Text style={styles.textoMes}>{item.mes}</Text>
                      </View>
                      <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                        <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                        <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>
                      </View>
                    </View>
                  </View>
                  {/* Parte de baixo */}
                  <View style={{ borderWidth: item.border, borderColor: '#FF9D00', padding: 20, height: 150, width: 300, backgroundColor: '#262626', borderRadius: 15, flexDirection: 'row' }}>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100 }}>
                      <Text style={{ color: '#fff', fontFamily: "SFUIText-Bold", fontSize: 14 }}>{item.tipo}</Text>
                      <Text style={{ color: '#fff', fontFamily: "SFUIText-Light", fontSize: 16, paddingTop: 10 }}>R$ {item.preco} (+{item.taxa} taxa)</Text>
                      <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Bold", fontSize: 14 }}>Pague em até 12x</Text>
                      <View style={{ justifyContent: 'flex-end', }}>
                        <Text style={{ color: '#fff', fontFamily: "SFUIText-LightItalic", fontSize: 14, paddingTop: 20 }}>Vendas até às 23h</Text>
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <BotaoTeste cor="#fff" bdColor="#4c4c4c"
                          bgColor="transparent" nome="+"
                          onPress={this.mais.bind(this, item)}//() => navigate('NovaConta', { name: 'NovaConta' })
                        />
                        <Text style={{ color: '#fff', fontSize: 20, fontFamily: "SFUIDisplay-Bold", marginRight: 14 }}>
                          {item.numero}
                        </Text>
                        <BotaoTeste cor="#fff" bdColor="#4c4c4c"
                          bgColor="transparent" nome="-"
                          onPress={this.menos.bind(this, item)}//() => navigate('NovaConta', { name: 'NovaConta' })
                        />
                      </View>
                    </View>
                  </View>

                  <View>
                    <Text style={{ color: '#fff', fontSize: 50, marginVertical: -189 }}>- - - - - - - - - -</Text>
                  </View>
                  <View style={{ height: 50, width: 300 }}></View>
                </View>
                // <View key={key} >

                //   <Text style={styles.TextStyle} > Preço = {item.preco} </Text>

                //   <Text style={styles.TextStyle} >Taxa = {item.taxa} </Text>

                //   <Text style={styles.TextStyle} > Tipo = {item.tipo} </Text>

                //   <View style={{ width: '100%', height: 1, backgroundColor: '#000' }} />

                // </View>

              ))
            }

          </Swiper>

          <View style={{ alignItems: 'center' }}>
            <BotaoComTexto cor="#fff" bdColor="#4c4c4c"
              bgColor="transparent"
              placeholderTextColor="#fff"
              underlineColorAndroid="white"
            />
          </View>
          <TouchableOpacity style={{ alignItems: 'center' }}>
            <Botao cor="#fff" bdColor="#F15A24"
              bgColor="transparent" nome="Aplicar Cupom"
            />
          </TouchableOpacity>

          <View style={{ alignItems: 'flex-start', marginStart: 0 }}>
            <Text style={styles.textoTotal}>Total</Text>
            <Text style={styles.textoValor}>R$ {this.state.strValTotal}</Text>
          </View>
          <TouchableOpacity style={{ alignItems: 'center' }}>
            <Botao cor="#fff" bdColor="#F15A24"
              bgColor="#F15A24" nome="Comprar"
              onPress={this.comprar}//() => navigate('NovaConta', { name: 'NovaConta' })
            />
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#1c1c1c',
    color: '#fff'
  },
  TextStyle: {
    fontSize: 20,
    color: '#000',
    textAlign: 'left'
  },

  margem: {
    flex: 1
  },
  switchView: {
    width: 350,
    height: 450
  },
  texto: {
    color: '#fff',
    fontFamily: 'arial',
    fontSize: 20,
    marginStart: 50,
    textAlign: "left",
  },
  textoTotal: {
    color: '#fff',
    fontSize: 18,
    marginStart: 20,
    textAlign: "left",
    fontFamily: "SFuidisplay-medium",
    alignItems: 'center'
  },
  textoValor: {
    color: '#F15A24',
    fontSize: 35,
    marginStart: 20,
    textAlign: "left",
    fontFamily: "SFuidisplay-medium",
    alignItems: 'center'
  },
  textoGrande: {
    color: '#fff',
    // fontFamily: 'rockwell extra bold',
    // fontSize: 50,
    textAlign: "left",
    // marginStart: 50,
    // marginEnd: 50,
    fontWeight: 'bold',
  },
  backgroundImage: {
    width: '100%',
    flex: 1,
    resizeMode: 'stretch',
    //backgroundColor: 'rgba(0, 0, 0, 1.0)',
  },
  fotoRedonda: {
    height: 533.6,
    width: 300,
    resizeMode: "stretch",
    justifyContent: 'center',
    borderRadius: 17.23,

    marginTop: -333.6,
    // marginVertical: 20,
    // borderWidth: 2,borderColor: '#FF9D00'
    // elevation: 10,
    // shadowColor: '#202020',
    // shadowOffset: {width: 10, height: 10 },
    // shadowRadius: 40,
  },
  filtroRedondo: {
    height: 533.6,
    width: 300,
    resizeMode: "stretch",
    justifyContent: 'center',
    borderRadius: 17.23,
    marginTop: -533.6,
    // marginVertical: 20,

    // elevation: 10,
    // shadowColor: '#202020',
    // shadowOffset: {width: 10, height: 10 },
    // shadowRadius: 40,
  },

  textoMes: {
    color: '#fff',
    fontFamily: "SFuidisplay-Black",
    fontSize: 30,
    textAlign: "center",
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  textoBarra: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 30,
    textAlign: "center",
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  textoDia: {
    color: '#fff',
    fontFamily: "SFuidisplay-Medium",
    fontSize: 40,
    textAlign: "center",
    fontWeight: 'bold',
    marginBottom: -35,
    justifyContent: 'center',
  },
  textoNomeEvento: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 40,
    textAlign: "left",
  },
  textoLocalizacao: {
    color: '#fff',
    fontFamily: 'arial',
    fontSize: 20,
    textAlign: "left",
  },
  textoCidade: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 15,
    textAlign: "left",
    fontWeight: 'bold',
  },

});