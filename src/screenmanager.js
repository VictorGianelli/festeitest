import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

//js_Inicio
import Open from './js_Inicio/Open';
import News from './js_Inicio/News';
import Login from './js_Inicio/Login';
import EsqueciMinhaSenha from './js_Inicio/EsqueciMinhaSenha';
import NovaConta from './js_Inicio/NovaConta';
//js_Home
import Home from './js_Home/Home';
import Perfil from './js_Home/Perfil';
import FotoPerfil from './js_Home/FotoPerfil';
import Fotos from './js_Home/Fotos';
import Ingressos from './js_Home/Ingressos';
import IngressoAberto from './js_Home/IngressoAberto';
import EventoQueFui from './js_Home/EventoQueFui';
import Medalha from './js_Home/Medalha';
import Conquistas from './js_Home/Conquistas';
import Configuracoes from './js_Home/Configuracoes';
import FaleConosco from './js_Home/FaleConosco';
//js_Noticias
import FeedNoticias from './js_Noticias/FeedNoticias';
//js_Eventos
import FeedEventos from './js_Eventos/FeedEventos';
import BioEvento from './js_Eventos/BioEventobkp';
import CompraIngresso from './js_Eventos/CompraIngresso';
import DadosIngresso from './js_Eventos/DadosIngresso';
import DadosIngresso2 from './js_Eventos/DadosIngresso2';
import AceitarTermos from './js_Eventos/AceitarTermos';
//js_Desafios
import FeedDesafios from './js_Desafios/FeedDesafios';
import DesafioAberto from './js_Desafios/DesafioAberto';
// import Swiper from './Swiper';
import Reducers from './Reducers';
import flatList from './flatList';

let store = createStore(Reducers);

const Screens = createAppContainer(
  createStackNavigator({
    // flatList: {
    //   screen: flatList,
    //   navigationOptions: {
    //     headerTitle: '',
    //   },
    // },
    Open: {
      // ** //
      screen: Open,
      navigationOptions: {
        headerTitle: '',
      },
    },
    CompraIngresso: {
      screen: CompraIngresso,
      navigationOptions: {
        headerTitle: '',
      },
    },
    News: {
      screen: News,
      navigationOptions: {
        hidden: true
      },
    }, 
    BioEvento: {
      screen: BioEvento,
      navigationOptions: {
        headerTitle: '',
      },
    },
    CompraIngresso: {
      screen: CompraIngresso,
      navigationOptions: {
        headerTitle: '',
      },
    },
    DadosIngresso: {
      screen: DadosIngresso,
      navigationOptions: {
        headerTitle: '',
      },
    },
    FeedEventos: {
      screen: FeedEventos,
      navigationOptions: {
        headerTitle: '',
      },
    },
    FeedDesafios: {
      screen: FeedDesafios,
      navigationOptions: {
        headerTitle: '',
      },
    },
    NovaConta: {
      screen: NovaConta,
      navigationOptions: {
        headerTitle: '',
      },
    },
    Home: {
      screen: Home,
      navigationOptions: {
        headerTitle: '',
      },
    },
    Perfil: {
      screen: Perfil,
      navigationOptions: {
        headerTransparent: true,
        
      },
    },
    Ingressos: {
      screen: Ingressos,
      navigationOptions: {
        headerTransparent: true
      },
    },
    FaleConosco: {
      screen: FaleConosco,
      navigationOptions: {
        headerTransparent: true
      },
    },
    Configuracoes: {
      screen: Configuracoes,
      navigationOptions: {
        headerTransparent: true
      },
    },
    Medalha: {
      screen: Medalha,
      navigationOptions: {
        headerTransparent: true
      },
    },
    EventoQueFui: {
      screen: EventoQueFui,
      navigationOptions: {
        headerTransparent: true
      },
    },
    Conquistas: {
      screen: Conquistas,
      navigationOptions: {
        headerTransparent: true
      },
    },
    IngressoAberto: {
      screen: IngressoAberto,
      navigationOptions: {
        headerTransparent: true
      },
    },
    Fotos: {
      screen: Fotos,
      navigationOptions: {
        headerTransparent: true,
      },
    },
    FotoPerfil: {
      screen: FotoPerfil,
      navigationOptions: {
        headerTitle: '',
      },
    },
    FeedNoticias: {
      screen: FeedNoticias,
      navigationOptions: {
        headerTitle: '',
      },
    },
    DesafioAberto: {
      screen: DesafioAberto,
      navigationOptions: {
        headerTitle: '',
      },
    },
    AceitarTermos: {
      screen: AceitarTermos,
      navigationOptions: {
        headerTitle: '',
      },
    },
    // Swiper: {
    //   screen: Swiper,
    //   navigationOptions: {
    //     headerTitle: '',
    //   },
    // },
    Login: {
      screen: Login,
      navigationOptions: {
        headerTitle: '',
      },
    },
    EsqueciMinhaSenha: {
      screen: EsqueciMinhaSenha,
      navigationOptions: {
        headerTitle: '',
      },
    },
  }) 
);

// export default Screens;
export default class screenmanager extends Component {
  render(){
    return(
      <Provider store={store}>
          <Screens/>
      </Provider>
    );
  }  
}