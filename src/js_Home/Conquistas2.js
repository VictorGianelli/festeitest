import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import BotaoHome from '../components/BotaoHome';

class Ingressos extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: '#000'
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Nenhuma função atribuida");
    }

    // componentDidMount() {
    //     this.myScroll.scrollTo(...)
    // }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ paddingStart: 37 }}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}></Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    <View >
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row', height: 30 }}>
                                <Text style={styles.botoesAreasCinza} onPress={() => alert("Link1")}>Perfil </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => alert("Link2")}>Ingressos </Text>
                                <Text style={styles.botoesAreasBranco} onPress={() => alert("Link3")}>Conquistas </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => alert("Link4")}>Configuraçoes </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => alert("Link5")}>Fale Conosco </Text>
                            </View>
                        </ScrollView>
                    </View>

                    <View>
                    <ScrollView>
                    <View style={{ marginTop: 20, marginVertical: 0, }}>


                        <Text style={styles.textoMenor}>EVENTOS QUE FUI</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <ScrollView horizontal={true}>
                                <View>
                                    <TouchableOpacity style={{ height: 150, width: 210 }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{  paddingHorizontal: 20, marginStart: 0, height: 160, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                            <View style={{ marginTop: -15, }} />
                                            <View>
                                                <Image source={require('../img/fundo_tela_1.png')}
                                                    style={styles.fotoRedondaHoriz} />
                                                <Image source={require('../img/filtro_background.png')}
                                                    style={styles.filtroRedondoHoriz} />

                                            </View>

                                            <View style={{ marginTop: -0, flexDirection: 'row', marginTop: -150 }} >
                                                <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={styles.textoDia}>22</Text>
                                                    <Text style={styles.textoData}>_____</Text>
                                                    <Text style={styles.textoData}>JUN</Text>
                                                </View>

                                                <View style={{ width: 90 }} />
                                                <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                                    <TouchableHighlight onPress={() => alert("Nenhuma função atribuida")} underlayColor="transparent">
                                                        <Image source={require('../img/coracao_vazio_white.png')}
                                                            style={styles.fotoMenu} />
                                                    </TouchableHighlight>

                                                </View>
                                            </View>

                                            <View style={{ height: 20 }} />

                                            <View style={{ width: 170, }}>

                                                <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                                                <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                                                <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>

                                            </View>

                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {/* ////////////////////////////////////////////////////////// */}
                                <View>
                                    <TouchableOpacity style={{ height: 150, width: 210 }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{  paddingHorizontal: 20, marginStart: 0, height: 160, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                            <View style={{ marginTop: -15, }} />
                                            <View>
                                                <Image source={require('../img/fundo_tela_1.png')}
                                                    style={styles.fotoRedondaHoriz} />
                                                <Image source={require('../img/filtro_background.png')}
                                                    style={styles.filtroRedondoHoriz} />

                                            </View>

                                            <View style={{ marginTop: -0, flexDirection: 'row', marginTop: -150 }} >
                                                <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={styles.textoDia}>22</Text>
                                                    <Text style={styles.textoData}>_____</Text>
                                                    <Text style={styles.textoData}>JUN</Text>
                                                </View>

                                                <View style={{ width: 90 }} />
                                                <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                                    <TouchableHighlight onPress={() => alert("Nenhuma função atribuida")} underlayColor="transparent">
                                                        <Image source={require('../img/coracao_vazio_white.png')}
                                                            style={styles.fotoMenu} />
                                                    </TouchableHighlight>

                                                </View>
                                            </View>

                                            <View style={{ height: 20 }} />

                                            <View style={{ width: 170, }}>

                                                <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                                                <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                                                <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>

                                            </View>

                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {/* ////////////////////////////////////////////////////////// */}
                                <View>
                                    <TouchableOpacity style={{ height: 150, width: 210 }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{  paddingHorizontal: 20, marginStart: 0, height: 160, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                            <View style={{ marginTop: -15, }} />
                                            <View>
                                                <Image source={require('../img/fundo_tela_1.png')}
                                                    style={styles.fotoRedondaHoriz} />
                                                <Image source={require('../img/filtro_background.png')}
                                                    style={styles.filtroRedondoHoriz} />

                                            </View>

                                            <View style={{ marginTop: -0, flexDirection: 'row', marginTop: -150 }} >
                                                <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={styles.textoDia}>22</Text>
                                                    <Text style={styles.textoData}>_____</Text>
                                                    <Text style={styles.textoData}>JUN</Text>
                                                </View>

                                                <View style={{ width: 90 }} />
                                                <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                                    <TouchableHighlight onPress={() => alert("Nenhuma função atribuida")} underlayColor="transparent">
                                                        <Image source={require('../img/coracao_vazio_white.png')}
                                                            style={styles.fotoMenu} />
                                                    </TouchableHighlight>

                                                </View>
                                            </View>

                                            <View style={{ height: 20 }} />

                                            <View style={{ width: 170, }}>

                                                <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                                                <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                                                <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>

                                            </View>

                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {/* ////////////////////////////////////////////////////////// */}
                                
                            </ScrollView>
                        </View>

                        <Text style={styles.textoMenor}>MEDALHAS</Text>
                        <Text style={styles.textoMenor}>3 Medalhas de conquista</Text>

                        <View style={{
                            flexDirection: 'column'
                        }}>
                            {/* <ScrollView horizontal={true}> */}
                            <View style={{
                                flexDirection: 'row', backgroundColor: 'transparent', width: '100%',
                                paddingHorizontal: '5%', paddingTop: '2%', alignItems: "center", justifyContent: "center"
                            }}>
                                <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                    <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../img/medalha_1.png')}
                                                style={styles.filtroMedalha} />
                                            <Text style={{
                                                color: '#fff',
                                                fontFamily: 'rockwell extra bold',
                                                fontSize: 17,
                                                textAlign: 'left',
                                                fontWeight: 'bold',
                                                fontFamily: "SFUIDisplay-Black",
                                                marginTop: 20,
                                            }}>10 eventos</Text>
                                        </View>

                                    </TouchableOpacity>
                                </View>
                                <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                    <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../img/medalha_1.png')}
                                                style={styles.filtroMedalha} />
                                            <Text style={{
                                                color: '#fff',
                                                fontFamily: 'rockwell extra bold',
                                                fontSize: 17,
                                                textAlign: 'left',
                                                fontWeight: 'bold',
                                                fontFamily: "SFUIDisplay-Black",
                                                marginTop: 20,
                                            }}>10 eventos</Text>
                                        </View>

                                    </TouchableOpacity>
                                </View>
                                <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                    <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../img/medalha_1.png')}
                                                style={styles.filtroMedalha} />
                                            <Text style={{
                                                color: '#fff',
                                                fontFamily: 'rockwell extra bold',
                                                fontSize: 17,
                                                textAlign: 'left',
                                                fontWeight: 'bold',
                                                fontFamily: "SFUIDisplay-Black",
                                                marginTop: 20,
                                            }}>10 eventos</Text>
                                        </View>

                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row', backgroundColor: 'transparent', width: '100%',
                                paddingHorizontal: '5%', paddingTop: '2%', alignItems: "center", justifyContent: "center"
                            }}>
                                <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                    <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../img/medalha_1.png')}
                                                style={styles.filtroMedalha} />
                                            <Text style={{
                                                color: '#fff',
                                                fontFamily: 'rockwell extra bold',
                                                fontSize: 17,
                                                textAlign: 'left',
                                                fontWeight: 'bold',
                                                fontFamily: "SFUIDisplay-Black",
                                                marginTop: 20,
                                            }}>10 eventos</Text>
                                        </View>

                                    </TouchableOpacity>
                                </View>
                                <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                    <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../img/medalha_1.png')}
                                                style={styles.filtroMedalha} />
                                            <Text style={{
                                                color: '#fff',
                                                fontFamily: 'rockwell extra bold',
                                                fontSize: 17,
                                                textAlign: 'left',
                                                fontWeight: 'bold',
                                                fontFamily: "SFUIDisplay-Black",
                                                marginTop: 20,
                                            }}>10 eventos</Text>
                                        </View>

                                    </TouchableOpacity>
                                </View>
                                <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                    <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  >
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../img/medalha_1.png')}
                                                style={styles.filtroMedalha} />
                                            <Text style={{
                                                color: '#fff',
                                                fontFamily: 'rockwell extra bold',
                                                fontSize: 17,
                                                textAlign: 'left',
                                                fontWeight: 'bold',
                                                fontFamily: "SFUIDisplay-Black",
                                                marginTop: 20,
                                            }}>10 eventos</Text>
                                        </View>

                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        <View style={{ height: 30 }} />
                    </View>
                </ScrollView>
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        // flexDirection: 'column',
        // justifyContent: 'center',
        backgroundColor: '#1c1c1c',
        // paddingStart: 37

    },
    botoesAreasBranco: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff'
    },
    botoesAreasCinza: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff',
        opacity: 0.4,
    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fundoRedondo: {
        height: 140,
        width: 140,
        borderRadius: 70,
        marginStart: 0,
        marginTop: 10,
    },
    fotoRetangular: {
        height: 300,
        width: 300,
        justifyContent: 'center',
        borderRadius: 15,
        // marginStart: 21,
        // marginTop: 50,
    },
    filtroRetangular: {
        height: 300,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 15,
        // marginStart: -100,
        // marginBottom: -100,
        marginTop: -300,
        marginRight: -300,
    },
    fotoQuadrada: {
        height: 180,
        width: 180,
        borderRadius: 10,
        marginEnd: 20,
        // marginTop: 50,
    },

});

export default Ingressos = StackNavigator({
    First: { screen: Ingressos },
    // Second: {screen: ViewDataUser },
    // Third: {screen: UpdeteDataUser }
})