import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import BotaoHome from '../components/BotaoHome';

class Ingressos extends Component {

    static navigationOptions = ({navigation}) => ({
        headerTintColor: 'white',
        tabBarVisible: true,
        headerBackTitle: null,
    });

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Nenhuma função atribuida");
    }

    // componentDidMount() {
    //     this.myScroll.scrollTo(...)
    // }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ paddingStart: 37 }}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}></Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    <View >
                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row', height: 30 }}>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Perfil', { name: 'Perfil' })}>Perfil </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Ingressos', { name: 'Ingressos' })}>Ingressos </Text>
                                <Text style={styles.botoesAreasBranco} onPress={() => navigate('Conquistas', { name: 'Conquistas' })}>Conquistas </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Configuracoes', { name: 'Configuracoes' })}>Configuraçoes </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('FaleConosco', { name: 'FaleConosco ' })}>Fale Conosco </Text>
                            </View>
                        </ScrollView>
                    </View>

                    {/* <View> */}
                    <ScrollView>
                        <View style={{ marginTop: 20, marginVertical: 0, }}>


                            <Text style={styles.textoMenor}>EVENTOS QUE FUI</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <ScrollView horizontal={true}>
                                    <View>
                                        <TouchableOpacity style={{ height: 150, width: 210 }} onPress={() => navigate('EventoQueFui', { name: 'EventoQueFui' })}  >
                                            <View style={{ paddingHorizontal: 20, marginStart: 0, height: 160, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                                <View style={{ marginTop: -15, }} />
                                                <View>
                                                    <Image source={require('../img/fundo_tela_1.png')}
                                                        style={styles.fotoRedondaHoriz} />
                                                    <Image source={require('../img/filtro_background.png')}
                                                        style={styles.filtroRedondoHoriz} />

                                                </View>

                                                <View style={{ marginTop: -0, flexDirection: 'row', marginTop: -150 }} >
                                                    <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                        <Text style={styles.textoDia}>22</Text>
                                                        <Text style={styles.textoData}>_____</Text>
                                                        <Text style={styles.textoData}>JUN</Text>
                                                    </View>

                                                    <View style={{ width: 90 }} />
                                                    <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                                        <TouchableOpacity onPress={() => alert("Nenhuma função atribuida")} underlayColor="transparent">
                                                            <Image source={require('../img/coracao_vazio_white.png')}
                                                                style={styles.fotoMenu} />
                                                        </TouchableOpacity>

                                                    </View>
                                                </View>

                                                <View style={{ height: 20 }} />

                                                <View style={{ width: 170, }}>

                                                    <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                                                    <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                                                    <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>

                                                </View>

                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    {/* ////////////////////////////////////////////////////////// */}
                                    <View>
                                        <TouchableOpacity style={{ height: 150, width: 210 }} onPress={() => navigate('EventoQueFui', { name: 'EventoQueFui' })}  >
                                            <View style={{ paddingHorizontal: 20, marginStart: 0, height: 160, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                                <View style={{ marginTop: -15, }} />
                                                <View>
                                                    <Image source={require('../img/fundo_tela_1.png')}
                                                        style={styles.fotoRedondaHoriz} />
                                                    <Image source={require('../img/filtro_background.png')}
                                                        style={styles.filtroRedondoHoriz} />

                                                </View>

                                                <View style={{ marginTop: -0, flexDirection: 'row', marginTop: -150 }} >
                                                    <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                        <Text style={styles.textoDia}>22</Text>
                                                        <Text style={styles.textoData}>_____</Text>
                                                        <Text style={styles.textoData}>JUN</Text>
                                                    </View>

                                                    <View style={{ width: 90 }} />
                                                    <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                                        <TouchableOpacity onPress={() => alert("Nenhuma função atribuida")} underlayColor="transparent">
                                                            <Image source={require('../img/coracao_vazio_white.png')}
                                                                style={styles.fotoMenu} />
                                                        </TouchableOpacity>

                                                    </View>
                                                </View>

                                                <View style={{ height: 20 }} />

                                                <View style={{ width: 170, }}>

                                                    <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                                                    <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                                                    <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>

                                                </View>

                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    {/* ////////////////////////////////////////////////////////// */}
                                    <View>
                                        <TouchableOpacity style={{ height: 150, width: 210 }} onPress={() => navigate('EventoQueFui', { name: 'EventoQueFui' })}  >
                                            <View style={{ paddingHorizontal: 20, marginStart: 0, height: 160, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                                <View style={{ marginTop: -15, }} />
                                                <View>
                                                    <Image source={require('../img/fundo_tela_1.png')}
                                                        style={styles.fotoRedondaHoriz} />
                                                    <Image source={require('../img/filtro_background.png')}
                                                        style={styles.filtroRedondoHoriz} />

                                                </View>

                                                <View style={{ marginTop: -0, flexDirection: 'row', marginTop: -150 }} >
                                                    <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                        <Text style={styles.textoDia}>22</Text>
                                                        <Text style={styles.textoData}>_____</Text>
                                                        <Text style={styles.textoData}>JUN</Text>
                                                    </View>

                                                    <View style={{ width: 90 }} />
                                                    <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                                        <TouchableOpacity onPress={() => alert("Nenhuma função atribuida")} underlayColor="transparent">
                                                            <Image source={require('../img/coracao_vazio_white.png')}
                                                                style={styles.fotoMenu} />
                                                        </TouchableOpacity>

                                                    </View>
                                                </View>

                                                <View style={{ height: 20 }} />

                                                <View style={{ width: 170, }}>

                                                    <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                                                    <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                                                    <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>

                                                </View>

                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    {/* ////////////////////////////////////////////////////////// */}

                                </ScrollView>
                            </View>

                            <Text style={styles.textoMenor}>MEDALHAS</Text>
                            <Text style={styles.textoMenor}>3 Medalhas de conquista</Text>

                            <View style={{
                                flexDirection: 'column'
                            }}>
                                {/* <ScrollView horizontal={true}> */}
                                <View style={{
                                    flexDirection: 'row', backgroundColor: 'transparent', width: '100%',
                                    paddingHorizontal: '5%', paddingTop: '2%', alignItems: "center", justifyContent: "center"
                                }}>
                                    <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                        <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('Medalha', { name: 'Medalha' })}  >
                                            <View style={{ alignItems: "center", justifyContent: "center" }}>
                                                <Image source={require('../img/medalha_1.png')}
                                                    style={styles.filtroMedalha} />
                                                <Text style={{
                                                    color: '#fff',
                                                    fontFamily: 'rockwell extra bold',
                                                    fontSize: 17,
                                                    textAlign: 'left',
                                                    fontWeight: 'bold',
                                                    fontFamily: "SFUIDisplay-Black",
                                                    marginTop: 20,
                                                }}>10 eventos</Text>
                                            </View>

                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                        <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('Medalha', { name: 'Medalha' })}  >
                                            <View style={{ alignItems: "center", justifyContent: "center" }}>
                                                <Image source={require('../img/medalha_1.png')}
                                                    style={styles.filtroMedalha} />
                                                <Text style={{
                                                    color: '#fff',
                                                    fontFamily: 'rockwell extra bold',
                                                    fontSize: 17,
                                                    textAlign: 'left',
                                                    fontWeight: 'bold',
                                                    fontFamily: "SFUIDisplay-Black",
                                                    marginTop: 20,
                                                }}>10 eventos</Text>
                                            </View>

                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                        <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('Medalha', { name: 'Medalha' })}  >
                                            <View style={{ alignItems: "center", justifyContent: "center" }}>
                                                <Image source={require('../img/medalha_1.png')}
                                                    style={styles.filtroMedalha} />
                                                <Text style={{
                                                    color: '#fff',
                                                    fontFamily: 'rockwell extra bold',
                                                    fontSize: 17,
                                                    textAlign: 'left',
                                                    fontWeight: 'bold',
                                                    fontFamily: "SFUIDisplay-Black",
                                                    marginTop: 20,
                                                }}>10 eventos</Text>
                                            </View>

                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {/* <View style={{ marginBottom:20}}></View> */}
                                <View style={{
                                    flexDirection: 'row', backgroundColor: 'transparent', width: '100%',
                                    paddingHorizontal: '5%', paddingTop: '2%', alignItems: "center", justifyContent: "center"
                                }}>
                                    <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                        <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('Medalha', { name: 'Medalha' })}  >
                                            <View style={{ alignItems: "center", justifyContent: "center" }}>
                                                <Image source={require('../img/medalha_1.png')}
                                                    style={styles.filtroMedalha} />
                                                <Text style={{
                                                    color: '#fff',
                                                    fontFamily: 'rockwell extra bold',
                                                    fontSize: 17,
                                                    textAlign: 'left',
                                                    fontWeight: 'bold',
                                                    fontFamily: "SFUIDisplay-Black",
                                                    marginTop: 20,
                                                }}>10 eventos</Text>
                                            </View>

                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                        <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('Medalha', { name: 'Medalha' })}  >
                                            <View style={{ alignItems: "center", justifyContent: "center" }}>
                                                <Image source={require('../img/medalha_1.png')}
                                                    style={styles.filtroMedalha} />
                                                <Text style={{
                                                    color: '#fff',
                                                    fontFamily: 'rockwell extra bold',
                                                    fontSize: 17,
                                                    textAlign: 'left',
                                                    fontWeight: 'bold',
                                                    fontFamily: "SFUIDisplay-Black",
                                                    marginTop: 20,
                                                }}>10 eventos</Text>
                                            </View>

                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ backgroundColor: 'transparent', width: '34%' }}>
                                        <TouchableOpacity style={{ height: 150, width: '100%' }} onPress={() => navigate('Medalha', { name: 'Medalha' })}  >
                                            <View style={{ alignItems: "center", justifyContent: "center" }}>
                                                <Image source={require('../img/medalha_1.png')}
                                                    style={styles.filtroMedalha} />
                                                <Text style={{
                                                    color: '#fff',
                                                    fontFamily: 'rockwell extra bold',
                                                    fontSize: 17,
                                                    textAlign: 'left',
                                                    fontWeight: 'bold',
                                                    fontFamily: "SFUIDisplay-Black",
                                                    marginTop: 20,
                                                }}>10 eventos</Text>
                                            </View>

                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>

                            <View style={{ marginBottom: 90 }} />
                        </View>
                    </ScrollView>
                    {/* </View> */}
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        // flexDirection: 'column',
        // justifyContent: 'center',
        backgroundColor: '#1c1c1c',
        // paddingStart: 37

    },
    botoesAreasBranco: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff'
    },
    botoesAreasCinza: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff',
        opacity: 0.4,
    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fundoRedondo: {
        height: 140,
        width: 140,
        borderRadius: 70,
        marginStart: 0,
        marginTop: 10,
    },
    fotoRetangular: {
        height: 300,
        width: 300,
        justifyContent: 'center',
        borderRadius: 15,
        // marginStart: 21,
        // marginTop: 50,
    },
    filtroRetangular: {
        height: 300,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 15,
        // marginStart: -100,
        // marginBottom: -100,
        marginTop: -300,
        marginRight: -300,
    },
    fotoQuadrada: {
        height: 180,
        width: 180,
        borderRadius: 10,
        marginEnd: 20,
        // marginTop: 50,
    },
    fotoRedondaHoriz: {
        height: 150,
        width: 200,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    filtroRedondoHoriz: {
        height: 150,
        width: 200,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -150,
    },
    filtroMedalha: {
        height: 80,
        width: 80,
        justifyContent: 'center',
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#FF9D00',
    },
    textoData: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 18,
        textAlign: "left",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 26,
        textAlign: "left",
        fontWeight: 'bold',
        marginBottom: -15,
        justifyContent: 'flex-start',
    },
    textoNomeEvento: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 20,
        textAlign: "left",
    },
    textoLocalizacao: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 10,
        textAlign: "left",
    },
    textoCidade: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 10,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoMenor: {
        color: '#fff',
        fontSize: 21,
        textAlign: 'left',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
        marginTop: 20,
    },

});

export default Ingressos ;
// = StackNavigator({
//     First: { screen: Ingressos },
//     // Second: {screen: ViewDataUser },
//     // Third: {screen: UpdeteDataUser }
// })

// container: {
//     //padding: 20,
//     flex: 1,
//     // justifyContent: 'space-between',
//     // paddingHorizontal: 15,
//     // paddingTop: 48,
//     // paddingBottom: 20,color: '#fff'
//     // alignItems: 'center',
//     backgroundColor: '#1c1c1c', //#026
// },


// foto: {
//     height: 40,
//     width: 40,
//     borderRadius: 20,
//     marginRight: -25,
//     marginVertical: 20,
//     // elevation: 10,
//     // shadowColor: '#202020',
//     // shadowOffset: { width: 10, height: 10 },
//     // shadowRadius: 40,
// },
// fotoRedondaVert: {
//     height: 200,
//     width: 150,
//     justifyContent: 'center',
//     borderRadius: 17.23,
//     marginTop: -0,
// },
// filtroRedondoVert: {
//     height: 200,
//     width: 150,
//     justifyContent: 'center',
//     borderRadius: 17.23,
//     marginTop: -200,
// },

// textoTitulo: {
//     color: '#fff',
//     // fontFamily: 'rockwell extra bold',
//     fontSize: 20,
//     textAlign: 'center',
//     fontWeight: 'bold',
//     fontFamily: "SFUIDisplay-Black",
//     opacity: 1,
//     marginTop: -40,
//     paddingTop: -20
// },
