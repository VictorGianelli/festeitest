import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
// import { FontAwesome} from '@expo/vector-icons';
import { StackActions, NavigationActions } from 'react-navigation';


import BotaoHome from '../components/BotaoHome';

class Ingressos extends Component {
    static navigationOptions = ({navigation}) => ({
        tabBarVisible: false,
        headerTintColor: 'white',
        headerBackTitle: null,
    });

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
          });
        this.props.navigation.dispatch(resetAction);
        return true;
    }

    onBackButtonPressAndroid = () => {
        // if (this.isSelectionModeEnabled()) {
        //   this.disableSelectionMode();
          alert("a");  
        //   return true;
        // } else {
        //     alert("b");
        //   return false;
        // }
      };
    
   
    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ paddingStart: 37 }}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}></Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    <View >
                        <ScrollView horizontal={true}>
                        <View style={{ flexDirection: 'row', height:30 }}>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Perfil', { name: 'Perfil' })}>Perfil </Text>
                                <Text style={styles.botoesAreasBranco} onPress={() => navigate('Ingressos', { name: 'Ingressos' })}>Ingressos </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Conquistas', { name: 'Conquistas' })}>Conquistas </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Configuracoes', { name: 'Configuracoes' })}>Configuraçoes </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('FaleConosco', { name: 'FaleConosco ' })}>Fale Conosco </Text>
                            </View>
                        </ScrollView>
                    </View>
                    {/* <Icon name="arrow-left" size={70} color="#fff"/> */}
                    <View>
                        <ScrollView>
                            <View style={{ alignItems: 'flex-start', marginTop: 20, marginVertical: 0 }}>
                                {/* <View style={{  }}> */}
                                <Text style={{
                                    color: '#fff',
                                    fontFamily: 'rockwell extra bold',
                                    fontSize: 25,
                                    textAlign: "left",
                                    fontWeight: 'bold',
                                }}>Happy Holi</Text>
                                <Text style={{
                                    color: '#fff',
                                    fontFamily: 'rockwell extra bold',
                                    fontSize: 20,
                                    textAlign: "left",
                                }}>2 ingressos</Text>
                                {/* style={{ backgroudColor: '#F00' }} */}
                                <View>
                                    <TouchableOpacity style={{}} onPress={() => navigate('IngressoAberto', { name: 'IngressoAberto' })}  >
                                        <View style={{ paddingStart: 7.5, paddingEnd: 7.5 }}>

                                            <View style={{ width: 600, flexDirection: 'row' }}>
                                                {/* <View> */}
                                                <Image source={require('../img/fundo_tela_1.png')}
                                                    style={styles.fotoRetangular} />
                                                <Image source={require('../img/filtro_background.png')}
                                                    style={styles.filtroRetangular} />
                                                {/* </View> */}

                                                <View style={{
                                                    justifyContent: 'center', alignItems: 'center',
                                                }}>

                                                    <View style={{
                                                        height: 150, width: 300, padding: 20, borderWidth: 0,
                                                        borderColor: '#FF9D00',
                                                        backgroundColor: '#262626', borderRadius: 15,
                                                        flexDirection: 'column', marginStart: -75,
                                                        transform: [{ rotate: '270deg' }]
                                                    }}>

                                                        <View style={{ marginBottom: 20 }}>
                                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 7 }}>
                                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Black", fontSize: 14 }}>1° Lote </Text>
                                                                <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Medium", fontSize: 14 }}>R$ 50,00 (+2,50 taxa)</Text>
                                                            </View>
                                                            <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Light", fontSize: 16 }}>Nome Sobrenome Completo</Text>
                                                            <Text style={{ color: '#F15A24', fontFamily: "SFUIDisplay-Bold", fontSize: 14 }}>CPF 123.456.789-10</Text>
                                                            <View style={{ alignItems: 'baseline', paddingTop: 17 }}>
                                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>(DDD) 9.9999-9999</Text>
                                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>email@domínio.com</Text>
                                                            </View>
                                                        </View>

                                                        <View>
                                                            <Text style={{ color: '#fff', fontSize: 50, marginVertical: -200 }}>- - - - - - - - - -</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                                   
                                    </TouchableOpacity>
                                </View>
                                {/* ////////////////////////////////////////////////////////// */}
                                <View>

                                </View>
                                {/* </View> */}

                            </View>
                        </ScrollView>
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        // flexDirection: 'column',
        // justifyContent: 'center',
        backgroundColor: '#1c1c1c',
        // paddingStart: 37

    },
    botoesAreasBranco: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff'
    },
    botoesAreasCinza: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff',
        opacity: 0.4,
    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fundoRedondo: {
        height: 140,
        width: 140,
        borderRadius: 70,
        marginStart: 0,
        marginTop: 10,
    },
    fotoRetangular: {
        height: 300,
        width: 300,
        justifyContent: 'center',
        borderRadius: 15,
        // marginStart: 21,
        // marginTop: 50,
    },
    filtroRetangular: {
        height: 300,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 15,
        // marginStart: -100,
        // marginBottom: -100,
        marginTop: -300,
        marginRight: -300,
    },
    fotoQuadrada: {
        height: 180,
        width: 180,
        borderRadius: 10,
        marginEnd: 20,
        // marginTop: 50,
    },

});

export default Ingressos;
//  = StackNavigator({
//     First: { screen: Ingressos },
//     // Second: { screen: ViewDataUser },
//     // Third: { screen: UpdeteDataUser }
//   },{navigationOptions: {
//     headerTransparent: true,
//     headerTintColor: 'white'
//   }})