import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import BotaoHome from '../components/BotaoHome';

class FotoPerfil extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Nenhuma função atribuida");
    }


    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ paddingStart: 37 }}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}></Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    <View>
                        <ScrollView horizontal={true} >
                            <View style={{ flexDirection: 'row', height: 30 }}>
                                <Text style={styles.botoesAreasBranco} onPress={() => alert("Link1")}>Perfil </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => alert("Link2")}>Ingrassos </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => alert("Link3")}>Conquistas </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => alert("Link4")}>Configuraçoes </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => alert("Link5")}>Fale Conosco </Text>
                            </View>
                        </ScrollView>
                    </View>

                    {/* <View  style={{backgroundColor: '#F15A24', height: 400}}> */}
                    <View >
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%', marginTop: 10 }}>
                            <Image source={require('../img/fundo_imagem_perfil.png')}
                                style={styles.fundoRedondo} />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%', marginTop: -270, opacity: 1 }}>
                            <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                style={styles.fotoRedonda} />
                        </View>

                        <View style={{ flexDirection: 'column', alignItems: 'flex-end', justifyContent: 'flex-end', marginBottom: 0 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', paddingStart: '35%' }}>
                                <View style={{ width: 1.5, height: 40, backgroundColor: '#F15A24', flexDirection: 'row', marginTop: 15, justifyContent: 'center', alignItems: 'flex-end' }} />
                                <Text style={{ color: '#fff' }} > lv</Text>
                                <Text style={{ color: '#fff' }} > 10</Text>
                            </View>
                            <View>
                                <Text style={{ color: '#fff' }} >120/800</Text>
                            </View>
                        </View>
                    </View>
                    {/* <View style={{ marginTop: -350, opacity: 0.3, marginRight: -500, backgroundColor: 'transparent' }}>
                        <View style={{ height: 300, width: 300, marginTop: 0, marginLeft: 0, backgroundColor: '#F15A24', alignItems: 'center', justifyContent: 'center'}}>
                            <TouchableOpacity style={{ height: 100, width: 100, borderRadius: 10, marginEnd: 0 }} onPress={() => alert("Foto de perfil")} >
                                <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                    style={styles.fotoRedonda} />
                            </TouchableOpacity>
                        </View>
                    </View> */}

                    {/*
                    {/*#F15A24  transparent 
                        {/* navigate('NovaConta', { name: 'NovaConta' })} > 
                    */}

                    <View style={{ width: '90%', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <View style={{ width: '30%', height: 3, backgroundColor: '#F15A24', flexDirection: 'row', marginTop: 40, justifyContent: 'center', alignItems: 'center' }} />
                        <View style={{ width: '70%', height: 3, backgroundColor: '#fff', opacity: .2, flexDirection: 'row', marginTop: 40, justifyContent: 'center', alignItems: 'center' }} />
                    </View>

                    <View style={{ marginTop: +20 }}>
                        <Text style={{ color: '#fff' }}>NOME SOBRENOME</Text>
                        <Text style={{ color: '#fff' }}>22 ANOS</Text>
                        <Text style={{ color: '#fff' }}>Belo Horizonte - MG</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        // flexDirection: 'column',
        // justifyContent: 'center',
        backgroundColor: '#1c1c1c',
        // paddingStart: 37

    },
    botoesAreasBranco: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff'
    },
    botoesAreasCinza: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff',
        opacity: 0.4,
    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fundoRedondo: {
        height: 300,
        width: 300,
        borderRadius: 150,
        marginStart: 0,
        marginTop: 10,
    },
    fotoRedonda: {
        height: 240,
        width: 240,
        borderRadius: 120,
        // marginStart: 21,
        // marginTop: 50,
    },
    fotoQuadrada: {
        height: 180,
        width: 180,
        borderRadius: 10,
        marginEnd: 20,
        // marginTop: 50,
    },

});

export default FotoPerfil;