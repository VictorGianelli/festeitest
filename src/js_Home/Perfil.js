import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    BackHandler,
    TouchableOpacity,
    Image,
    ScrollView,
    View
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

class Perfil extends Component {

    static navigationOptions = ({navigation}) => ({
        tabBarVisible: false,
        headerTintColor: 'white',
        headerBackTitle: null,
    });

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
          });
        this.props.navigation.dispatch(resetAction);
        return true;
    }

    
    render() {
        const { navigate } = this.props.navigation
        
        return (
            <View style={styles.container}>
                <View style={{ paddingStart: 37 }}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}></Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    <View>
                        <ScrollView horizontal={true} >
                            <View style={{ flexDirection: 'row', height:30 }}>
                                <Text style={styles.botoesAreasBranco} onPress={() => navigate('Perfil', { name: 'Perfil' })}>Perfil </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Ingressos', { name: 'Ingressos' })}>Ingressos </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Conquistas', { name: 'Conquistas' })}>Conquistas </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Configuracoes', { name: 'Configuracoes' })}>Configuraçoes </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('FaleConosco', { name: 'FaleConosco ' })}>Fale Conosco </Text>
                            </View>
                        </ScrollView>
                    </View>
                    <View>
                        <View style={{ flexDirection: 'row', width: '100%',marginTop: 10 }}>
                            <Image source={require('../img/fundo_imagem_perfil.png')}
                                style={styles.fundoRedondo} />
                            <View style={{ flexDirection: 'column', alignItems: 'flex-end', justifyContent: 'flex-end', marginBottom: 0 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end', paddingStart: '35%' }}>
                                    <View style={{ width: 1.5, height: 40, backgroundColor: '#F15A24', flexDirection: 'row', marginTop: 15, justifyContent: 'center', alignItems: 'flex-end' }} />
                                    <Text style={{ color: '#fff' }} > lv</Text>
                                    <Text style={{ color: '#fff' }} > 10</Text>
                                </View>
                                <View style={{}}>
                                    <Text style={{ color: '#fff' }} >120/800</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ marginTop: -170, marginRight: -500 }}>
                            <View style={{height: 100, width: 100,marginTop:50,marginLeft:20,backgroundColor: 'transparent', }}>{/* #F15A24  transparent*/}
                                <TouchableOpacity style={{ height: 100, width: 100, borderRadius: 10, marginEnd: 0, }} onPress={() => navigate('FotoPerfil', { name: 'FotoPerfil' })} > 
                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                        style={styles.fotoRedonda} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        {/* <View style={{ flexDirection: 'row', width: '100%' }}>

                    </View> */}
                    </View>

                    <View style={{ width: '90%', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <View style={{ width: '30%', height: 3, backgroundColor: '#F15A24', flexDirection: 'row', marginTop: 40, justifyContent: 'center', alignItems: 'center' }} />
                        <View style={{ width: '70%', height: 3, backgroundColor: '#fff', opacity: .2, flexDirection: 'row', marginTop: 40, justifyContent: 'center', alignItems: 'center' }} />
                    </View>

                    <View style={{ marginTop: +20 }}>
                        <Text style={{ color: '#fff' }}>NOME SOBRENOME</Text>
                        <Text style={{ color: '#fff' }}>22 ANOS</Text>
                        <Text style={{ color: '#fff' }}>Belo Horizonte - MG</Text>
                    </View>
                    <View style={{ paddingTop: 20 }}>
                        <Text style={{ color: '#fff' }}>@nickname</Text>
                    </View>
                </View>
                <View>
                    <ScrollView horizontal={true}>
                        <View>
                            <View style={{ marginStart: 37, flexDirection: 'row' }}>
                                <View style={{ paddingTop: 20 }} />
                                <View>
                                    <TouchableOpacity onPress={() => navigate('Fotos', { name: 'Fotos' })} >{/* navigate('NovaConta', { name: 'NovaConta' })} > */}
                                        <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                            style={styles.fotoQuadrada} />
                                    </TouchableOpacity>
                                </View>

                                <View>
                                    <TouchableOpacity onPress={() => navigate('Fotos', { name: 'Fotos' })} >{/* navigate('NovaConta', { name: 'NovaConta' })} > */}
                                        <Image source={require('../img/FotoQuadrada/foto2.jpg')}
                                            style={styles.fotoQuadrada} />
                                    </TouchableOpacity>
                                </View>

                                <View>
                                    <TouchableOpacity onPress={() => navigate('Fotos', { name: 'Fotos' })} >{/* navigate('NovaConta', { name: 'NovaConta' })} > */}
                                        <Image source={require('../img/FotoQuadrada/foto3.jpg')}
                                            style={styles.fotoQuadrada} />
                                    </TouchableOpacity>
                                </View>

                                <View>
                                    <TouchableOpacity onPress={() => navigate('Fotos', { name: 'Fotos' })} >{/* navigate('NovaConta', { name: 'NovaConta' })} > */}
                                        <Image source={require('../img/FotoQuadrada/foto4.jpg')}
                                            style={styles.fotoQuadrada} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        // flexDirection: 'column',
        // justifyContent: 'center',
        backgroundColor: '#1c1c1c',
        // paddingStart: 37

    },
    botoesAreasBranco: {
        fontSize: 20, 
        fontWeight: 'bold', 
        paddingEnd: 15,
        color: '#fff'
    },
    botoesAreasCinza: {
        fontSize: 18, 
        fontWeight: 'bold', 
        paddingEnd: 15,
        color: '#fff',
        opacity: 0.4,
    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fundoRedondo: {
        height: 140,
        width: 140,
        borderRadius: 70,
        marginStart: 0,
        marginTop: 10,
    },
    fotoRedonda: {
        height: 100,
        width: 100,
        borderRadius: 50,
        // marginStart: 21,
        // marginTop: 50,
    },
    fotoQuadrada: {
        height: 180,
        width: 180,
        borderRadius: 10,
        marginEnd: 20,
        // marginTop: 50,
    },

});

export default Perfil;
