import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert,
    ScrollView,
    Platform,
    Image,
    FlatList,
    TouchableOpacity,
    Button
} from 'react-native';
import BotaoTeste from '../components/BotaoTeste';
import BotaoComTexto from '../components/BotaoComTexto';
import Botao from '../components/Botao';
import { connect } from 'react-redux';
import { inserirDadosIngressos, inserirStrValTotal } from '../actions/AuthActions';
// import DadosIngresso from './DadosIngresso';

import Swiper from 'react-native-swiper';

class ValorTotal extends Component {
    render() {
        return (
            <View>
                <Text style={styles.textoValor}>R$ {this.props.strValueTotal}</Text>
            </View>
        );
    }
}

export class Comprar extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            empyt: [],
            eventos: [
                {
                    id: 0,
                    data: 22,
                    mes: "JUN",
                    numero: 0,
                    preco: 25.00,
                    taxa: 2.50,
                    strPreco: "25,00",
                    strTaxa: "2,50",
                    total: 0,
                    tipo: "1º LOTE",
                    border: 0
                },
                {
                    id: 1,
                    data: 22,
                    mes: "JUN",
                    numero: 0,
                    preco: 30,
                    taxa: 2.25,
                    strPreco: "30,00",
                    strTaxa: "2,25",
                    total: 0,
                    tipo: "VIP",
                    border: 2
                }
            ],
            cont: 0,
            valTotal: 0,
            strValTotal: this.props.strValueTotal,
            opacity: 0.5,
            enabled: true,
            numeroTotal: 0,
            newName: ""
        };
    }

    onChange = (newName) => {

        this.setState({
            strValueTotal: newName
        })
    }

    render() {
        let state = this.state;
        if (state.strTotal == undefined ? state.strTotal = "" : null);
        // if (state.newName == undefined ? state.newName = "" : null);
        // if (this.props.strValueTotal == undefined ? this.props.strValueTotal = "" : this.props.strValueTotal = "" );
        // this.onChange(state.newName);
        const { navigate } = this.props.navigation
        return (
            <View style={styles.MainContainer}>

                <View style={styles.margem}>

                    <Swiper
                        horizontal={true}
                        paginationStyle={{ bottom: -1 }}
                        dot={<View style={{ backgroundColor: '#555', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                        activeDot={<View style={{ backgroundColor: '#ED1E79', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                    >

                        {
                            this.state.eventos.map((item, key) => (
                                // onPress={this.showArrayItem.bind(this, String(item.age))}

                                <View key={item.id} style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0 }}>
                                    {/* Parte de cima */}
                                    <View style={{ borderWidth: item.border, borderColor: '#FF9D00', borderBottomLeftRadius: 17.23, borderBottomRightRadius: 17.23 }}>
                                        <Image source={require('../img/fundo_tela_evento.png')}
                                            style={styles.fotoRedonda} />
                                        <Image source={require('../img/filtro_background.png')}
                                            style={styles.filtroRedondo} />
                                        <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={styles.textoDia}>{item.data}</Text>
                                                <Text style={styles.textoBarra}>___</Text>
                                                <Text style={styles.textoMes}>{item.mes}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                                                <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                                                <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                                                <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>
                                            </View>
                                        </View>
                                    </View>
                                    {/* Parte de baixo */}
                                    <View style={{ borderWidth: item.border, borderColor: '#FF9D00', padding: 20, height: 150, width: 300, backgroundColor: '#262626', borderRadius: 15, flexDirection: 'column' }}>
                                            {/* <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100 }}> */}
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 7 }}>{/* flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100  */}
                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Black", fontSize: 14 }}>{item.tipo}</Text>
                                                <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Medium", fontSize: 14 }}>R$ {item.preco} (+{item.taxa})</Text>
                                            </View>
                                            <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Light", fontSize: 16 }}>Nome Sobrenome Completo</Text>
                                            <Text style={{ color: '#F15A24', fontFamily: "SFUIDisplay-Bold", fontSize: 14 }}>CPF 123.456.789-10</Text>
                                            <View style={{ alignItems: 'baseline', paddingTop: 17 }}>
                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>(DDD) 9.9999-9999</Text>
                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>email@domínio.com</Text>
                                            </View>
                                        </View>

                                        <View>
                                            <Text style={{ color: '#fff', fontSize: 50, marginVertical: -189 }}>- - - - - - - - - -</Text>
                                        </View>
                                        <View style={{ height: 50, width: 300 }}></View>
                                    </View>

                                    ))
                                }
        
                    </Swiper>

                                <View style={{ alignItems: 'center' }}>
                                    <Image source={require('../img/qrCode_sample.png')}
                                    />
                                </View>
                    
                </View>
        
            </View>
                    );
                }
            }
            
const styles = StyleSheet.create({

                        MainContainer: {
                        flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    backgroundColor: '#1c1c1c'
                },
    TextStyle: {
                        fontSize: 20,
                    color: '#000',
                    textAlign: 'left'
                },
            
    margem: {
                        flex: 1,
                    width: 305
                },
    switchView: {
                        width: 350,
                    height: 450
                },
    texto: {
                        color: '#fff',
                    fontFamily: 'arial',
                    fontSize: 20,
                    marginStart: 50,
                    textAlign: "left",
                },
    textoTotal: {
                        color: '#fff',
                    fontSize: 18,
                    marginStart: 20,
                    textAlign: "left",
                    fontFamily: "SFuidisplay-medium",
                    alignItems: 'center'
                },
    textoValor: {
                        color: '#F15A24',
                    fontSize: 35,
                    marginStart: 20,
                    textAlign: "left",
                    fontFamily: "SFuidisplay-medium",
                    alignItems: 'center'
                },
    textoGrande: {
                        color: '#fff',
                    // fontFamily: 'rockwell extra bold',
                    // fontSize: 50,
                    textAlign: "left",
                    // marginStart: 50,
                    // marginEnd: 50,
                    fontWeight: 'bold',
                },
    backgroundImage: {
                        width: '100%',
                    flex: 1,
                    resizeMode: 'stretch',
                    //backgroundColor: 'rgba(0, 0, 0, 1.0)',
                },
    fotoRedonda: {
                        height: 533.6,
                    width: 300,
                    resizeMode: "stretch",
                    justifyContent: 'center',
                    borderRadius: 17.23,
                    marginTop: -333.6,
                    // marginVertical: 20,
                    // borderWidth: 2,borderColor: '#FF9D00'
                    // elevation: 10,
                    // shadowColor: '#202020',
        // shadowOffset: {width: 10, height: 10 },
                    // shadowRadius: 40,
                },
    filtroRedondo: {
                        height: 533.6,
                    width: 300,
                    resizeMode: "stretch",
                    justifyContent: 'center',
                    borderRadius: 17.23,
                    marginTop: -533.6,
                    // marginVertical: 20,
            
                    // elevation: 10,
                    // shadowColor: '#202020',
        // shadowOffset: {width: 10, height: 10 },
                    // shadowRadius: 40,
                },
            
    textoMes: {
                        color: '#fff',
                    fontFamily: "SFuidisplay-Black",
                    fontSize: 30,
                    textAlign: "center",
                    fontWeight: 'bold',
                    justifyContent: 'center',
                },
    textoBarra: {
                        color: '#fff',
                    fontFamily: 'rockwell extra bold',
                    fontSize: 30,
                    textAlign: "center",
                    fontWeight: 'bold',
                    justifyContent: 'center',
                },
    textoDia: {
                        color: '#fff',
                    fontFamily: "SFuidisplay-Medium",
                    fontSize: 40,
                    textAlign: "center",
                    fontWeight: 'bold',
                    marginBottom: -35,
                    justifyContent: 'center',
                },
    textoNomeEvento: {
                        color: '#fff',
                    fontFamily: 'rockwell extra bold',
                    fontSize: 40,
                    textAlign: "left",
                },
    textoLocalizacao: {
                        color: '#fff',
                    fontFamily: 'arial',
                    fontSize: 20,
                    textAlign: "left",
                },
    textoCidade: {
                        color: '#fff',
                    fontFamily: 'rockwell extra bold',
                    fontSize: 15,
                    textAlign: "left",
                    fontWeight: 'bold',
                },
            
            });
            
const mapStateToProps = (state) => {
    return {
                        dadosIngressos: state.auth.dadosIngressos,
                    strValueTotal: state.auth.strValueTotal
                }
            };
            
const ComprarConnct = connect(mapStateToProps, {inserirDadosIngressos, inserirStrValTotal })(Comprar);
                    
export default ComprarConnct;