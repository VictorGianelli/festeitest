import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    Swiper,
    ScrollView,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import BotaoHome from '../components/BotaoHome';

class Fotos extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            fotos: [
                {
                    id: 0,
                    image: '../img/FotoQuadrada/foto1.jpg'
                },
                {
                    id: 1,
                    image: '../img/FotoQuadrada/foto2.jpg'
                }
            ],
        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Nenhuma função atribuida");
    }


    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ paddingStart: 37 }}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}></Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    <View style={{ paddingTop: 20 }}>
                        <Text style={{ color: '#fff' }}>@nickname</Text>
                    </View>
                </View>

                {/* <Swiper
                    horizontal={true}
                    paginationStyle={{ bottom: -1 }}
                    dot={<View style={{ backgroundColor: '#555', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                    activeDot={<View style={{ backgroundColor: '#ED1E79', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                >

                    {
                        this.state.fotos.map((item, key) => (
                            // onPress={this.showArrayItem.bind(this, String(item.age))}
                            // <View style={{ marginTop: -150}}>
                            // <View key={item.id} style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0, marginTop: -135 }}>
                            <View key={item.id} >
                                {/* <Image source={require('item.image')}
                                            style={styles.fotoQuadrada} /> 
                                <Text>item.image</Text>            
                            </View>

                        ))
                    }

                </Swiper> */}
                <View>
                    <ScrollView horizontal={true}>
                        <View>
                            <View style={{ marginStart: 37, flexDirection: 'row' }}>
                                <View style={{ paddingTop: 20 }} />
                                <View>
                                    <TouchableOpacity onPress={() => alert("Foto do Insta")} >{/* navigate('NovaConta', { name: 'NovaConta' })} > */}
                                        <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                            style={styles.fotoQuadrada} />
                                    </TouchableOpacity>
                                </View>

                                <View>
                                    <TouchableOpacity onPress={() => alert("Foto do Insta")} >{/* navigate('NovaConta', { name: 'NovaConta' })} > */}
                                        <Image source={require('../img/FotoQuadrada/foto2.jpg')}
                                            style={styles.fotoQuadrada} />
                                    </TouchableOpacity>
                                </View>

                                <View>
                                    <TouchableOpacity onPress={() => alert("Foto do Insta")} >{/* navigate('NovaConta', { name: 'NovaConta' })} > */}
                                        <Image source={require('../img/FotoQuadrada/foto3.jpg')}
                                            style={styles.fotoQuadrada} />
                                    </TouchableOpacity>
                                </View>

                                <View>
                                    <TouchableOpacity onPress={() => alert("Foto do Insta")} >{/* navigate('NovaConta', { name: 'NovaConta' })} > */}
                                        <Image source={require('../img/FotoQuadrada/foto4.jpg')}
                                            style={styles.fotoQuadrada} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        // flexDirection: 'column',
        // justifyContent: 'center',
        backgroundColor: '#1c1c1c',
        // paddingStart: 37

    },
    botoesAreasBranco: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff'
    },
    botoesAreasCinza: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff',
        opacity: 0.4,
    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fundoRedondo: {
        height: 140,
        width: 140,
        borderRadius: 70,
        marginStart: 0,
        marginTop: 10,
    },
    fotoRedonda: {
        height: 100,
        width: 100,
        borderRadius: 50,
        // marginStart: 21,
        // marginTop: 50,
    },
    fotoQuadrada: {
        height: 180,
        width: 180,
        borderRadius: 10,
        marginEnd: 20,
        // marginTop: 50,
    },

});

export default Fotos;