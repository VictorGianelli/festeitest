import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Picker,
    ScrollView,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import Botao from '../components/Botao';

class FaleConosco extends Component {

    static navigationOptions = ({navigation}) => ({
        headerTintColor: 'white',
        tabBarVisible: true,
        headerBackTitle: null,
    });

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Nenhuma função atribuida");
    }

    // componentDidMount() {
    //     this.myScroll.scrollTo(...)
    // }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View style={{ paddingStart: 37 }}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}></Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    <View >
                        <ScrollView horizontal={true}>
                        <View style={{ flexDirection: 'row', height:30 }}>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Perfil', { name: 'Perfil' })}>Perfil </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Ingressos', { name: 'Ingressos' })}>Ingressos </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Conquistas', { name: 'Conquistas' })}>Conquistas </Text>
                                <Text style={styles.botoesAreasCinza} onPress={() => navigate('Configuracoes', { name: 'Configuracoes' })}>Configuraçoes </Text>
                                <Text style={styles.botoesAreasBranco} onPress={() => navigate('FaleConosco', { name: 'FaleConosco ' })}>Fale Conosco </Text>
                            </View>
                        </ScrollView>
                    </View>
                    {/* <Text style={styles.textoMenor}>TEMAS DO APP</Text> */}
                </View>
                <View style={{ marginTop: 20, marginVertical: 0, }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', }}>
                        <Text style={{ color: '#fff', width: '70%' }}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</Text>
                        
                        <Picker
                            selectedValue={this.state.assunto}
                            style={{ color: '#fff',height: 50, width: '70%'  }}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({ assunto: itemValue })
                            }>
                            <Picker.Item label="Assunto" value="java" />
                            <Picker.Item label="Assunto1" value="java1" />
                            <Picker.Item label="Assunto2" value="java2" />
                            <Picker.Item label="Assunto3" value="java3" />
                        </Picker>

                        <TextInput //use the color style to change the text color
                        placeholder=" Mensagem"
                        placeholderTextColor="#fff"
                        multiline={true}
                        underlineColorAndroid="white"
                        style={{ fontSize: 20,color: '#fff',height: '50%', width: '70%', textAlignVertical: "bottom" }}
                    />
                    </View>
                    {/* <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                        <View style={{ marginHorizontal: '11%', }} />
                        <Text style={{ color: '#fff', opacity: 1 }}>SELECIONADO</Text>
                    </View> */}

                </View>
                <View style={{ alignItems: 'center',justifyContent: 'flex-end' }}>
                    <TouchableOpacity>
                        <Botao cor="#fff" bdColor="#F15A24"
                            bgColor="#F15A24" nome="Enviar"
                        //onPress={this.recuperar.bind(this)}
                        //onPress={() => alert("Nenhuma função atribuida")}
                        />
                    </TouchableOpacity>

                    <View style={{ padding: 40 }}></View>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        // flexDirection: 'column',
        // justifyContent: 'space-between',
        backgroundColor: '#1c1c1c',
        // paddingStart: 37

    },
    botoesAreasBranco: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff'
    },
    botoesAreasCinza: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingEnd: 15,
        color: '#fff',
        opacity: 0.4,
    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fundoRedondo: {
        height: 300,
        width: 140,
        borderRadius: 7,
        resizeMode: "stretch",
        justifyContent: 'center',
        marginStart: 0,
        marginTop: 10,
    },
    fotoRetangular: {
        height: 300,
        width: 300,
        justifyContent: 'center',
        borderRadius: 15,
        // marginStart: 21,
        // marginTop: 50,
    },
    filtroRetangular: {
        height: 300,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 15,
        // marginStart: -100,
        // marginBottom: -100,
        marginTop: -300,
        marginRight: -300,
    },
    fotoQuadrada: {
        height: 180,
        width: 180,
        borderRadius: 10,
        marginEnd: 20,
        // marginTop: 50,
    },
    fotoRedondaHoriz: {
        height: 150,
        width: 200,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    filtroRedondoHoriz: {
        height: 150,
        width: 200,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -150,
    },
    filtroMedalha: {
        height: 80,
        width: 80,
        justifyContent: 'center',
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#FF9D00',
    },
    textoData: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 18,
        textAlign: "left",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 26,
        textAlign: "left",
        fontWeight: 'bold',
        marginBottom: -15,
        justifyContent: 'flex-start',
    },
    textoNomeEvento: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 20,
        textAlign: "left",
    },
    textoLocalizacao: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 10,
        textAlign: "left",
    },
    textoCidade: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 10,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoMenor: {
        color: '#fff',
        fontSize: 21,
        textAlign: 'left',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
        marginTop: 20,
    },

});

export default FaleConosco;
//  = StackNavigator({
//     First: { screen: FaleConosco },
//     // Second: {screen: ViewDataUser },
//     // Third: {screen: UpdeteDataUser }
// })

// container: {
//     //padding: 20,
//     flex: 1,
//     // justifyContent: 'space-between',
//     // paddingHorizontal: 15,
//     // paddingTop: 48,
//     // paddingBottom: 20,color: '#fff'
//     // alignItems: 'center',
//     backgroundColor: '#1c1c1c', //#026
// },


// foto: {
//     height: 40,
//     width: 40,
//     borderRadius: 20,
//     marginRight: -25,
//     marginVertical: 20,
//     // elevation: 10,
//     // shadowColor: '#202020',
//     // shadowOffset: { width: 10, height: 10 },
//     // shadowRadius: 40,
// },
// fotoRedondaVert: {
//     height: 200,
//     width: 150,
//     justifyContent: 'center',
//     borderRadius: 17.23,
//     marginTop: -0,
// },
// filtroRedondoVert: {
//     height: 200,
//     width: 150,
//     justifyContent: 'center',
//     borderRadius: 17.23,
//     marginTop: -200,
// },

// textoTitulo: {
//     color: '#fff',
//     // fontFamily: 'rockwell extra bold',
//     fontSize: 20,
//     textAlign: 'center',
//     fontWeight: 'bold',
//     fontFamily: "SFUIDisplay-Black",
//     opacity: 1,
//     marginTop: -40,
//     paddingTop: -20
// },
