import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import BotaoHome from '../components/BotaoHome';

class NovaConta extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    load = () => {
        fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/tables/user.php', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: TextInputName,
                        cpf: TextInputCPF,
                })
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson === 'Data Matched') {

                        ToastAndroid.show('Esse CPF já esta cadastrado...', ToastAndroid.LONG);
                        // this.props.navigation.navigate('Login', { name: 'Login' });

                    } else {

                        ToastAndroid.show('Cadastro efetuado com sucesso!', ToastAndroid.SHORT);
                            this.props.navigation.navigate('Login', { name: 'Login' });
                    }

                }).catch((error) => {
                    console.error(error);
                })
    }

    render() {
        this.load.bind(this);
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View>
                    <TouchableOpacity onPress={() =>  navigate('Perfil', { name: 'Perfil' })} > 
                        <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                            style={styles.fotoRedonda} />
                    </TouchableOpacity>
                </View>
                <View style={styles.botoes} >
                    <BotaoHome titulo="NOTÍCIAS" color='#FF005B' onPress={() => navigate('FeedNoticias', { name: 'FeedNoticias' })} />
                    <BotaoHome titulo="EVENTOS" color='#9900FF' onPress={() => navigate('FeedEventos', { name: 'FeedEventos' })} />
                    <BotaoHome titulo="DESAFIOS" color='#00FFBA' onPress={() => navigate('FeedDesafios', { name: 'FeedDesafios' })} />
                    <BotaoHome titulo="FAVORITO" color='#84F500' onPress={() => alert("Função não atribuida")} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        // flexDirection: 'column',
        // justifyContent: 'center',
        backgroundColor: '#1c1c1c'

    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center',
        marginTop: 50
    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fotoRedonda: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginStart: 37,
        marginTop: 50,
    },

});

export default NovaConta;