import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    Colors,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import BotaoHome from '../components/BotaoHome';

class Medalha extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Nenhuma função atribuida");
    }


    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                    style={styles.filtroMedalha} />
                <Text style={styles.textoTitulo}>10 eventos</Text>
                <Text style={{ color: 'white', width: 300 }}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</Text>
                <View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 39, width: 78, height: 30, backgroundColor: '#05CEA5' }} >
                    <Text style={{ color: '#fff' }}>180xp</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // marginRight: -20,
        // paddingTop: 48,
        // paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1c1c1c'

    },
    botoes: {
        marginRight: -20,
        paddingTop: 0,
        paddingBottom: 20,
        flexDirection: 'column',
        justifyContent: 'center'

    },
    textoTitulo: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
        fontWeight: 'bold',
    },
    fotoRedonda: {
        height: 60,
        width: 60,
        borderRadius: 30,
    },
    filtroMedalha: {
        height: 200,
        width: 200,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        borderWidth: 1,
        borderColor: '#FF9D00',
    },

});

export default Medalha;