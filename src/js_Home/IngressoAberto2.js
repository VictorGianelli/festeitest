import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  ScrollView,
  Platform,
  Image,
  FlatList,
  TextInput,
  Switch,
  TouchableOpacity,
  ToastAndroid,
  Button
} from 'react-native';
import BotaoTeste from '../components/BotaoTeste';
import BotaoComTexto from '../components/BotaoComTexto';
import Botao from '../components/Botao';
import { connect } from 'react-redux';
import { inserirDadosIngressos, inserirStrValTotal, inserirDadosIngressosDetalhes, editNome } from '../actions/AuthActions';

import Swiper from 'react-native-swiper';

export class IngressoAberto extends Component {

  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: 'white',
  };



  constructor(props) {
    super(props);
    this.state = {
      empyt: this.props.dadosIngressos,
      empyt2: {
        id: 0,
        tipo: 'tipo',
        preco: 25,
        taxa: 2.5,
        nome: '0,00',
        cpf: '0,00',
        telefone: '0,00',
        email: '0,00',
        nascimento: '0,00'
      },
      cont: 0,
      valTotal: 10,
      newTest: '20',
      strValTotal: '0',
      strValTotal2: '10.00',
      switchValue: false,
      teste: ""
    };

    this.state = { textNome: '' };
    this.state = { textCpf: '' };
    this.state = { textTelefone: '' };
    this.state = { textEmail: '' };
    this.state = { textNascimento: '' };
  }

  showArrayItem = (item) => {

    Alert.alert(item);
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  mais = (item, textNome) => {
    let state = this.state;

    //  let txtNome = String(textNome);
    // state.numeroTotal = this.props.dadosIngressos.length;

    let i = item.id;
    this.props.dadosIngressos[i].nome = String(textNome);

    Alert.alert(item.id + " - " + item.tipo + " - " + String(textNome));
    // let item2 = this.props.dadosIngressosDetalhes[i];
    // let empytCracs = new Array(state.numeroTotal).fill(null).map(() => (
    //   {
    //     nome: '0,00',
    //     cpf: '0,00',
    //     telefone: '0,00',
    //     email: '0,00',
    //     nascimento: '0,00'
    //   })
    // )
    //this.setState(state);

    //      this.props.dadosIngressos[i].nome = String(textNome);
    // item.nome=String(textNome);
    // empyt[String(item.id)].cpf = item.cpf;
    // empyt[String(item.id)].telefone = item.telefone;
    // empyt[String(item.id)].email = item.email;
    // empyt[String(item.id)].nascimento = item.nascimento;

    // this.props.dadosIngressos[i].nome = empyt[i].nome;

    //Alert.alert("empytCracs[i].nome  = " + this.props.dadosIngressos[i].nome);

    //this.props.dadosIngressos[i].nome=empytCracs[i].nome;
    // Alert.alert(this.props.dadosIngressos[0].strValTotal);
  }

  // toggleSwitch = (value) => {
  //   let state = this.state;
  //   ToastAndroid.show("this.state.strValTotal 1 = " + this.state.strValTotal, ToastAndroid.SHORT);
  //   //onValueChange of the switch this function will be called
  //   this.setState({ switchValue: value })
  //   this.state.strValTotal = this.props.strValTotal;
  //   ToastAndroid.show("this.props.strValTotal 2 = " + this.props.strValTotal, ToastAndroid.SHORT);
  //   //state changes according to switch
  //   //which will result in re-render the text
  // }

  render() {
    // if (this.props.dadosIngressos[0].nome == undefined ? this.props.dadosIngressos[0].nome = "" : Alert.alert("pronto"));

    // this.state.strValTotal=this.state.valTotal;
    // let valTotal = this.props.strValTotal;
    // this.state.strValTotal = valTotal;
    // ToastAndroid.show("this.props.strValTotal = " + String(this.props.strValTotal), ToastAndroid.SHORT);
    // this.state.strValTotal = this.state.strValTotal.replace(/\./g, ',');
    const { navigate } = this.props.navigation
    return (
      <View style={styles.MainContainer}>
        <View style={styles.margem}>
          {/* <ScrollView> */}

          <Swiper
            horizontal={true}
            paginationStyle={{ bottom: -1 }}
            dot={<View style={{ backgroundColor: '#555', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
            activeDot={<View style={{ backgroundColor: '#ED1E79', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
          >

            {
              this.props.dadosIngressos.map((item, key) => (
                // onPress={this.showArrayItem.bind(this, String(item.age))}
                // <View style={{ marginTop: -150}}>
                <View key={item.id} style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0, marginTop: -135 }}>
                  {/* Parte de cima */}
                  <View style={{ borderWidth: item.border, borderColor: '#FF9D00', borderBottomLeftRadius: 17.23, borderBottomRightRadius: 15 }}>
                    <Image source={require('../img/fundo_tela_evento.png')}
                      style={styles.fotoRedonda} />
                    <Image source={require('../img/filtro_background.png')}
                      style={styles.filtroRedondo} />
                    <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                      <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.textoDia}>{item.data}</Text>
                        <Text style={styles.textoBarra}>___</Text>
                        <Text style={styles.textoMes}>{item.mes}</Text>
                      </View>
                      <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                        <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                        <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>
                      </View>
                    </View>
                  </View>
                  {/* Parte de baixo */}
                  <View style={{ borderWidth: item.border, borderColor: '#FF9D00', padding: 20, height: 150, width: 300, backgroundColor: '#262626', borderRadius: 15, flexDirection: 'column' }}>
                    {/* <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100 }}> */}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 7 }}>{/* flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100  */}
                      <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Black", fontSize: 14 }}>{item.tipo}</Text>
                      <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Medium", fontSize: 14 }}>R$ {item.preco} (+{item.taxa})</Text>
                    </View>
                    <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Light", fontSize: 16 }}>Nome Sobrenome Completo</Text>
                    <Text style={{ color: '#F15A24', fontFamily: "SFUIDisplay-Bold", fontSize: 14 }}>CPF 123.456.789-10</Text>
                    <View style={{ alignItems: 'baseline', paddingTop: 17 }}>
                      <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>(DDD) 9.9999-9999</Text>
                      <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>email@domínio.com</Text>
                    </View>
                    {/* </View> */}
                  </View>

                  <View>
                    <Text style={{ color: '#fff', fontSize: 50, marginVertical: -189 }}>- - - - - - - - - -</Text>
                  </View>
                  {/* <View style={{ height: 50, width: 300 }}></View> */}

                  <View style={{
                    flexDirection: 'row', justifyContent: 'flex-start',
                    alignItems: 'center', paddingTop: 15
                  }}>
                    <Switch
                      style={{ padding: 0 }}
                      onValueChange={this.toggleSwitch}
                      value={this.state.switchValue}
                      thumbColor='white'
                      trackColor='white' />
                    <Text style={{ color: '#fff', padding: 5 }}>Preencher meus dados salvos</Text>
                  </View>

                  <TextInput
                    placeholder=" Nome"
                    placeholderTextColor="#fff"
                    underlineColorAndroid="white"
                    style={styles.textInputFormat}
                    onChangeText={(textNome) => this.props.editNome({ textNome })}
                    value={item.nome}
                  //onChange={this.mais.bind(this, item, this.state.textNome)}
                  />
                  <TextInput
                    placeholder=" CPF"
                    placeholderTextColor="#fff"
                    underlineColorAndroid="white"
                    style={styles.textInputFormat}
                    onChangeText={(textCpf) => this.setState({ textCpf })}
                    keyboardType={'numeric'}
                    value={item.cpf}
                  />
                  <TextInput
                    placeholder=" Telefone DDD 9.9999-9999"
                    placeholderTextColor="#fff"
                    underlineColorAndroid="white"
                    style={styles.textInputFormat}
                    onChangeText={(textTelefone) => this.setState({ textTelefone })}
                    keyboardType={'numeric'}
                    value={item.telefone}
                  />
                  <TextInput
                    placeholder=" E-mail"
                    placeholderTextColor="#fff"
                    underlineColorAndroid="white"
                    style={styles.textInputFormat}
                    onChangeText={(textEmail) => this.setState({ textEmail })}
                    value={item.email}
                  />
                  <TextInput
                    placeholder=" Nascimento"
                    placeholderTextColor="#fff"
                    underlineColorAndroid="white"
                    style={styles.textInputFormat}
                    onChangeText={(textNascimento) => this.setState({ textNascimento })}
                    keyboardType={'numeric'}
                    value={item.nascimento}
                  />
                </View>

              ))
            }

          </Swiper>



          {/* </ScrollView> */}
          
          {/* Parte de baixo  */}
          <View style={{ alignItems: 'flex-start', marginTop: 30 }}>
            <Text style={styles.textoTotal}>Total</Text>
            <Text style={styles.textoValor}>R$ {this.props.dadosIngressos[0].strValTotal}</Text>
          </View>
          <View style={{ opacity: this.state.opacity }} >
            <TouchableOpacity style={{ alignItems: 'center' }} >
              <Botao cor="#fff" bdColor="#F15A24"
                bgColor="#F15A24" nome="Comprar" disable={this.state.enabled}
                onPress={() => navigate('AceitarTermos', { name: 'AceitarTermos' })}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#1c1c1c'
  },
  TextStyle: {
    fontSize: 20,
    color: '#000',
    textAlign: 'left'
  },

  margem: {
    flex: 1,
  },
  switchView: {
    width: 350,
    height: 450
  },
  texto: {
    color: '#fff',
    fontFamily: 'arial',
    fontSize: 20,
    marginStart: 50,
    textAlign: "left",
  },
  textoTotal: {
    color: '#fff',
    fontSize: 18,
    marginStart: 20,
    textAlign: "left",
    fontFamily: "SFuidisplay-medium",
    alignItems: 'center'
  },
  textoValor: {
    color: '#F15A24',
    fontSize: 35,
    marginStart: 20,
    textAlign: "left",
    fontFamily: "SFuidisplay-medium",
    alignItems: 'center'
  },
  textoGrande: {
    color: '#fff',
    // fontFamily: 'rockwell extra bold',
    // fontSize: 50,
    textAlign: "left",
    // marginStart: 50,
    // marginEnd: 50,
    fontWeight: 'bold',
  },
  backgroundImage: {
    width: '100%',
    flex: 1,
    resizeMode: 'stretch',
    //backgroundColor: 'rgba(0, 0, 0, 1.0)',
  },
  fotoRedonda: {
    height: 533.6,
    width: 300,
    resizeMode: "stretch",
    justifyContent: 'center',
    borderRadius: 17.23,

    marginTop: -333.6,
    // marginVertical: 20,
    // borderWidth: 2,borderColor: '#FF9D00'
    // elevation: 10,
    // shadowColor: '#202020',
    // shadowOffset: {width: 10, height: 10 },
    // shadowRadius: 40,
  },
  filtroRedondo: {
    height: 533.6,
    width: 300,
    resizeMode: "stretch",
    justifyContent: 'center',
    borderRadius: 17.23,
    marginTop: -533.6,
    // marginVertical: 20,

    // elevation: 10,
    // shadowColor: '#202020',
    // shadowOffset: {width: 10, height: 10 },
    // shadowRadius: 40,
  },

  textoMes: {
    color: '#fff',
    fontFamily: "SFuidisplay-Black",
    fontSize: 30,
    textAlign: "center",
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  textoBarra: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 30,
    textAlign: "center",
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  textoDia: {
    color: '#fff',
    fontFamily: "SFuidisplay-Medium",
    fontSize: 40,
    textAlign: "center",
    fontWeight: 'bold',
    marginBottom: -35,
    justifyContent: 'center',
  },
  textoNomeEvento: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 40,
    textAlign: "left",
  },
  textoLocalizacao: {
    color: '#fff',
    fontFamily: 'arial',
    fontSize: 20,
    textAlign: "left",
  },
  textoCidade: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 15,
    textAlign: "left",
    fontWeight: 'bold',
  },
  textInputFormat: {
    height: 60,
    width: 300,
    color: 'white',
    marginBottom: -15,
    fontSize: 18
  },

});

const mapStateToProps = (state) => {
  return {
    dadosIngressos: state.auth.dadosIngressos,
    dadosIngressosDetalhes: state.auth.dadosIngressosDetalhes,
    strValTotal: state.auth.strValTotal,
    nome: state.auth.nome
  }
};

const DadosConnct = connect(mapStateToProps, { inserirDadosIngressos, inserirDadosIngressosDetalhes, inserirStrValTotal, editNome })(IngressoAberto);

export default DadosConnct;
