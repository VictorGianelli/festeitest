import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { editEmail, editSenha } from '../actions/AuthActions';

import Botao from '../components/Botao';

export class EventoQueFui extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Mais 1 voto");
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <View>
                    <View style={{
                        alignItems: 'center', justifyContent: 'center',
                        paddingTop: 0, height: 100
                    }} >
                        <Image source={require('../img/fundo_tela_1.png')}
                            style={styles.fotoHeader} />
                        <Image source={require('../img/filtro_background.png')}
                            style={styles.filtroHeader} />
                        <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.textoDia}>22</Text>
                                <Text style={styles.textoBarra}>___</Text>
                                <Text style={styles.textoMes}>JUN</Text>
                            </View>
                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                                <Text style={styles.textoNomeEvento}>Happy Holi</Text>
                                <Text style={styles.textoLocalizacao}>Esplanada Mineirão</Text>
                                <Text style={styles.textoCidade}>Belo Horizonte - MG</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ marginBottom: 70 }} />
                </View>
                {/* ////////////////////////////////// */}
                <ScrollView>
                    <View style={{ alignItems: 'flex-start', marginTop: 60, marginVertical: 0, }}>
                        <Text style={styles.textoMenor}>DESAFIOS DO DIA</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <ScrollView horizontal={true}>
                                <View>
                                    <TouchableOpacity style={{ height: 460, width: 300 }} onPress={() => navigate('DesafioAberto', { name: 'DesafioAberto' })}  >
                                        <View style={{ height: 460, justifyContent: 'flex-start', alignItems: 'center', paddingStart: 7.5, paddingEnd: 7.5 }}>

                                            <View style={{ height: 345.19 }} />

                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: -430, borderRadius: 10, width: 245, height: 100, backgroundColor: '#fff' }} >
                                                <Text style={{ color: '#000', fontSize: 40, fontWeight: 'bold' }}>05:00</Text>
                                                <Text style={{ color: '#000', fontSize: 10 }}>HORAS RESTANTES</Text>
                                            </View>

                                            <View>
                                                <Image source={require('../img/fundo_tela_5.png')}
                                                    style={styles.fotoRedonda} />
                                                <Image source={require('../img/filtro_background.png')}
                                                    style={styles.filtroRedondo} />
                                            </View>

                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: -355.19, borderRadius: 39, width: 78, height: 30, backgroundColor: '#05CEA5' }} >
                                                <Text style={{ color: '#fff' }}>180xp</Text>
                                            </View>

                                            <View style={{ width: 250, flexDirection: 'row', marginTop: 190.19, marginBottom: -20 }} >
                                                <View style={{ width: 100, flexDirection: 'row' }} >
                                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                        style={styles.foto} />
                                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                        style={styles.foto} />
                                                </View>
                                                <View style={{ width: 125, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >
                                                    <Text style={{ color: '#fff' }}>7 PESSOAS</Text>
                                                </View>
                                            </View>

                                            <View style={{ width: 250, height: 80, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start' }} >{/* , backgroundColor: '#EF3D4E' */}
                                                <Text style={{ color: '#fff', fontSize: 30, fontWeight: 'bold' }}>DRESS LIKE HALLOWEEN</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {/* ////////////////////////////////////////////////////////// */}
                                <View>
                                    <TouchableOpacity style={{ height: 460, width: 300 }} onPress={() => alert("Nenhuma função atribuida")}  >
                                        <View style={{ height: 460, justifyContent: 'flex-start', alignItems: 'center', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                            <View style={{ height: 345.19 }} />

                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: -430, borderRadius: 10, width: 245, height: 100, backgroundColor: '#fff' }} >
                                                <Text style={{ color: '#000', fontSize: 40, fontWeight: 'bold' }}>05:00</Text>
                                                <Text style={{ color: '#000', fontSize: 10 }}>HORAS RESTANTES</Text>
                                            </View>

                                            <View>
                                                <Image source={require('../img/fundo_tela_2.png')}
                                                    style={styles.fotoRedonda} />
                                                <Image source={require('../img/filtro_background.png')}
                                                    style={styles.filtroRedondo} />
                                            </View>

                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: -355.19, borderRadius: 39, width: 78, height: 30, backgroundColor: '#05CEA5' }} >
                                                <Text style={{ color: '#fff' }}>180xp</Text>
                                            </View>

                                            <View style={{ width: 250, flexDirection: 'row', marginTop: 190.19, marginBottom: -20 }} >
                                                <View style={{ width: 100, flexDirection: 'row' }} >
                                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                        style={styles.foto} />
                                                    <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                        style={styles.foto} />
                                                </View>
                                                <View style={{ width: 125, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >
                                                    <Text style={{ color: '#fff' }}>7 PESSOAS</Text>
                                                </View>
                                            </View>

                                            <View style={{ width: 250, height: 80, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start' }} >{/* , backgroundColor: '#EF3D4E' */}
                                                <Text style={{ color: '#fff', fontSize: 30, fontWeight: 'bold' }}>DRINKS ON FIRE</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View></View>
                </ScrollView>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // justifyContent: 'space-between',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    foto: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: -25,
        marginVertical: 20,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    fotoHeader: {
        height: 350.19,
        width: '100%',
        justifyContent: 'center',
        marginTop: -0,
    },
    filtroHeader: {
        height: 350.19,
        width: '100%',
        justifyContent: 'center',
        marginTop: -350.19,
    },
    
    fotoRedonda: {
        height: 350.19,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    filtroRedondo: {
        height: 350.19,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -350.19,
    },
    fotoRedonda2: {
        height: 280,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },

    fotoRedonda3: {
        height: 280,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginLeft: -10,
    },
    filtroRedondo2: {
        height: 280,
        width: 280,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -280,
    },
    textoTitulo: {
        color: '#fff',
        // fontFamily: 'rockwell extra bold',
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
        opacity: 1,
        marginTop: -40,
        paddingTop: -20
    },
    textoMenor: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        marginStart: 23,
        fontSize: 21,
        textAlign: 'left',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
    },
    textoMenor2: {
        color: '#fff',
        //fontFamily: 'rockwell extra bold',
        marginHorizontal: 23,
        fontSize: 14,
        textAlign: 'left',
        fontFamily: "SFUIDisplay-Lighr",
    },
    inputNome: {
        width: 300,
        //   borderWidth: 1,
        //   borderColor: '#999999',
        //   backgroundColor: '#EEEEEE',
        color: '#000',
        height: 38,
        margin: 20,
        padding: 10,
    },
    textInputFormat: {
        height: 60,
        width: 300,
        color: 'white',
        fontSize: 18
    },

    textoMes: {
        color: '#fff',
        fontFamily: "SFuidisplay-Black",
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoBarra: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: "SFuidisplay-Medium",
        fontSize: 40,
        textAlign: "center",
        fontWeight: 'bold',
        marginBottom: -35,
        justifyContent: 'center',
    },
    textoNomeEvento: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
    },
    textoLocalizacao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        textAlign: "left",
    },
    textoCidade: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
    },

});

const mapStateToProps = (state) => {
    return {
    }
};

const EventoQueFuiConnct = connect(mapStateToProps, {})(EventoQueFui);

export default EventoQueFuiConnct;