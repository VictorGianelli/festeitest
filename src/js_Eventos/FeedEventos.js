import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    ScrollView,
    Alert,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    TouchableHighlight,
    FlatList,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { editEmail, editSenha } from '../actions/AuthActions';

import Botao from '../components/Botao';
import Lista from '../components/Lista';

export class FeedEventos extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            localEvento: [
                {
                    id: '0',
                    data: 22,
                    mes: "JUN",
                    nomeEvento: "Happy Holi",
                    namePlace: "Esplanada Mineirão",
                    cityPlace: "Belo Horizonte - MG",
                    like: true
                },
                {
                    id: '1',
                    data: 23,
                    mes: "JUN",
                    nomeEvento: "Happy Holi",
                    namePlace: "Esplanada Mineirão",
                    cityPlace: "Belo Horizonte - MG",
                    like: false
                }, {
                    id: '2',
                    data: 22,
                    mes: "JUN",
                    nomeEvento: "Happy Holi",
                    namePlace: "Esplanada Mineirão",
                    cityPlace: "Belo Horizonte - MG",
                    like: true
                }, {
                    id: '3',
                    data: 23,
                    mes: "JUN",
                    nomeEvento: "Happy Holi",
                    namePlace: "Esplanada Mineirão",
                    cityPlace: "Belo Horizonte - MG",
                    like: false
                }
            ],
            dataSource: [],
            all: [],
            showIndicator: true,
        };

        // this.state.like = true
        // this.carregaIcone = this.carregaIcone.bind(this);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);


    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Mais 1 voto");
    }

    carregaIcone = (like) => {
        return like ? require('../img/coracao_vazio_white.png') : require('../img/coracao_cheio.png');

    }

    toLike = () => {

        if (this.state.like) {
            this.setState({
                imageURL: '../img/coracao_vazio_white.png',
                like: false
            })
        } else {
            this.setState({
                imageURL: './img/coracao_cheio.png',
                like: true
            })
        }
    }

    componentDidMount() {
        var that = this;
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        // var hours2 = new Date().getHours()+1; //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        // var min2 = new Date().getMinutes()-1; //Current Minutes
        var sec = new Date().getSeconds(); //Current Seconds
        that.setState({
            //Setting the value of the date time
            date:
                date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec,
        });

        fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/tables/event_proximos_eventos.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                date: this.state.date,
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                // If server response message same as Data Matched
                if (responseJson === 'Result not found') {
                    Alert.alert("Erro", responseJson);
                    //Then open Profile activity and send user email to profile activity.
                    //this.props.navigation.navigate('Home', { name: 'Home' });

                    // Alert.alert("Feito",responseJson);

                } else {
                    // Alert.alert("Data",JSON.stringify(responseJson));
                    this.setState({
                        dataSource: responseJson,
                        showIndicator: false,
                    })
                    // Alert.alert("Data",this.state.dataSource);
                }

            }).catch((error) => {
                console.error(error);
            });
    }

    // componentWillUnmount() {
    //     this.setState({
    //         showIndicator: true,
    //     })
    // }

    teste = (id) => {
        // this.setState({
        //     all : [...this.state.event, ...this.state.place]
        // });

        // Alert.alert("Data", id);

        this.props.navigation.navigate('BioEvento', { name: 'BioEvento' })
    }

    teste2 = (id) => {
        // this.setState({
        //     all : [...this.state.event, ...this.state.place]
        // });

        //Alert.alert("Data", id);

        this.props.navigation.navigate('BioEventobkp', { name: 'BioEventobkp' })
    }

    stopShowing = () => {
        //function to change the state to true to view activity indicator
        this.setState({
            showIndicator: false,
        });
        //changing state will re-render the view and indicator will appear
    };

    render() {
        const { navigate } = this.props.navigation
        // Alert.alert("Data", JSON.stringify(this.state.all));
        // if (this.state.dataSource != null ? this.stopShowing() : null);
        // /if (this.state.all != null ? alert(JSON.stringify(this.state.all)) : null);
        // Alert.alert("Data",this.state.dataSource.length());
        if (this.state.showIndicator) {
            return (
                <View style={styles.container}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, backgroundColor: '#9900FF', height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}>Eventos1</Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    {/*Code to show Activity Indicator*/}
                    <View style={styles.containerLoading}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                    {/*Size can be large/ small*/}
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    <View>
                        <View style={{
                            alignItems: 'center', justifyContent: 'center',
                            paddingTop: 0, backgroundColor: '#9900FF', height: 60, opacity: 0.5
                        }} >
                        </View>
                        <Text style={styles.textoTitulo}>Eventos</Text>
                        <View style={{ marginBottom: 13 }} />
                    </View>
                    {/* ////////////////////////////////// */}
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled>
                        <View style={{ alignItems: 'flex-start', marginTop: 20, marginVertical: 0, }}>
                            <Text style={styles.textoMenor}>PRÓXIMOS EVENTOS</Text>
                            <View style={{ flexDirection: 'row' }}>
                                {/* <FlatList keyExtractor={item => item.id}
                                data={this.state.localEvento}
                                renderItem={({ item }) => <Lista data={item} />}
                                // renderItem={({ item }) =>
                                //     <TouchableOpacity onPress={() => { this.props.navigator.push({ id: 'employeeEdit' }) }} >
                                //         <Lista employee={item} navigation={this.props.navigation} />
                                //     </TouchableOpacity>}
                                horizontal={true}
                            /> */}
                                <ScrollView keyExtractor={item => item.id}
                                    horizontal={true}
                                    paginationStyle={{ bottom: -1 }}
                                >

                                    {
                                        this.state.dataSource.map((item, key) => (
                                            <View key={item.id} >
                                                {/* <TouchableOpacity style={{ height: 210, width: 170 }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  > */}
                                                <TouchableOpacity style={{ height: 210, width: 170 }} onPress={() => this.teste(item.id)}  >
                                                    <View style={{ paddingHorizontal: 20, marginStart: 0, height: 210, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                                        <View style={{ marginTop: -10, }} />
                                                        <View>
                                                            <Image source={require('../img/fundo_tela_1.png')}
                                                                style={styles.fotoRedondaVert} />
                                                            <Image source={require('../img/filtro_background.png')}
                                                                style={styles.filtroRedondoVert} />

                                                        </View>

                                                        <View style={{ marginTop: -30, flexDirection: 'row', marginTop: -200 }} >
                                                            <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                                <Text style={styles.textoDia}>{item.date}</Text>
                                                                <Text style={styles.textoData}>_____</Text>
                                                                <Text style={styles.textoData}>{item.month}</Text>
                                                            </View>

                                                            <View style={{ width: 40 }} />
                                                            <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                                                {/* <TouchableHighlight onPress={this.toLike} underlayColor="transparent"> */}

                                                                <Image
                                                                    style={styles.fotoMenu} source={this.carregaIcone(item.like)} />

                                                                {/* </TouchableHighlight> */}

                                                            </View>
                                                        </View>

                                                        <View style={{ height: 65 }} />

                                                        <View style={{ width: 170, }}>

                                                            <Text style={styles.textoNomeEvento}>{item.nameEvent}</Text>
                                                            <Text style={styles.textoLocalizacao}>{item.namePlace}</Text>
                                                            <Text style={styles.textoCidade}>{item.city}</Text>

                                                        </View>

                                                    </View>
                                                </TouchableOpacity>
                                            </View>

                                        ))
                                    }


                                </ScrollView>
                            </View>

                            {/* <FlatList 
                            keyExtractor={item => item.id}
                            data={this.state.dataSource}
                            // horizontal={true}
                            // renderItem={({ item }) => <Lista data={item} />}
                        /> */}
                            <Text style={styles.textoMenor}>POR PERTO</Text>

                            <View style={{ flexDirection: 'row' }}>
                                {/* <FlatList keyExtractor={item => item.id}
                                data={this.state.localEvento}
                                renderItem={({ item }) => <Lista data={item} />}
                                // renderItem={({ item }) =>
                                //     <TouchableOpacity onPress={() => { this.props.navigator.push({ id: 'employeeEdit' }) }} >
                                //         <Lista employee={item} navigation={this.props.navigation} />
                                //     </TouchableOpacity>}
                                horizontal={true}
                            /> */}
                                <ScrollView keyExtractor={item => item.id}
                                    horizontal={true}
                                    paginationStyle={{ bottom: -1 }}
                                >

                                    {
                                        this.state.dataSource.map((item, key) => (
                                            <View key={item.id} >
                                                {/* <TouchableOpacity style={{ height: 210, width: 170 }} onPress={() => navigate('BioEvento', { name: 'BioEvento' })}  > */}
                                                <TouchableOpacity style={{ height: 210, width: 170 }} onPress={() => this.teste2(item.id)}  >
                                                    <View style={{ paddingHorizontal: 20, marginStart: 0, height: 210, justifyContent: 'center', alignItems: 'flex-start', paddingStart: 7.5, paddingEnd: 7.5 }}>
                                                        <View style={{ marginTop: -10, }} />
                                                        <View>
                                                            <Image source={require('../img/fundo_tela_1.png')}
                                                                style={styles.fotoRedondaVert} />
                                                            <Image source={require('../img/filtro_background.png')}
                                                                style={styles.filtroRedondoVert} />

                                                        </View>

                                                        <View style={{ marginTop: -30, flexDirection: 'row', marginTop: -200 }} >
                                                            <View style={{ width: 50, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                                <Text style={styles.textoDia}>{item.date}</Text>
                                                                <Text style={styles.textoData}>_____</Text>
                                                                <Text style={styles.textoData}>{item.month}</Text>
                                                            </View>

                                                            <View style={{ width: 40 }} />
                                                            <View style={{ width: 50, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                                                {/* <TouchableHighlight onPress={this.toLike} underlayColor="transparent"> */}

                                                                <Image
                                                                    style={styles.fotoMenu} source={this.carregaIcone(item.like)} />

                                                                {/* </TouchableHighlight> */}

                                                            </View>
                                                        </View>

                                                        <View style={{ height: 65 }} />

                                                        <View style={{ width: 170, }}>

                                                            <Text style={styles.textoNomeEvento}>{item.nameEvent}</Text>
                                                            <Text style={styles.textoLocalizacao}>{item.namePlace}</Text>
                                                            <Text style={styles.textoCidade}>{item.city}</Text>

                                                        </View>

                                                    </View>
                                                </TouchableOpacity>
                                            </View>

                                        ))
                                    }


                                </ScrollView>
                            </View>

                            <Text style={styles.textoMenor}>RECOMENDADOS</Text>

                            <View style={{ height: 30 }} />
                        </View>
                    </ScrollView>
                </View>
            );
        }
    }

}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // justifyContent: 'space-between',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    containerLoading: {
        //padding: 20,
        flex: 1,
        justifyContent: 'center',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    textoNomeEvento: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 20,
        textAlign: "left",
    },
    textoLocalizacao: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 10,
        textAlign: "left",
    },
    textoCidade: {
        marginStart: 10,
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 10,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoData: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 18,
        textAlign: "left",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 26,
        textAlign: "left",
        fontWeight: 'bold',
        marginBottom: -15,
        justifyContent: 'flex-start',
    },
    textodateEvent: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 18,
        textAlign: "left",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 26,
        textAlign: "left",
        fontWeight: 'bold',
        marginBottom: -15,
        justifyContent: 'flex-start',
    },
    foto: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: -25,
        marginVertical: 20,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    fotoRedondaVert: {
        height: 200,
        width: 150,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    filtroRedondoVert: {
        height: 200,
        width: 150,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -200,
    },
    fotoRedondaHoriz: {
        height: 150,
        width: 200,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -0,
    },
    filtroRedondoHoriz: {
        height: 150,
        width: 200,
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -150,
    },
    textoTitulo: {
        color: '#fff',
        // fontFamily: 'rockwell extra bold',
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
        opacity: 1,
        marginTop: -40,
        paddingTop: -20
    },
    textoMenor: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        marginStart: 23,
        fontSize: 21,
        textAlign: 'left',
        fontWeight: 'bold',
        fontFamily: "SFUIDisplay-Black",
        marginTop: 20,
    },


});

const mapStateToProps = (state) => {
    return {
    }
};

const FeedEventosConnct = connect(mapStateToProps, {})(FeedEventos);

export default FeedEventosConnct;