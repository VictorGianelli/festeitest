import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  ScrollView,
  Platform,
  Image,
  FlatList,
  TextInput,
  Switch,
  TouchableOpacity,
  ToastAndroid,
  Button
} from 'react-native';
// import BotaoTeste from './components/BotaoTeste';
// import BotaoComTexto from './components/BotaoComTexto';
import Botao from '../components/Botao';
import { connect } from 'react-redux';
import { inserirDadosIngressos, inserirStrValTotal ,inserirDadosIngressosDetalhes} from '../actions/AuthActions';

import Swiper from 'react-native-swiper';

export class Dados extends Component {

  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: 'white',
  };

  constructor(props) {
    super(props);
    this.state = {
      empyt: this.props.dadosIngressos,
      cont: 0,
      valTotal: 10,
      newTest: '20',
      strValTotal: '0',
      strValTotal2: '10.00',
      switchValue: false
    };

    // this.state = { textNome: '' };
    // this.state = { textCpf: '' };
    // this.state = { textTelefone: '' };
    // this.state = { textEmail: '' };
    // this.state = { textNascimento: '' };
  }

  valores = () => {

    let texto = "Nro total = " + this.props.dadosIngressos.length;
    for (var i = 0; i < this.props.dadosIngressos.length; i++) {
      // for (var j = 0; j < this.props.dadosIngressos[j].numero; j++) {
        texto += "\nTipo = " + this.props.dadosIngressos[i].tipo;// + " Valor = " + this.props.dadosIngressos[i].preco +" e "+ this.props.dadosIngressos[i].taxa;
        texto += "\nNome = " + this.props.dadosIngressos[i].nome;
        //             empyt[nro].cpf = this.state.eventos[i].cpf;
        //             empyt[nro].telefone = this.state.eventos[i].telefone;
        //             empyt[nro].email = this.state.eventos[i].email;
        //             empyt[nro].nascimento = this.state.eventos[i].nascimento;
        
        // texto += "\nInfo = " + this.props.dadosIngressos[i].nome;
        // texto += "\nInfo = " + this.props.dadosIngressos[i].cpf;
        // texto += "\nInfo = " + this.props.dadosIngressos[i].telefone;
        // texto += "\nInfo = " + this.props.dadosIngressos[i].email;
        // texto += "\nInfo = " + this.props.dadosIngressos[i].nascimento;
      // }
    }
      texto += "\nValor total = " + this.props.dadosIngressos[0].strValTotal;

      Alert.alert("Valores", texto);

  }
  render() {
    // this.state.strValTotal=this.state.valTotal;
    // let valTotal = this.props.strValTotal;
    // this.state.strValTotal = valTotal;
    // ToastAndroid.show("this.props.strValTotal = " + String(this.props.strValTotal), ToastAndroid.SHORT);
    // this.state.strValTotal = this.state.strValTotal.replace(/\./g, ','); justifyContent: 'flex-end'
    return (

      <View style={styles.MainContainerBotton}>
        <View style={{ flexDirection: 'column', alignItems: 'flex-start', paddingStart: 20, marginTop: 48, }}>
          {/* <Text style={styles.textoTitulo}>NOVA CONTA</Text> */}
        </View>

        <ScrollView>
          <View>

            <View style={{  padding: 10, width: 350, marginHorizontal: 38 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 20 }}>"Termos e condições gerais de uso e/ou de venda</Text>
              <Text style={{ color: '#fff', fontSize: 16 }}>{"\n"}Por meio de seus termos e condições gerais de uso e/ou de venda, um site ou um aplicativo explica aos usuários quais são as condições de utilização do serviço disponibilizado através de sua página ou programa, seja ele gratuito ou pago. Além de informar o usuário sobre a necessidade de cadastro ou sobre os elementos protegidos por direitos autorais, este instrumento determina, ainda, as responsabilidades de cada uma das partes - editor (pessoa que mantém o site ou aplicativo) e usuário -, em relação ao uso do serviço.
      {"\n"}
                {"\n"}A cláusula mais importante dos termos e condições gerais de uso é aquela que define a política de privacidade do site ou aplicativo. Nela, o editor comunica claramente ao usuário como serão protegidas ou utilizadas as informações que este fornece ao site ou ao aplicativo, sejam elas dados pessoais ou dados de navegação.
      {"\n"}
                {"\n"}Nos casos em que o site ou o aplicativo colocar à venda produtos ou serviços, este documento virá, também, complementado pelas condições gerais de venda, que regularão os detalhes desta transação, tais como as formas de pagamento, a entrega e a política de trocas e devoluções.
      {"\n"}
                {"\n"}Neste documento ainda será possível adaptar a política de privacidade do site ou aplicativo ao Regulamento Geral de Proteção de Dados da União Europeia (RGPD), também conhecido como General Data Protection Regulation (GDPR)."</Text>
            </View>

          </View>
        </ScrollView>

        <View style={{ alignItems: 'center' }}>
          <TouchableOpacity>
            <Botao cor="#fff" bdColor="#F15A24"
              bgColor="#F15A24" nome="Aceitar Termos"
              onPress={this.valores.bind(this)}
            />
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#1c1c1c'
  },
  MainContainerBotton: {
    flex: 1,
    backgroundColor: '#1c1c1c'
  },
  texto: {
    color: '#fff',
    fontFamily: 'arial',
    fontSize: 20,
    marginStart: 50,
    textAlign: "left",
  },
});

const mapStateToProps = (state) => {
  return {
    dadosIngressos: state.auth.dadosIngressos,
    dadosIngressosDetalhes: state.auth.dadosIngressosDetalhes,
    strValTotal: state.auth.strValTotal
  }
};

const DadosConnct = connect(mapStateToProps, { inserirDadosIngressos, inserirStrValTotal, inserirDadosIngressosDetalhes })(Dados);

export default DadosConnct;
