import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    BackHandler,
    ScrollView,
    Alert,
    Image,
    ActivityIndicator,
    TouchableHighlight,
    ImageBackground,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { inserirDadosIngressos, inserirStrValTotal } from '../actions/AuthActions';

export class FeedEventos extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {

            dataSource: [],
            showIndicator: true,
        };

        // this.state.like = true
        // this.carregaIcone = this.carregaIcone.bind(this);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);


    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.state.p0 = 0; // works best when the goBack is async
            return true;
          });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', () => {
            this.state.p0 = 0; // works best when the goBack is async
            return true;
          });
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    alert = () => {
        alert("Mais 1 voto");
    }

    carregaIcone = (like) => {
        return like ? require('../img/coracao_vazio_white.png') : require('../img/coracao_cheio.png');

    }

    toLike = () => {

        if (this.state.like) {
            this.setState({
                imageURL: '../img/coracao_vazio_white.png',
                like: false
            })
        } else {
            this.setState({
                imageURL: './img/coracao_cheio.png',
                like: true
            })
        }
    }

    componentDidMount() {
        fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/tables/event_bioevento.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: 1
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                // If server response message same as Data Matched
                if (responseJson === 'Result not found') {
                    Alert.alert("Erro", responseJson);
                    //Then open Profile activity and send user email to profile activity.
                    //this.props.navigation.navigate('Home', { name: 'Home' });

                    // Alert.alert("Feito",responseJson);

                } else {
                    this.setState({
                        dataSource: responseJson,
                        showIndicator: false,
                    })
                    // Alert.alert("Data",JSON.stringify(responseJson));
                    // Alert.alert("Data",this.state.dataSource);
                }

            }).catch((error) => {
                console.error(error);
            });
    }

    // componentWillUnmount() {
    //     this.setState({
    //         showIndicator: true,
    //     })
    // }

    render() {
        const { navigate } = this.props.navigation
        // Alert.alert("Data", JSON.stringify(this.state.dataSource));
        // if (this.state.dataSource != null ? Alert.alert("Data", JSON.stringify(this.state.dataSource)) : null);
        // /if (this.state.all != null ? alert(JSON.stringify(this.state.all)) : null);
        // Alert.alert("Data",this.state.dataSource.length());
        if (this.state.showIndicator) {
            return (
                <View style={styles.container}>
                    <View style={styles.containerLoading}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                </View>
            );
        } else {
            return (
                <ImageBackground source={require('../img/fundo_tela_1.png')}
                    style={styles.backgroundImage}  >
                    <ImageBackground source={require('../img/filtro_background.png')}
                        style={styles.backgroundImage}  >
                        {
                            this.state.dataSource.map((item, key) => (
                                // <View key={item.id} >
                                <View key={item.id} style={styles.screenMargin}>
                                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start', width: 150 }}>
                                        <View>
                                            <View style={{ width: 100, marginLeft: 0, }}>
                                                <Text style={styles.textoDia}>{item.date}</Text>
                                                <Text style={styles.textoData}>___</Text>
                                                <Text style={styles.textoData}>{item.month}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                    style={styles.fotoRedonda} />
                                                <Image source={require('../img/FotoQuadrada/foto2.jpg')}
                                                    style={styles.fotoRedonda} />
                                                <Image source={require('../img/FotoQuadrada/foto3.jpg')}
                                                    style={styles.fotoRedonda} />
                                                <Image source={require('../img/FotoQuadrada/foto4.jpg')}
                                                    style={styles.fotoRedonda} />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ marginTop: -180, flex: 1, flexDirection: 'row', justifyContent: 'flex-start' }}>
                                        <ScrollView
                                            showsHorizontalScrollIndicator={false}
                                            pagingEnabled>
                                            <View style={{ width: 250 }}>

                                                <Text style={styles.textoNomeEvento}>{item.nameEvent}</Text>
                                                <Text style={styles.textoLocalizacao}>{item.namePlace}</Text>
                                                <Text style={styles.textoCidade}>{item.city}</Text>
                                                <Text style={styles.textoEndereco}>{item.address}</Text>
                                                <Text style={styles.textoDescricao}>{item.description}</Text>

                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={styles.styleFotoFamoso}>
                                                        <Image source={require('../img/FotoQuadrada/foto1.jpg')}
                                                            style={styles.fotoRedondaFamoso} />
                                                        <Text style={styles.textoNomeFamoso}>Yes, we can!</Text>
                                                    </View>
                                                    <View style={styles.styleFotoFamoso}>
                                                        <Image source={require('../img/FotoQuadrada/foto2.jpg')}
                                                            style={styles.fotoRedondaFamoso} />
                                                        <Text style={styles.textoNomeFamoso}>Criss</Text>
                                                    </View>
                                                    <View style={styles.styleFotoFamoso}>
                                                        <Image source={require('../img/FotoQuadrada/foto3.jpg')}
                                                            style={styles.fotoRedondaFamoso} />
                                                        <Text style={styles.textoNomeFamoso}>Gloria Groove</Text>
                                                    </View>
                                                </View>

                                                <Text style={styles.textoTituloPromocoes}>Promoções</Text>
                                                <Text style={styles.textoPromocoes}>- Rodada Dupla de SHOT</Text>
                                                <Text style={styles.textoPromocoes}>- Entrada OFF até 23h</Text>
                                                <Text style={styles.textoPromocoes}>- Combo de tequila até 02h</Text>
                                                <Text style={styles.textoPromocoes}></Text>
                                            </View>
                                        </ScrollView>
                                        <View style={{ width: 50, justifyContent: 'flex-end' }}>

                                            <TouchableHighlight onPress={this.toLike} underlayColor="transparent">

                                                <Image
                                                    style={styles.fotoMenu} source={this.carregaIcone(this.state.like)} />

                                            </TouchableHighlight>

                                            {/* <TouchableHighlight onPress={() => alert("Nenhuma função atribuida")} underlayColor="transparent">
                                        <Image source={require('../img/check_vazio_white.png')}
                                            style={styles.fotoMenu} />
                                    </TouchableHighlight> */}

                                            <TouchableHighlight onPress={() => navigate('CompraIngresso', { name: 'CompraIngresso' })} underlayColor="transparent">
                                                <Image source={require('../img/ticket_white.png')}
                                                    style={styles.fotoMenu} />
                                            </TouchableHighlight>

                                            <TouchableHighlight onPress={() => alert("Nenhuma função atribuida")} underlayColor="transparent">
                                                <Image source={require('../img/share_white.png')}
                                                    style={styles.fotoMenu} />
                                            </TouchableHighlight>

                                        </View>
                                    </View>
                                </View>
                                // </View>

                            ))
                        }
                    </ImageBackground>
                </ImageBackground>
            );
        }
    }

}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // justifyContent: 'space-between',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    containerLoading: {
        //padding: 20,
        flex: 1,
        justifyContent: 'center',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    screenMargin: {
        // padding: 36,
        flex: 1,
        justifyContent: 'space-between',
        marginHorizontal: 15,
        marginTop: 48,
        marginBottom: 20,
        // alignItems: 'center',
        // backgroundColor: '#4c4c4c'
    },
    fotoRedonda: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: -15,
        marginVertical: 20,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    fotoRedondaFamoso: {
        height: 80,
        width: 80,
        borderRadius: 40,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    fotoMenu: {
        height: 40,
        width: 40,
        // borderRadius: 20,
        // marginRight: 15,
        marginHorizontal: 5,
        marginVertical: 10,
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: { width: 10, height: 10 },
        // shadowRadius: 40,
    },
    styleFotoFamoso: {
        flexDirection: 'column',
        marginRight: 10,
        marginVertical: 20,
        width: 80,
    },
    textoNomeFamoso: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'space-between',
        marginTop: 5,
    },
    textoTituloPromocoes: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
        justifyContent: 'space-between',
        // marginBottom: 15,
    },
    textoPromocoes: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 10,
        textAlign: "left",
        justifyContent: 'space-between',
        marginTop: 5,
    },
    textoData: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "center",
        fontWeight: 'bold',
        marginBottom: -35,
        justifyContent: 'center',
    },
    textoNomeEvento: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
    },
    textoLocalizacao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        textAlign: "left",
    },
    textoCidade: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textoEndereco: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
        marginVertical: 20,
        // marginDown: 20,
    },
    textoDescricao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 10,
        textAlign: "left",
        // marginDown: 20,
    },

    // textoMenor: {
    //     color: '#fff',
    //     fontFamily: 'rockwell extra bold',
    //     fontSize: 20,
    // },
    // inputNome: {
    //     width: 300,
    //     //   borderWidth: 1,
    //     //   borderColor: '#999999',
    //     //   backgroundColor: '#EEEEEE',
    //     color: '#000',
    //     height: 38,
    //     margin: 20,
    //     padding: 10,
    // },
    // textInputFormat: {
    //     height: 60,
    //     width: 300,
    //     color: 'white',
    //     fontSize: 18
    // },
    backgroundImage: {
        width: '100%',
        flex: 1,
        // resizeMode: 'stretch',
        //backgroundColor: 'rgba(0, 0, 0, 1.0)', 
    },

});


const mapStateToProps = (state) => {
    return {
    }
};

const FeedEventosConnct = connect(mapStateToProps, { inserirDadosIngressos, inserirStrValTotal })(FeedEventos);

export default FeedEventosConnct;