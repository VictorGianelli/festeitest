import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert,
    ActivityIndicator,
    ScrollView,
    Platform,
    Image,
    FlatList,
    Switch,
    TouchableOpacity,
    TextInput,
    Button
} from 'react-native';
import BotaoTeste from '../components/BotaoTeste';
import BotaoComTexto from '../components/BotaoComTexto';
import Botao from '../components/Botao';
import { connect } from 'react-redux';
import { inserirDadosIngressos, inserirStrValTotal } from '../actions/AuthActions';
import {
    addIngresso
} from '../actions/ticket';
// import DadosIngresso from './DadosIngresso';

class ValorTotal extends Component {

    render() {
        return (
            <View>
                <Text style={styles.textoValor}
                    onChangeText={(texto) => this.props.strValueTotal(texto)}>R$ {this.props.strValueTotal}</Text>
            </View>
        );
    }
}

// const INITIAL_STATE = {
//     result: [1, 2, 3],
// };

export class Dados extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            empyt: [],
            cont: 0,
            valTotal: 0,
            strValueTotal: "0,00",
            opacity: 0.5,
            enabled: true,
            numeroTotal: 0,
            newName: "0,00",
            dataSource: [],
            showIndicator: true,
            result: [],
            value: '1',
            v1: "",
            somaValor1: "",
        };
        // this.state.result: [1, 2, 3];
    }

    onAddItem = () => {
        // not allowed AND not working
        // let val = Number(this.state.result);

        this.setState(state => {
            const result = state.result.push(state.value);
            return {
                result,
                // value: '1',
            };
        });
        Alert.alert("resultT", JSON.stringify(this.state.result));
    };

    componentDidMount() {
        fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/tables/event_compra_ingresso_teste.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: 1
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                // If server response message same as Data Matched
                if (responseJson === 'Result not found') {
                    Alert.alert("Erro", responseJson);
                    //Then open Profile activity and send user email to profile activity.
                    //this.props.navigation.navigate('Home', { name: 'Home' });

                    // Alert.alert("Feito",responseJson);

                } else {
                    this.setState({
                        dataSource: responseJson,
                        showIndicator: false,
                        result: responseJson,

                    })
                    this.props.dadosIngressos = this.state.dataSource;
                    //this.onAddItem();
                    Alert.alert("Data", this.props.strValueTotal);
                    // Alert.alert("Data",this.state.dataSource);

                }

            }).catch((error) => {
                console.error(error);
            });
        // this.setState(state => {
        //     const result = state.result.push(state.value);
        //     return {
        //       result,
        //       // value: '1',
        //     };
        //   });
        //Alert.alert("resultT2", JSON.stringify(this.state.result));
    }

    onChange = (newName) => {
        // Alert.alert("Valor Total2", JSON.stringify(this.props.strValueTotal) + "");
        this.setState({
            strValueTotal: newName
        })
        // alert(strValueTotal);
    }

    mais = (itemId, item) => {
        itemId -= 1;
        let state = this.state;

        let a = Number(item.number);
        a += 1;

        // alert(this.state.dataSource[itemId].preco);
        item.number = a;

        var value = 0;
        for (var i = 0; i < this.state.dataSource.length; i++) {
            value += (Number(this.state.dataSource[i].preco) + Number(this.state.dataSource[i].taxa)) * this.state.dataSource[i].number;
        }

        state.strValTotal = value.toFixed(2);
        state.strValTotal = state.strValTotal.replace(/\./g, ',');

        state.opacity = 1;
        state.enabled = false;

        let dataSource = [...this.state.dataSource];
        dataSource[itemId] = { ...dataSource[itemId], number: value };

        this.setState({ dataSource });

        this.setState(state);

        // _storeData = async () => {
        //     try {
        //       await AsyncStorage.setItem('@MySuperStore:key', 'I like to save it.');
        //     } catch (error) {
        //       // Error saving data
        //     }
        //   };

        // Alert.alert("Data", state.strValTotal);
        // Alert.alert("Valor Total2", state.strValTotal + "");
    }

    menos = (itemId, item) => {
        let state = this.state;

        if (item.number > 0) {
            let a = Number(item.number);
            a -= 1;

            // alert(this.state.dataSource[itemId].preco);
            item.number = a;

            var value = 0;
            for (var i = 0; i < this.state.dataSource.length; i++) {
                value += (Number(this.state.dataSource[i].preco) + Number(this.state.dataSource[i].taxa)) * this.state.dataSource[i].number;
            }

            state.strValTotal = value.toFixed(2);
            state.strValTotal = state.strValTotal.replace(/\./g, ',');

            state.opacity = 1;
            state.enabled = false;

            let dataSource = [...this.state.dataSource];
            dataSource[itemId] = { ...dataSource[itemId], number: value };

            // Alert.alert("Valor state", JSON.stringify(dataSource[itemId]) + "");

            this.setState({ dataSource });
            this.setState(state);
        }
        // Alert.alert(this.props.dadosIngressos[0].strValTotal);
    }

    showArrayItem = () => {

        Alert.alert(this.props.strValueTotal + " - " + this.state.strValTotal);

    }

    teste = () => {
        // this.props.dadosIngressos=this.state.dataSource;
        // Alert.alert("Valor state", JSON.stringify(this.state.dataSource) + "");

        // this.props.strValTotal = this.state.strValTotal;
        // this.setState(state => {
        //     const strValTotal = state.strValTotal.push(state.value);
        //     return {
        //         strValTotal,
        //         // value: '1',
        //     };
        // });
        // let strValTotal = [...this.props.strValTotal];
        // strValTotal = { ...dataSource[itemId], number: value };

        let dataSource = [...this.state.dataSource];
        v1 = "";

        // for (let i = 0; i < dataSource.length; i++) {
        for (let i = 0; i < 1; i++) {
            valortotal = "";
            valortotal = Number(dataSource[i].preco) + Number(dataSource[i].taxa);
            v1 += JSON.stringify(dataSource[i].id) + " " + JSON.stringify(dataSource[i].number) + " " + JSON.stringify(valortotal) + "\n";
        }

        this.props.onAddIngresso(quickly);

        Alert.alert("Valor state", v1);

        // Alert.alert("Valor props", JSON.stringify(this.props.strValueTotal) + "");
        // this.props.navigation.navigate('DadosIngresso', { name: 'DadosIngresso' })
    }

    render() {
        let state = this.state;
        // if (this.props.strValTotal == undefined ? this.props.strValTotal = 0 : this.props.strValTotal = this.state.strValTotal);
        // if (state.newName == undefined ? state.newName = "" : null);
        // if (this.props.strValueTotal == undefined ? this.props.strValueTotal = "" : this.props.strValueTotal = "" );
        // if (this.state.strValTotal == "0,00" ? null : alert());
        // this.onChange;
        // .alert("Valor Total 3", JSON.stringify(this.props.strValueTotal) + "");
        // Alert.alert("4", JSON.stringify(this.state.strValueTotal));
        const { navigate } = this.props.navigation
        if (this.state.showIndicator) {
            return (
                <View style={styles.container}>
                    <View style={styles.containerLoading}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.MainContainer}>

                    <View style={styles.margem}>

                        <ScrollView
                            horizontal={true}
                            
                            // paginationStyle={{ padding: 10 }}
                            keyExtractor={item => item.id.toString()}
                            paddingStart={30}
                        // dot={<View style={{ backgroundColor: '#555', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                        // activeDot={<View style={{ backgroundColor: '#ED1E79', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                        >

                            {
                                this.state.dataSource.map((item, key) => (
                                    // onPress={this.showArrayItem.bind(this, String(item.age))}
                                    // <View style={{ marginTop: -150}}>
                                    <View key={item.id} style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0, marginTop: -135 }}>
                                        {/* Parte de cima */}

                                        {/* {this.state.dataSource.map((item, key) => (
                                        // <View key={item.id} style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0, marginTop: -135 }}>
                                        <View key={item.id} >

                                            </View>
                                    ))
                                    }     */}
                                        <View style={{ borderWidth: Number(item.border), borderColor: '#FF9D00', borderBottomLeftRadius: 17.23, borderBottomRightRadius: 15 }}>
                                            <Image source={require('../img/fundo_tela_evento.png')}
                                                style={styles.fotoRedonda} />
                                            <Image source={require('../img/filtro_background.png')}
                                                style={styles.filtroRedondo} />
                                            <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                                                <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={styles.textoDia}>{item.date}</Text>
                                                    <Text style={styles.textoBarra}>___</Text>
                                                    <Text style={styles.textoMes}>{item.month}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                                                    <Text style={styles.textoNomeEvento}>{item.nameEvent}</Text>
                                                    <Text style={styles.textoLocalizacao}>{item.namePlace}</Text>
                                                    <Text style={styles.textoCidade}>{item.city}</Text>
                                                </View>
                                            </View>
                                        </View>


                                        {/* Parte de baixo */}
                                        <View style={{ borderWidth: Number(item.border), borderColor: '#FF9D00', padding: 20, height: 150, width: 300, backgroundColor: '#262626', borderRadius: 15, flexDirection: 'column' }}>
                                            {/* <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100 }}> */}
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 7 }}>
                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Black", fontSize: 14 }}>{item.type}</Text>
                                                <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Medium", fontSize: 14 }}>R$ {item.preco} (+{item.taxa})</Text>
                                            </View>
                                            <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Light", fontSize: 16 }}>{this.state.nome}</Text>
                                            <Text style={{ color: '#F15A24', fontFamily: "SFUIDisplay-Bold", fontSize: 14 }}>{this.state.cpf}</Text>
                                            <View style={{ alignItems: 'baseline', paddingTop: 17 }}>
                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>{this.state.telefone}</Text>
                                                <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>{this.state.email}</Text>
                                            </View>
                                            {/* </View> */}
                                        </View>

                                        <View>
                                            <Text style={{ color: '#fff', fontSize: 50, marginVertical: -189 }}>- - - - - - - - - -</Text>
                                        </View>
                                        {/* <View style={{ height: 50, width: 300 }}></View> */}

                                        <View style={{
                                            flexDirection: 'row', justifyContent: 'flex-start',
                                            alignItems: 'center', paddingTop: 15
                                        }}>
                                            <Switch
                                                style={{ padding: 0 }}
                                                onValueChange={this.toggleSwitch}
                                                value={this.state.switchValue}
                                                thumbColor='white'
                                                trackColor='white' />
                                            <Text style={{ color: '#fff', padding: 5 }}>Preencher meus dados salvos</Text>
                                        </View>

                                        <View style={{ width: '80%' }}>
                                            <TextInput
                                                placeholder=" Nome"
                                                placeholderTextColor="#fff"
                                                underlineColorAndroid="white"
                                                style={styles.textInputFormat}
                                                onChangeText={(textNome) => this.setState({ nome: textNome })}
                                            //value={item.nome}
                                            // onChange={this.mais.bind(this, key)}
                                            />

                                            {/* <Text style={styles.TextStyle}> Nome : {this.state.textNome}</Text> */}
                                            <TextInput
                                                placeholder=" CPF"
                                                placeholderTextColor="#fff"
                                                underlineColorAndroid="white"
                                                maxLength={11}
                                                style={styles.textInputFormat}
                                                onChangeText={(textCpf) => this.setState({ cpf: textCpf })}
                                                keyboardType={'numeric'}
                                            //value={item.cpf}
                                            />
                                            <TextInput
                                                placeholder=" Telefone DDD 9.9999-9999"
                                                placeholderTextColor="#fff"
                                                underlineColorAndroid="white"
                                                maxLength={11}
                                                style={styles.textInputFormat}
                                                onChangeText={(textTelefone) => this.setState({ telefone: textTelefone })}
                                                keyboardType={'numeric'}
                                            //value={item.telefone}
                                            />
                                            <TextInput
                                                placeholder=" E-mail"
                                                placeholderTextColor="#fff"
                                                underlineColorAndroid="white"
                                                autoCapitalize="none"
                                                autoCorrect={false}
                                                keyboardType={'email-address'}
                                                style={styles.textInputFormat}
                                                onChangeText={(textEmail) => this.setState({ email: textEmail })}
                                            //value={item.email}
                                            />
                                            <TextInput
                                                placeholder=" Nascimento"
                                                placeholderTextColor="#fff"
                                                underlineColorAndroid="white"
                                                maxLength={10}
                                                style={styles.textInputFormat}
                                                onChangeText={(textNascimento) => this.setState({ nascimento: textNascimento })}
                                                keyboardType={'numeric'}
                                            //value={item.nascimento}
                                            />
                                        </View>
                                    </View>

                                ))
                            }

                        </ScrollView>
                        <View style={{ alignItems: 'flex-start', marginStart: 40 }}>
                        
                            <Text style={styles.textoTotal}>Total {this.props.strValueTotal}</Text>
                            <ValorTotal strValueTotal={this.state.strValTotal} onNameChange={this.onChange} />
                            {/* <MyChild childName={this.state.parentName} onNameChange={this.onChange} /> */}
                        </View>


                        <View style={{ opacity: this.state.opacity }}>
                            <TouchableOpacity style={{ alignItems: 'center' }} >
                                <Botao cor="#fff" bdColor="#F15A24"
                                    bgColor="#F15A24" nome="Comprar" disable={this.state.enabled}
                                    // onPress={() => navigate('DadosIngresso', { name: 'DadosIngresso' })}
                                    onPress={() => this.teste()}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // justifyContent: 'space-between',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    containerLoading: {
        //padding: 20,
        flex: 1,
        justifyContent: 'center',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#1c1c1c'
    },
    TextStyle: {
        fontSize: 20,
        color: '#000',
        textAlign: 'left'
    },

    margem: {
        flex: 1
    },
    switchView: {
        width: 350,
        height: 450
    },
    texto: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        marginStart: 50,
        textAlign: "left",
    },
    textoTotal: {
        color: '#fff',
        fontSize: 18,
        marginStart: 20,
        textAlign: "left",
        fontFamily: "SFuidisplay-medium",
        alignItems: 'center'
    },
    textoValor: {
        color: '#F15A24',
        fontSize: 35,
        marginStart: 20,
        textAlign: "left",
        fontFamily: "SFuidisplay-medium",
        alignItems: 'center'
    },
    textoGrande: {
        color: '#fff',
        // fontFamily: 'rockwell extra bold',
        // fontSize: 50,
        textAlign: "left",
        // marginStart: 50,
        // marginEnd: 50,
        fontWeight: 'bold',
    },
    backgroundImage: {
        width: '100%',
        flex: 1,
        resizeMode: 'stretch',
        //backgroundColor: 'rgba(0, 0, 0, 1.0)',
    },
    fotoRedonda: {
        height: 533.6,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -333.6,
        // marginVertical: 20,
        // borderWidth: 2,borderColor: '#FF9D00'
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: {width: 10, height: 10 },
        // shadowRadius: 40,
    },
    filtroRedondo: {
        height: 533.6,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -533.6,
        // marginVertical: 20,

        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: {width: 10, height: 10 },
        // shadowRadius: 40,
    },

    textoMes: {
        color: '#fff',
        fontFamily: "SFuidisplay-Black",
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoBarra: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: "SFuidisplay-Medium",
        fontSize: 40,
        textAlign: "center",
        fontWeight: 'bold',
        marginBottom: -35,
        justifyContent: 'center',
    },
    textoNomeEvento: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
    },
    textoLocalizacao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        textAlign: "left",
    },
    textoCidade: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
    },

});

const mapStateToProps = (state) => {
    return {
        dadosIngressos: state.auth.dadosIngressos,
        strValueTotal: state.auth.strValueTotal
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onAddIngresso: (name) => { dispatch(addPlace(name)) },
    }
}

export default connect(mapStateToProps,{inserirStrValTotal})(Dados);

// export default ComprarConnct;