import React, { Component } from 'react';
import {
    StyleSheet, Text, View, Alert, ActivityIndicator, ScrollView, Platform, Image, FlatList, TouchableOpacity, TextInput, Button
} from 'react-native';
import BotaoTeste from '../components/BotaoTeste';
import BotaoComTexto from '../components/BotaoComTexto';
import Botao from '../components/Botao';
import { connect } from 'react-redux';
import { valoresIntermediarios, inserirDadosIngressos, inserirStrValTotal } from '../actions/AuthActions';

// import DadosIngresso from './DadosIngresso';


class ValorTotal extends Component {

    render() {
        return (
            <View>
                <Text style={styles.textoValor}
                    onChangeText={(texto) => this.props.strValueTotal(texto)}>R$ {this.props.strValueTotal}</Text>
            </View>
        );
    }
}

// const INITIAL_STATE = {
//     result: [1, 2, 3],
// };

export class Comprar extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            empyt: [],
            cont: 0,
            valTotal: 0,
            strValueTotal: "0,00",
            opacity: 0.5,
            enabled: true,
            numeroTotal: 0,
            newName: "0,00",
            dataSource: [],
            showIndicator: true,
            result: [],
            value: '1',
            v1: "",
            somaValor1: "",
            valoresIntermediarios: []      // id number somaValor
        };
        // this.state.result: [1, 2, 3];
    }

    onAddItem = () => {
        // not allowed AND not working
        // let val = Number(this.state.result);

        this.setState(state => {
            const result = state.result.push(state.value);
            return {
                result,
                // value: '1',
            };
        });
        Alert.alert("resultT", JSON.stringify(this.state.result));
    };

    componentDidMount() {
        fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/tables/event_compra_ingresso_teste.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((responseJson) => {

                if (responseJson === 'Result not found') {
                    Alert.alert("Erro", responseJson);

                } else {
                    this.setState({
                        dataSource: responseJson,
                        showIndicator: false,
                        result: responseJson,
                    })
                    this.props.dadosIngressos = this.state.dataSource;

                }

            }).catch((error) => {
                console.error(error);
            });

    }


    onChange = (newName) => {
        //Alert.alert("Valor Total2", JSON.stringify(this.props.strValueTotal) + "");
        this.setState({
            strValueTotal: newName
        })
        // alert(strValueTotal);
    }

    mais = (itemId, item) => {
        itemId -= 1;
        let state = this.state;

        let a = Number(item.number);
        a += 1;

        // alert(this.state.dataSource[itemId].preco);
        item.number = a;

        var value = 0;
        for (var i = 0; i < this.state.dataSource.length; i++) {
            value += (Number(this.state.dataSource[i].preco) + Number(this.state.dataSource[i].taxa)) * this.state.dataSource[i].number;
        }

        state.strValueTotal = value.toFixed(2);
        state.strValueTotal = state.strValueTotal.replace(/\./g, ',');

        state.opacity = 1;
        state.enabled = false;

        Alert.alert("strValueTotal", state.strValueTotal + "");

        let dataSource = [...this.state.dataSource];
        dataSource[itemId] = { ...dataSource[itemId], number: value };

        this.setState({ dataSource });
        this.setState({ strValueTotal: state.strValueTotal });

        this.setState(state);

        // _storeData = async () => {
        //     try {
        //       await AsyncStorage.setItem('@MySuperStore:key', 'I like to save it.');
        //     } catch (error) {
        //       // Error saving data
        //     }
        //   };

        // Alert.alert("Data", state.strValueTotal);
        // Alert.alert("Valor Total2", state.strValueTotal + "");

        var quickly = [String(this.state.count)];
        this.props.onAddPlace(quickly);
        // this.state.placeName = "";
        // this.state.number = "";
        this.setState({ count: this.state.count + 1 })
    }

    menos = (itemId, item) => {
        let state = this.state;

        if (item.number > 0) {
            let a = Number(item.number);
            a -= 1;

            // alert(this.state.dataSource[itemId].preco);
            item.number = a;

            var value = 0;
            for (var i = 0; i < this.state.dataSource.length; i++) {
                value += (Number(this.state.dataSource[i].preco) + Number(this.state.dataSource[i].taxa)) * this.state.dataSource[i].number;
            }

            state.strValueTotal = value.toFixed(2);
            state.strValueTotal = state.strValueTotal.replace(/\./g, ',');

            state.opacity = 1;
            state.enabled = false;

            let dataSource = [...this.state.dataSource];
            dataSource[itemId] = { ...dataSource[itemId], number: value };

            // Alert.alert("Valor state", JSON.stringify(dataSource[itemId]) + "");

            this.setState({ dataSource });
            this.setState(state);
        }
        // Alert.alert(this.props.dadosIngressos[0].strValueTotal);
    }

    showArrayItem = () => {

        Alert.alert(this.props.strValueTotal + " - " + this.state.strValueTotal);

    }

    teste = () => {

        let state = this.state;
        let dataSource = [...this.state.dataSource];
        v1 = "";

        for (let i = 0; i < dataSource.length; i++) {
            // i=0;
            somaValor = "";
            somaValor = Number(dataSource[i].preco) + Number(dataSource[i].taxa);

            let empyt = // id number somaValor
            {
                id: JSON.stringify(dataSource[i].id),
                number: JSON.stringify(dataSource[i].number),
                somaValor: JSON.stringify(somaValor)
            }
            this.props.valoresIntermediarios[i] = empyt;

            //v1 += state.valoresIntermediarios[i].id+" "+state.valoresIntermediarios[i].number+" "+state.valoresIntermediarios[i].somaValor+"\n";
        }

        //Alert.alert("Valor state", this.props.valoresIntermediarios);

        // Alert.alert("Valor props", JSON.stringify(this.props.strValueTotal) + "");
        this.props.navigation.navigate('DadosIngresso', { name: 'DadosIngresso' })
    }

    placesOutput = () => {
        return (

            <ScrollView
                horizontal={true}
                keyExtractor={item => item.id.toString()}
            // paginationStyle={{ bottom: -1 }}
            // dot={<View style={{ backgroundColor: '#555', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
            // activeDot={<View style={{ backgroundColor: '#ED1E79', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
            >

                {
                    this.state.dataSource.map((item, key) => (
                        // onPress={this.showArrayItem.bind(this, String(item.age))}

                        <View key={item.id} style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0 }}>
                            {/* Parte de cima */}
                            <View style={{ borderWidth: 1, borderColor: '#FF9D00', borderBottomLeftRadius: 17.23, borderBottomRightRadius: 17.23 }}>
                                <Image source={require('../img/fundo_tela_evento.png')}
                                    style={styles.fotoRedonda} />
                                <Image source={require('../img/filtro_background.png')}
                                    style={styles.filtroRedondo} />
                                <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                                    <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={styles.textoDia}>{item.date}</Text>
                                        <Text style={styles.textoBarra}>___</Text>
                                        <Text style={styles.textoMes}>{item.month}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                                        <Text style={styles.textoNomeEvento}>{item.nameEvent}</Text>
                                        <Text style={styles.textoLocalizacao}>{item.namePlace}</Text>
                                        <Text style={styles.textoCidade}>{item.city}</Text>
                                    </View>
                                </View>
                            </View>
                            {/* Parte de baixo */}
                            <View style={{ borderWidth: 1, borderColor: '#FF9D00', padding: 20, height: 150, width: 300, backgroundColor: '#262626', borderRadius: 17.23, flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100 }}>
                                    <Text style={{ color: '#fff', fontFamily: "SFUIText-Bold", fontSize: 14 }}>{item.type}</Text>
                                    <Text style={{ color: '#fff', fontFamily: "SFUIText-Light", fontSize: 16, paddingTop: 10 }}>R$ {item.preco} (+{item.taxa} taxa)</Text>
                                    <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Bold", fontSize: 14 }}>{item.how2pay}</Text>
                                    <View style={{ justifyContent: 'flex-end', }}>
                                        <Text style={{ color: '#fff', fontFamily: "SFUIText-LightItalic", fontSize: 14, paddingTop: 20 }}>{item.saleFinishedAt}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                        <BotaoTeste cor="#fff" bdColor="#4c4c4c"
                                            bgColor="transparent" nome="+"
                                            onPress={this.mais.bind(this, item.id, item)}//() => navigate('NovaConta', { name: 'NovaConta' })
                                        />
                                        <Text style={{ color: '#fff', fontSize: 20, fontFamily: "SFUIDisplay-Bold", marginRight: 14 }}>
                                            {item.number}
                                        </Text>
                                        <BotaoTeste cor="#fff" bdColor="#4c4c4c"
                                            bgColor="transparent" nome="-"
                                            onPress={this.menos.bind(this, item.id, item)}//() => navigate('NovaConta', { name: 'NovaConta' })
                                        />
                                    </View>
                                </View>
                            </View>

                            <View>
                                <Text style={{ color: '#fff', fontSize: 50, marginVertical: -189 }}>- - - - - - - - - -</Text>
                            </View>
                            <View style={{ height: 50, width: 300 }}></View>
                        </View>

                    ))
                }

            </ScrollView>

        )
    }

    render() {
        let state = this.state;
        // if (this.props.strValueTotal == undefined ? this.props.strValueTotal = 0 : this.props.strValueTotal = this.state.strValueTotal);
        // if (state.newName == undefined ? state.newName = "" : null);
        // if (this.props.strValueTotal == undefined ? this.props.strValueTotal = "" : this.props.strValueTotal = "" );
        // if (this.state.strValueTotal == "0,00" ? null : alert());
        // this.onChange;
        // Alert.alert("3", this.props.strValueTotal + "");
        const { navigate } = this.props.navigation
        if (this.state.showIndicator) {
            return (
                <View style={styles.container}>
                    <View style={styles.containerLoading}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                </View>
            );

        } else {
            return (
                <View style={styles.MainContainer}>

                    <View style={styles.margem}>

                        {this.placesOutput()}

                        <View style={{ alignItems: 'center' }}>
                            <BotaoComTexto cor="#fff" bdColor="#4c4c4c"
                                bgColor="transparent"
                                placeholderTextColor="#fff"
                                underlineColorAndroid="white"
                            />
                        </View>

                        {/* <TextInput                                                                       
                            style={{color:"#fff"}}
                            // placeholder="Cupom de Desconto"
                            placeholderTextColor="#fff"
                            underlineColorAndroid="transparent"
                            // onChangeText={(texto)=>this.props(texto)}
                            secureTextEntry={true}
                            value={this.props.desconto} ></TextInput> */}

                        <TouchableOpacity style={{ alignItems: 'center' }}>
                            <Botao cor="#fff" bdColor="#F15A24"
                                bgColor="transparent" nome="Aplicar Cupom"
                                onPress={() => alert("Nenhuma função atribuida")}
                            />
                        </TouchableOpacity>
                        {/* Parte de baixo  */}


                        <View style={{ alignItems: 'flex-start', marginStart: 40 }}>
                            <Text style={styles.textoTotal}>Total</Text>
                            <ValorTotal strValueTotal={state.strValueTotal} onNameChange={this.onChange} />
                            {/* <MyChild childName={this.state.parentName} onNameChange={this.onChange} /> */}
                        </View>


                        <View style={{ opacity: this.state.opacity }}>
                            <TouchableOpacity style={{ alignItems: 'center' }} >
                                <Botao cor="#fff" bdColor="#F15A24"
                                    bgColor="#F15A24" nome="Comprar" disable={this.state.enabled}
                                    // onPress={() => navigate('DadosIngresso', { name: 'DadosIngresso' })}
                                    onPress={() => this.teste()}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // justifyContent: 'space-between',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    containerLoading: {
        //padding: 20,
        flex: 1,
        justifyContent: 'center',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#1c1c1c'
    },
    TextStyle: {
        fontSize: 20,
        color: '#000',
        textAlign: 'left'
    },

    margem: {
        flex: 1
    },
    switchView: {
        width: 350,
        height: 450
    },
    texto: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        marginStart: 50,
        textAlign: "left",
    },
    textoTotal: {
        color: '#fff',
        fontSize: 18,
        marginStart: 20,
        textAlign: "left",
        fontFamily: "SFuidisplay-medium",
        alignItems: 'center'
    },
    textoValor: {
        color: '#F15A24',
        fontSize: 35,
        marginStart: 20,
        textAlign: "left",
        fontFamily: "SFuidisplay-medium",
        alignItems: 'center'
    },
    textoGrande: {
        color: '#fff',
        // fontFamily: 'rockwell extra bold',
        // fontSize: 50,
        textAlign: "left",
        // marginStart: 50,
        // marginEnd: 50,
        fontWeight: 'bold',
    },
    backgroundImage: {
        width: '100%',
        flex: 1,
        resizeMode: 'stretch',
        //backgroundColor: 'rgba(0, 0, 0, 1.0)',
    },
    fotoRedonda: {
        height: 533.6,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -333.6,
        // marginVertical: 20,
        // borderWidth: 2,borderColor: '#FF9D00'
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: {width: 10, height: 10 },
        // shadowRadius: 40,
    },
    filtroRedondo: {
        height: 533.6,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -533.6,
        // marginVertical: 20,

        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: {width: 10, height: 10 },
        // shadowRadius: 40,
    },

    textoMes: {
        color: '#fff',
        fontFamily: "SFuidisplay-Black",
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoBarra: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: "SFuidisplay-Medium",
        fontSize: 40,
        textAlign: "center",
        fontWeight: 'bold',
        marginBottom: -35,
        justifyContent: 'center',
    },
    textoNomeEvento: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
    },
    textoLocalizacao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        textAlign: "left",
    },
    textoCidade: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
    },

});

const mapStateToProps = (state) => {
    return {
        dadosIngressos: state.auth.dadosIngressos,
        strValueTotal: state.auth.strValueTotal
    }
};

const ComprarConnct = connect(mapStateToProps, { valoresIntermediarios, inserirDadosIngressos, inserirStrValTotal })(Comprar);

export default ComprarConnct;