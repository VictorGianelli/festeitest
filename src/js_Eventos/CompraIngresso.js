import React, { Component } from 'react';
import {
    BackHandler, CheckBox, Switch, StyleSheet, Text, View, Alert, ActivityIndicator, ScrollView, Platform, Image, FlatList, TouchableOpacity, TextInput, Button
} from 'react-native';
import BotaoTeste from '../components/BotaoTeste';
import BotaoComTexto from '../components/BotaoComTexto';
import Botao from '../components/Botao';
// import Botao from '../components/ListaIngresso';
import { connect } from 'react-redux';
// import { valoresIntermediarios, inserirDadosIngressos, inserirStrValTotal } from '../actions/AuthActions';

import {
    addTicket,
} from '../actions/ticket';

// import DadosIngresso from './DadosIngresso';

class ValorTotal extends Component {

    render() {
        return (
            <View>
                <Text style={styles.textoValor}
                    onChangeText={(texto) => this.props.strValueTotal(texto)}>R$ {this.props.strValueTotal}</Text>
            </View>
        );
    }
}

export class Comprar extends Component {

    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
    };

    ///////////////  ///////////////////////
    constructor(props) {
        super(props);
        this.state = {
            strValueTotal: "0,00",
            opacity: 0.5,
            enabled: true,
            newName: "0,00",
            showIndicator: true,
            result: [],
            v1: "",
            // valoresIntermediarios: [],      // id number somaValor
            p1: 0,
            p0: 0,
            switchValue: false,
            opc: 0.5,

            keyNumber: 1,
            count: 0,
            dataSource: [],
            ticket: [],
            ticketCompra: [],
            ticketComprado: [],
        };

        this.toggleSwitch = this.toggleSwitch.bind(this);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    /////////////////////////   /////////////////////

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    validarCPF = (i) => {
        const { cpf } = this.state.ticketCompra[i].cpf;

        strCPF = cpf;

        var Soma;
        var Resto;
        Soma = 0;

        if (strCPF == "00000000000") return false;
        if (strCPF == "") return false;

        for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10))) return false;
        console.log(1);

        Soma = 0;

        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11))) return false;
        console.log(2);

        return true;
    }

    toggleSwitch = (value, id) => {
        //onValueChange of the switch this function will be called
        // this.setState({ switchValue: value })
        let ticketComprado = [...this.state.ticketComprado];
        ticketComprado[0] = { ...ticketComprado[0], switchValue: value };
        // alert(id)
        this.change(value)
        console.log(id + " " + value)

        this.setState({ ticketComprado });
    }

    onAddItem = () => {

        this.setState(state => {
            const result = state.result.push(state.value);
            return {
                result,
            };
        });
    };

    componentDidMount() {
        fetch('http://festeiphptest2.sa-east-1.elasticbeanstalk.com/tables/event_compra_ingresso_teste.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((responseJson) => {

                if (responseJson === 'Result not found') {
                    Alert.alert("Erro", responseJson);

                } else {
                    this.setState({
                        dataSource: responseJson,
                        showIndicator: false,
                        result: responseJson,
                    })
                    this.props.dadosIngressos = this.state.dataSource;

                    let ticket = [...this.state.ticket];
                    // let a = 0;

                    for (var i = 0; i < this.state.dataSource.length; i++) {
                        ticket[i] = { ...ticket[i], id: i };
                        ticket[i] = { ...ticket[i], tipo: this.state.dataSource[i].type };
                        ticket[i] = { ...ticket[i], count: 0 };
                    }
                    this.setState({ ticket });
                }

            }).catch((error) => {
                console.error(error);
            });

    }

    onChange = (newName) => {
        this.setState({
            strValueTotal: newName
        })
    }

    fill = () => {
        let ticketComprado = [...this.state.ticketComprado];
        ticketComprado[1] = { ...ticketComprado[1], nome: 'textValue' };
        this.state.stop = false

        // this.setState({ ticketComprado});
        // alert(12);
    }

    alterText = (id, item, textValue, value) => {

        let ticketComprado = [...this.state.ticketComprado];
        let ticketCompra = [...this.state.ticketCompra];
        let itemT = [item];

        if (value == "nome") {
            ticketCompra[id] = { ...ticketCompra[id], nome: textValue };
            ticketComprado[id] = { ...ticketComprado[id], nome: textValue };
        } else if (value == "cpf") {
            ticketComprado[id] = { ...ticketComprado[id], cpf: textValue };
            // let text = [textValue];
            // if (textValue.length > 2 ? textValue = textValue + "." : null);
            ticketCompra[id] = { ...ticketCompra[id], cpf: textValue };
        } else if (value == "telefone") {
            ticketCompra[id] = { ...ticketCompra[id], telefone: textValue };
            ticketComprado[id] = { ...ticketComprado[id], telefone: textValue };
        } else if (value == "email") {
            ticketCompra[id] = { ...ticketCompra[id], email: textValue };
            ticketComprado[id] = { ...ticketComprado[id], email: textValue };
        } else if (value == "nascimento") {
            ticketCompra[id] = { ...ticketCompra[id], nascimento: textValue };
            ticketComprado[id] = { ...ticketComprado[id], nascimento: textValue };
        }

        // alert(JSON.stringify(ticketCompra[id].email))
        this.setState({
            ticketCompra,
            ticketComprado
        });
    }

    mais = (itemId, item, ) => {
        itemId -= 1;
        let state = this.state;

        let a = Number(item.number);
        a += 1;

        item.number = a;

        var value = 0;
        for (var i = 0; i < this.state.dataSource.length; i++) {
            value += (Number(this.state.dataSource[i].preco) + Number(this.state.dataSource[i].taxa)) * this.state.dataSource[i].number;
        }

        state.strValueTotal = value.toFixed(2);
        state.strValueTotal = state.strValueTotal.replace(/\./g, ',');

        state.opacity = 1;
        state.enabled = false;


        let dataSource = [...this.state.dataSource];
        dataSource[itemId] = { ...dataSource[itemId], number: value };

        this.setState({ dataSource });

        let ticket = [...this.state.ticket];

        var valorUnico = Number(item.preco) + Number(item.taxa);
        valorUnico = valorUnico.toFixed(2);
        valorUnico = valorUnico.replace(/\./g, ',');

        ticket[itemId] = { ...ticket[itemId], tipo: item.type };
        ticket[itemId] = { ...ticket[itemId], count: a };
        ticket[itemId] = { ...ticket[itemId], valor: valorUnico };

        this.setState({ ticket });
        // Alert.alert("this.state.ticket.length", JSON.stringify(ticket) + " " + this.state.ticket.length);

        this.state.ticket = ticket;
        this.setState({ strValueTotal: state.strValueTotal });

        this.setState(state);
    }

    menos = (itemId, item) => {
        itemId -= 1;
        let state = this.state;

        if (item.number > 0) {
            let a = Number(item.number);
            a -= 1;

            item.number = a;

            var value = 0;
            for (var i = 0; i < this.state.dataSource.length; i++) {
                value += (Number(this.state.dataSource[i].preco) + Number(this.state.dataSource[i].taxa)) * this.state.dataSource[i].number;
            }

            state.strValueTotal = value.toFixed(2);
            state.strValueTotal = state.strValueTotal.replace(/\./g, ',');

            state.opacity = 1;
            state.enabled = false;

            let dataSource = [...this.state.dataSource];
            dataSource[itemId] = { ...dataSource[itemId], number: value };

            let ticket = [...this.state.ticket];
            ticket[itemId] = { ...ticket[itemId], count: a };

            this.state.ticket[itemId] = ticket[itemId];

            this.setState({
                dataSource,
                ticket
            });
            this.setState(state);

            if (value == 0) {
                state.opacity = 0.5;
                state.enabled = true;
            }
        }
    }

    teste = () => {
        if (this.state.p0 == 0) {
            let state = this.state;
            v1 = "";
            tot = 0;
            for (let i = 0; i < this.state.dataSource.length; i++) {
                let dT = 0;
                if (this.state.dataSource[i].number == "0" ? dT = 0 : dT = this.state.dataSource[i].number);
                tot += dT;
                v1 += this.state.dataSource[i].type + " = " + dT + " / ";

                var quickly = [String(this.state.count), this.state.dataSource[i].type, 0];
                this.props.onAddTicket(quickly);

            }
            v1 += " total = " + tot;

            v1 = "";
            conti = 0;
            a = 1;

            values = "";
            for (let i = 0; i < this.state.dataSource.length; i++) {
                values += "i" + i + " ";
                try {
                    for (let j = 0; j < this.state.ticket[i].count; j++) {
                        values += "j" + j + "/" + a + " ";
                        let ticketCompra = [...this.state.ticketCompra];
                        let ticketComprado = [...this.state.ticketComprado];
                        let dataSource = [...this.state.dataSource];


                        ticketCompra[conti] = { ...dataSource[i] };
                        ticketCompra[conti] = { ...ticketCompra[conti], idCompra: conti };
                        ticketCompra[conti] = { ...ticketCompra[conti], tipo: this.state.ticket[i].tipo };
                        ticketCompra[conti] = { ...ticketCompra[conti], valor: this.state.ticket[i].valor };
                        ticketCompra[conti] = { ...ticketCompra[conti], nome: "Nome" };
                        ticketCompra[conti] = { ...ticketCompra[conti], cpf: "Cpf" };
                        ticketCompra[conti] = { ...ticketCompra[conti], telefone: "99 99999-9999" };
                        ticketCompra[conti] = { ...ticketCompra[conti], email: "seu@mail.com" };
                        ticketCompra[conti] = { ...ticketCompra[conti], nascimento: "" };
                        ticketComprado[conti] = { ...ticketComprado[conti], id: a };
                        ticketComprado[conti] = { ...ticketComprado[conti], tipo: this.state.ticket[i].tipo };
                        ticketComprado[conti] = { ...ticketComprado[conti], valor: this.state.ticket[i].valor };
                        ticketComprado[conti] = { ...ticketComprado[conti], switchValue: false };
                        ticketComprado[conti] = { ...ticketComprado[conti], valor: this.state.ticket[i].valor };

                        this.state.ticketCompra[conti] = ticketCompra[conti];
                        this.state.ticketComprado[conti] = ticketComprado[conti];

                        conti++;
                        a++;
                    }
                } catch (e) {
                    console.log(e);
                }
                values += "\n";
            }
            values += a;
            // Alert.alert("tickets", values);


            this.setState({ p0: 1 });


        } else if (this.state.p0 == 1) {
            // Alert.alert("fim", JSON.stringify(this.state.ticketCompra));
            //this.validarCPF.bind(this);
            let ticketComprado = [...this.state.ticketComprado];
            let ticketCompra = [...this.state.ticketCompra];
            var resto = this.state.ticketCompra.length;
            // console.log("leng = 10 "+this.state.ticketCompra[i].telefone.length <= 10 );
            // console.log("und"+this.state.ticketComprado[i].cpf == undefined);

            for (var i = 0; i < this.state.ticketCompra.length; i++) {
                if (ticketCompra[i].nome.length == 0 || ticketComprado[i].nome == undefined) {
                    Alert.alert("", "Nome inválido no " + (i + 1) + "° ticket");
                } else if (ticketCompra[i].cpf.length != 11 || ticketComprado[i].cpf == undefined) {
                    Alert.alert("", "Cpf inválido no " + (i + 1) + "° ticket");
                } else if (ticketCompra[i].telefone.length <= 10 || ticketComprado[i].telefone == undefined) {
                    Alert.alert("", "Telefone inválido no " + (i + 1) + "° ticket");
                } else if (ticketCompra[i].email.length == 0 || ticketComprado[i].email == undefined) {
                    Alert.alert("", "E-mail inválido no " + (i + 1) + "° ticket");
                } else if (ticketCompra[i].nascimento.length == 0 || ticketComprado[i].nascimento == undefined) {
                    Alert.alert("", "Data de nascimento inválida no " + (i + 1) + "° ticket");
                } else {
                    //Alert.alert("ticketComprado", JSON.stringify(this.state.ticketComprado));
                    resto -= 1;
                }
            }

            console.log(resto);

            if (resto == 0) {
                let confirmar = "";
                for (var j = 0; j < this.state.ticketCompra.length; j++) {
                    if (j == 0 || j == this.state.ticketCompra.length ? null : confirmar += "\n \n");
                    confirmar += "Ticket " + (j + 1) + "\nnome : " + ticketCompra[j].nome + "\ncpf : " + ticketCompra[j].cpf + "\ntelefone : " +
                        ticketCompra[j].telefone + "\nemail : " + ticketCompra[j].email + "\nnascimento : " + ticketCompra[j].nascimento;
                }

                Alert.alert(
                    'Confirmar informaçoes',
                    confirmar,
                    [
                        { text: 'Corrigir', onPress: () => console.log('Corrigir') },
                        //   {
                        //     text: 'Cancelar',
                        //     onPress: () => console.log('Cancelared'),
                        //     style: 'cancel',
                        //   },
                        { text: 'Continuar', onPress: () => this.alterP() },
                    ],
                    { cancelable: true },
                );

                this.state.opacity = 0.5;
                this.state.enabled = true;
            }

            // this.setState({ p0: 2 })
        } else if (this.state.p0 == 2) {
            // Alert.alert("fim", JSON.stringify(this.state.ticketCompra));
            Alert.alert(
                "Fim desse processo da compra",
                "O JSON formado: " + JSON.stringify(this.state.ticketComprado),
                // [
                //     { text: 'Corrigir', onPress: () => console.log('Corrigir') },
                //     //   {
                //     //     text: 'Cancelar',
                //     //     onPress: () => console.log('Cancelared'),
                //     //     style: 'cancel',
                //     //   },
                //     { text: 'Ok', onPress: () => this.alterP() },
                // ],
                // { cancelable: true },
            );
        }
    }

    alterP = () => {
        this.setState({
            p0: 2,
            p1: 1
        });
    }

    change = (test) => {
        let ticketComprado = [...this.state.ticketComprado];
        ticketComprado[0] = { ...ticketComprado[0], switchValue: test };

        this.state.ticketComprado[0] = ticketComprado[0];
        // ticketComprado[1] = { ...ticketComprado[1], switchValue: true };
        //this.setState({ ticketComprado });
        alert(JSON.stringify(ticketComprado[0]));
    }

    placesOutput = () => {
        if (this.state.p0 == 0) {
            return (
                <View style={{ width: '100%', height: '75%', }}>

                    <ScrollView
                        horizontal={true}
                        keyExtractor={item => item.id.toString()}

                        showsHorizontalScrollIndicator={false}
                        pagingEnabled
                    // paginationStyle={{ bottom: -1 }}
                    // dot={<View style={{ backgroundColor: '#555', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                    // activeDot={<View style={{ backgroundColor: '#ED1E79', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                    >

                        {
                            this.state.dataSource.map((item, key) => (
                                // onPress={this.showArrayItem.bind(this, String(item.age))}

                                <View key={item.id} style={{ marginHorizontal: 0, alignItems: 'center', width: '50%', height: 550, paddingHorizontal: 0 }}>
                                    {/* Parte de cima */}
                                    <View style={{ borderWidth: Number(item.border), borderColor: '#FF9D00', borderBottomLeftRadius: 17.23, borderBottomRightRadius: 17.23 }}>
                                        <Image source={require('../img/fundo_tela_evento.png')}
                                            style={styles.fotoRedonda} />
                                        <Image source={require('../img/filtro_background.png')}
                                            style={styles.filtroRedondo} />
                                        <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={styles.textoDia}>{item.date}</Text>
                                                <Text style={styles.textoBarra}>___</Text>
                                                <Text style={styles.textoMes}>{item.month}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                                                <Text style={styles.textoNomeEvento}>{item.nameEvent}</Text>
                                                <Text style={styles.textoLocalizacao}>{item.namePlace}</Text>
                                                <Text style={styles.textoCidade}>{item.city}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    {/* Parte de baixo */}
                                    <View style={{ borderWidth: Number(item.border), borderColor: '#FF9D00', padding: 20, height: 150, width: 300, backgroundColor: '#262626', borderRadius: 17.23, flexDirection: 'row' }}>
                                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', width: 300, marginRight: -100 }}>
                                            <Text style={{ color: '#fff', fontFamily: "SFUIText-Bold", fontSize: 14 }}>{item.type}</Text>
                                            <Text style={{ color: '#fff', fontFamily: "SFUIText-Light", fontSize: 16, paddingTop: 10 }}>R$ {item.preco} (+{item.taxa} taxa)</Text>
                                            <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Bold", fontSize: 14 }}>{item.how2pay}</Text>
                                            <View style={{ justifyContent: 'flex-end', }}>
                                                <Text style={{ color: '#fff', fontFamily: "SFUIText-LightItalic", fontSize: 14, paddingTop: 20 }}>{item.saleFinishedAt}</Text>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column' }}>
                                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                                <BotaoTeste cor="#fff" bdColor="#4c4c4c"
                                                    bgColor="transparent" nome="+"
                                                    onPress={this.mais.bind(this, item.id, item)}//() => navigate('NovaConta', { name: 'NovaConta' })
                                                />
                                                <Text style={{ color: '#fff', fontSize: 20, fontFamily: "SFUIDisplay-Bold", marginRight: 14 }}>
                                                    {item.number}
                                                </Text>
                                                <BotaoTeste cor="#fff" bdColor="#4c4c4c"
                                                    bgColor="transparent" nome="-"
                                                    onPress={this.menos.bind(this, item.id, item)}//() => navigate('NovaConta', { name: 'NovaConta' })
                                                />
                                            </View>
                                        </View>
                                    </View>

                                    <View>
                                        <Text style={{ color: '#fff', fontSize: 50, marginVertical: -189 }}>- - - - - - - - - -</Text>
                                    </View>
                                    <View style={{ height: 50, width: 300 }}></View>
                                </View>

                            ))
                        }

                    </ScrollView>

                    <View style={{ alignItems: 'center' }}>
                        <BotaoComTexto cor="#fff" bdColor="#4c4c4c"
                            bgColor="transparent"
                            placeholderTextColor="#fff"
                            underlineColorAndroid="white"
                        />
                    </View>

                    <TouchableOpacity style={{ alignItems: 'center' }}>
                        <Botao cor="#fff" bdColor="#F15A24"
                            bgColor="transparent" nome="Aplicar Cupom"
                            onPress={() => Alert.alert("this.state.ticket", this.state.ticket + "")}
                        />
                    </TouchableOpacity>
                </View>
            )

        } else if (this.state.p0 == 1) {
            // Alert.alert("try", JSON.stringify(this.state.ticketCompra) + "");
            // if ((this.state.switchValue == true) ? this.change() : null);
            // if ((item.nome.length == 0) ? item.nome="nome" : null);
            // Alert.alert("p0 ",JSON.stringify(this.state.p0));
            return (
                <View style={{ width: '100%', height: '75%', }}>

                    <ScrollView
                        horizontal={true}
                        // paginationStyle={{ bottom: -1 }}
                        keyExtractor={item => item.idCompra.toString()}
                        extraData={this.state}
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled
                    // dot={<View style={{ backgroundColor: '#555', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                    // activeDot={<View style={{ backgroundColor: '#ED1E79', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, }} />}
                    >

                        {
                            this.state.ticketCompra.map((item, index) => (

                                <View key={item.idCompra} style={{ marginHorizontal: 7, alignItems: 'center', width: 305, height: 550, paddingHorizontal: 0, marginTop: -135 }}>

                                    <View style={{ borderWidth: Number(item.border), borderColor: '#FF9D00', borderBottomLeftRadius: 17.23, borderBottomRightRadius: 15 }}>
                                        <Image source={require('../img/fundo_tela_evento.png')}
                                            style={styles.fotoRedonda} />
                                        <Image source={require('../img/filtro_background.png')}
                                            style={styles.filtroRedondo} />
                                        <View style={{ width: 300, height: 130, flexDirection: 'row', marginTop: -130, justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={styles.textoDia}>{item.date}</Text>
                                                <Text style={styles.textoBarra}>___</Text>
                                                <Text style={styles.textoMes}>{item.month}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                                                <Text style={styles.textoNomeEvento}>{item.nameEvent}</Text>
                                                <Text style={styles.textoLocalizacao}>{item.namePlace}</Text>
                                                <Text style={styles.textoCidade}>{item.city}</Text>
                                            </View>
                                        </View>
                                    </View>


                                    <View style={{ borderWidth: Number(item.border), borderColor: '#FF9D00', padding: 20, height: 150, width: 300, backgroundColor: '#262626', borderRadius: 15, flexDirection: 'column' }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 7 }}>
                                            <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Black", fontSize: 14 }}>{item.type}</Text>
                                            <Text style={{ color: '#F15A24', fontFamily: "SFUIText-Medium", fontSize: 14 }}>R$ {item.preco} (+{item.taxa})</Text>
                                        </View>
                                        <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-Light", fontSize: 16 }}>{item.nome}</Text>
                                        <Text style={{ color: '#F15A24', fontFamily: "SFUIDisplay-Bold", fontSize: 14 }}>{item.cpf}</Text>
                                        <View style={{ alignItems: 'baseline', paddingTop: 17 }}>
                                            <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>{item.telefone}</Text>
                                            <Text style={{ color: '#fff', fontFamily: "SFUIDisplay-LightItalic", fontSize: 14 }}>{item.email}</Text>
                                        </View>
                                    </View>

                                    <View>
                                        <Text style={{ color: '#fff', fontSize: 50, marginVertical: -189 }}>- - - - - - - - - -</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingTop: 15 }}>
                                        {/* <Switch
                                            style={{ padding: 0 }}
                                            onValueChange={this.toggleSwitch}
                                            value={this.state.switchValue}
                                            thumbColor='white'
                                            trackColor='white' /> */}
                                        <Switch
                                            style={{ padding: 0 }}
                                            onValueChange={this.toggleSwitch.bind(item.id)}
                                            value={item.switchValue}
                                            // onValueChange={(value) => {this.setState({ ticketComprado: { [item.idCompra]: value }})}}
                                            // value={this.state.switchValue[item.idCompra]} 
                                            // onPress={this.fill()}
                                            thumbColor='white'
                                            trackColor='white' />

                                        <Text style={{ color: '#fff', padding: 5 }}>Preencher meus dados salvos</Text>
                                    </View>

                                    <View style={{ width: '80%' }}>
                                        <TextInput
                                            placeholder=" Nome"
                                            placeholderTextColor="#fff"
                                            underlineColorAndroid="white"
                                            style={styles.textInputFormat}
                                            onChangeText={(textNome) => this.alterText(item.idCompra, item, textNome, "nome")}
                                        />

                                        {/* <Text style={styles.TextStyle}> Nome : {this.state.textNome}</Text> */}
                                        <TextInput
                                            placeholder=" CPF"
                                            placeholderTextColor="#fff"
                                            underlineColorAndroid="white"
                                            maxLength={11}
                                            style={styles.textInputFormat}
                                            onChangeText={(textCpf) => this.alterText(item.idCompra, item, textCpf, "cpf")}
                                            keyboardType={'numeric'}
                                        />
                                        <TextInput
                                            placeholder=" Telefone DDD 9.9999-9999"
                                            placeholderTextColor="#fff"
                                            underlineColorAndroid="white"
                                            maxLength={11}
                                            style={styles.textInputFormat}
                                            onChangeText={(textTelefone) => this.alterText(item.idCompra, item, textTelefone, "telefone")}
                                            keyboardType={'numeric'}
                                        />
                                        <TextInput
                                            placeholder=" E-mail"
                                            placeholderTextColor="#fff"
                                            underlineColorAndroid="white"
                                            autoCapitalize="none"
                                            autoCorrect={false}
                                            keyboardType={'email-address'}
                                            style={styles.textInputFormat}
                                            onChangeText={(textEmail) => this.alterText(item.idCompra, item, textEmail, "email")}
                                        />
                                        <TextInput
                                            placeholder=" Nascimento"
                                            placeholderTextColor="#fff"
                                            underlineColorAndroid="white"
                                            maxLength={10}
                                            style={styles.textInputFormat}
                                            onChangeText={(textNascimento) => this.alterText(item.idCompra, item, textNascimento, "nascimento")}
                                            keyboardType={'numeric'}
                                        />
                                    </View>
                                </View>

                            ))
                        }

                    </ScrollView>

                </View>
            )
        } else if (this.state.p0 == 2) {
            // Alert.alert("try", JSON.stringify(this.state.ticketCompra) + "");
            // if ((this.state.switchValue == true) ? this.change() : null);
            // Alert.alert("id",JSON.stringify(this.state.checked));
            console.log(JSON.stringify(this.state.opc))
            return (
                <View style={{ width: '100%', height: '75%', }}>

                    <ScrollView>
                        <View>

                            <View style={{ padding: 10, width: 350, marginHorizontal: 38 }}>
                                <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 20 }}>"Termos e condições gerais de uso e/ou de venda</Text>
                                <Text style={{ color: '#fff', fontSize: 16 }}>{"\n"}Por meio de seus termos e condições gerais de uso e/ou de venda, um site ou um aplicativo explica aos usuários quais são as condições de utilização do serviço disponibilizado através de sua página ou programa, seja ele gratuito ou pago. Além de informar o usuário sobre a necessidade de cadastro ou sobre os elementos protegidos por direitos autorais, este instrumento determina, ainda, as responsabilidades de cada uma das partes - editor (pessoa que mantém o site ou aplicativo) e usuário -, em relação ao uso do serviço.
      {"\n"}
                                    {"\n"}A cláusula mais importante dos termos e condições gerais de uso é aquela que define a política de privacidade do site ou aplicativo. Nela, o editor comunica claramente ao usuário como serão protegidas ou utilizadas as informações que este fornece ao site ou ao aplicativo, sejam elas dados pessoais ou dados de navegação.
      {"\n"}
                                    {"\n"}Nos casos em que o site ou o aplicativo colocar à venda produtos ou serviços, este documento virá, também, complementado pelas condições gerais de venda, que regularão os detalhes desta transação, tais como as formas de pagamento, a entrega e a política de trocas e devoluções.
      {"\n"}
                                    {"\n"}Neste documento ainda será possível adaptar a política de privacidade do site ou aplicativo ao Regulamento Geral de Proteção de Dados da União Europeia (RGPD), também conhecido como General Data Protection Regulation (GDPR)."</Text>

                                {/* <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingTop: 15 }}>
                                    <CheckBox
                                        value={this.state.checked}
                                        labelStyle={{ color: 'white' }}
                                        onValueChange={() => this.opacityChange()}
                                    // onValueChange={() => this.setState({ checked: !this.state.checked,
                                    //     opacity : this.state.opc, enabled : this.state.checked})}

                                    />
                                    <Text style={{ color: '#fff', padding: 5 }}>Eu concordo com todos os termos e condições acima</Text>
                                </View> */}
                            </View>

                        </View>
                    </ScrollView>
                </View>
            )
        }
    }

    _render = () => {
        if (this.state.p1 == 0) {
            return (
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ alignItems: 'flex-start', width: 300, height: 60 }}>
                        <Text style={styles.textoTotal}>Total</Text>
                        <ValorTotal strValueTotal={this.state.strValueTotal} onNameChange={this.onChange} />
                    </View>

                </View>
            )
        } else if (this.state.p1 == 1) {
            return (
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>

                    <View style={{ backgroundColor:"#333", flexDirection: 'row', alignItems: 'center', width: 300, height: 60 }}>
                        <CheckBox
                            value={this.state.checked}
                            labelStyle={{ color: 'white' }}
                            onValueChange={() => this.opacityChange()}
                        // onValueChange={() => this.setState({ checked: !this.state.checked,
                        //     opacity : this.state.opc, enabled : this.state.checked})}

                        />
                        <Text style={{ color: '#fff', padding: 5 }}>Eu concordo com todos os termos e condições acima</Text>
                    </View>
                </View>
            )
        }
    }

    opacityChange = () => {
        // console.log(this.state.checked)
        if (this.state.checked ? this.state.opc = 0.5 : this.state.opc = 1);
        this.setState({
            checked: !this.state.checked,
            opacity: this.state.opc, enabled: this.state.checked
        })
        // alert(check);
    }

    render() {

        let state = this.state;
        // if (this.props.strValueTotal == undefined ? this.props.strValueTotal = 0 : this.props.strValueTotal = this.state.strValueTotal);
        // if (state.ticket == undefined ? null : this.tickets);
        // if (this.props.strValueTotal == undefined ? this.props.strValueTotal = "" : this.props.strValueTotal = "" );
        // if (this.state.strValueTotal == "0,00" ? null : alert());
        // this.onChange;
        //Alert.alert("3", JSON.stringify(this.state.strValueTotal));
        const { navigate } = this.props.navigation
        if (this.state.showIndicator) {
            return (
                <View style={styles.container}>
                    <View style={styles.containerLoading}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                </View>
            );


            // Testar passar o "DadosIngresso" daqui para o arquivo correto

        } else {
            return (
                <View style={styles.MainContainer}>

                    <View style={styles.margem}>

                        {
                            this.placesOutput()
                        }

                        {
                            this._render()
                        }



                        <View style={{ opacity: this.state.opacity }}>
                            <TouchableOpacity style={{ alignItems: 'center' }} >
                                <Botao cor="#fff" bdColor="#F15A24"
                                    bgColor="#F15A24" nome="Comprar" disable={this.state.enabled}
                                    // onPress={() => navigate('DadosIngresso', { name: 'DadosIngresso' })}
                                    onPress={() => this.teste()}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
        flex: 1,
        // justifyContent: 'space-between',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        // alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    containerLoading: {
        //padding: 20,
        flex: 1,
        justifyContent: 'center',
        // paddingHorizontal: 15,
        // paddingTop: 48,
        // paddingBottom: 20,color: '#fff'
        alignItems: 'center',
        backgroundColor: '#1c1c1c', //#026
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#1c1c1c'
    },
    TextStyle: {
        fontSize: 20,
        color: '#000',
        textAlign: 'left'
    },

    margem: {
        flex: 1
    },
    switchView: {
        width: 350,
        height: 450
    },
    texto: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        marginStart: 50,
        textAlign: "left",
    },
    textoTotal: {
        color: '#fff',
        fontSize: 18,
        marginStart: 20,
        textAlign: "left",
        fontFamily: "SFuidisplay-medium",
        alignItems: 'center'
    },
    textoValor: {
        color: '#F15A24',
        fontSize: 35,
        marginStart: 20,
        textAlign: "left",
        fontFamily: "SFuidisplay-medium",
        alignItems: 'center'
    },
    fotoRedonda: {
        height: 533.6,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -333.6,
        // marginVertical: 20,
        // borderWidth: 2,borderColor: '#FF9D00'
        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: {width: 10, height: 10 },
        // shadowRadius: 40,
    },
    filtroRedondo: {
        height: 533.6,
        width: 300,
        resizeMode: "stretch",
        justifyContent: 'center',
        borderRadius: 17.23,
        marginTop: -533.6,
        // marginVertical: 20,

        // elevation: 10,
        // shadowColor: '#202020',
        // shadowOffset: {width: 10, height: 10 },
        // shadowRadius: 40,
    },

    textoMes: {
        color: '#fff',
        fontFamily: "SFuidisplay-Black",
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoBarra: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 30,
        textAlign: "center",
        fontWeight: 'bold',
        justifyContent: 'center',
    },
    textoDia: {
        color: '#fff',
        fontFamily: "SFuidisplay-Medium",
        fontSize: 40,
        textAlign: "center",
        fontWeight: 'bold',
        marginBottom: -35,
        justifyContent: 'center',
    },
    textoNomeEvento: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 40,
        textAlign: "left",
    },
    textoLocalizacao: {
        color: '#fff',
        fontFamily: 'arial',
        fontSize: 20,
        textAlign: "left",
    },
    textoCidade: {
        color: '#fff',
        fontFamily: 'rockwell extra bold',
        fontSize: 15,
        textAlign: "left",
        fontWeight: 'bold',
    },
    textInputFormat: {
        color: '#fff',
        // fontFamily: 'rockwell extra bold',
        // fontSize: 15,
        // textAlign: "left",
        // fontWeight: 'bold',
    },

});

const mapStateToProps = (state) => {
    return {
        dadosIngressos: state.auth.dadosIngressos,
        strValueTotal: state.auth.strValueTotal
    }
};

const mapDispatchToProps = dispatch => {
    return {
        // add: (name) => {
        //   dispatch(addPlace(name))
        // }
        // add: (name) => {dispatch(addPlace(name))}
        onAddTicket: (name) => { dispatch(addTicket(name)) },
        onSelectPlace: key => dispatch(selectPlace(key)),
        //   onDeletePlace: () => dispatch(deletePlace()),
        //   onDeselectPlace: () => dispatch(deselectPlace())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Comprar);

    //export default connect(mapStateToProps, mapDispatchToProps)(App)
