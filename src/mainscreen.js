import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  StatusBar,
  BackHandler,
  TextInput,
  ToastAndroid
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Botao from './components/Botao';

console.disableYellowBox = true;

class TelaLogo extends Component {

  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: 'white',
  };

  constructor(props) {
    super(props);
    this.state = {};

  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }


  render() {

    const { navigate } = this.props.navigation;
    setTimeout(() => {
      navigate('PreLogin'); //this.props.navigation.navigate('Login')
    }, 2000);  //5000 milliseconds
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />

        <Image
          style={{ width: 89, height: 165, resizeMode: 'stretch', }}
          source={require('./img/logoFestei.png')}
        />


      </View>
    );
  }
}

class PreLogin extends Component {

  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props);
    this.state = {};

    this.novaConta = this.novaConta.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  // handleBackButton() {
  //     ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
  //     return true;
  // }

  //   logar(){
  //     this.props.navigation.navigate(Login);
  //   }

  novaConta() {
    alert("Nova Conta");
    this.props.navigation.navigate(screen1);
  }

  render() {
    const { navigate } = this.props.navigation
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />

        <Text style={styles.texto}>A{"\n"}FESTA{"\n"}CONTINUA{"\n"}EM{"\n"}SUAS{"\n"}MÃOS</Text>

        {/* <TextInput style=
        {{
          height: 40, borderColor: 'gray', borderWidth: 1, color : "#fff"
        }}
        placeholder="E-mail"
        underlineColor="#fff"
        // onChangeText={this.nomeForm}
      /> */}

        <Text style={styles.texto}>{"\n"}{"\n"}</Text>

        <TouchableOpacity >
          <Botao cor="#fff" bdColor="#F15A24"
            bgColor="#F15A24" nome="Login"
            onPress={() => navigate('Login', { name: 'Login' })} />
        </TouchableOpacity>

        <TouchableOpacity >
          <Botao cor="#fff" bdColor="#F15A24"
            bgColor="transparent" nome="Nova Conta"
            onPress={() => navigate('NovaConta', { name: 'NovaConta' })}
          />
        </TouchableOpacity>

      </View>
    );
  }
}

const Left = ({ onPress }) => (
  <TouchableOpacity onPress={onPress}>
      <Image
          source={require('./img/fundo_tela_evento.png')}
      />
  </TouchableOpacity>
);

const stackNavigatorConfigs = {
  initialRouteName: 'Dashboard',
  navigationOptions: {
      header: ({ goBack }) => ({
          left: <Left style={{color: "#000"}} />,
      }),
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1c1c1c',
    color: '#fff'
  },
  texto: {
    color: '#fff',
    fontFamily: 'rockwell extra bold',
    fontSize: 50,
    textAlign: "left",
    marginStart: 50,
    marginEnd: 50,
    fontWeight: 'bold',
  }

});

const Navegador = StackNavigator({
  TelaLogo: { screen: TelaLogo },
  PreLogin: { screen: PreLogin }
});

export default PreLogin;

