import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux';
import reducers from '../Reducers.js';
// import App from '../../App.js'
import App from '../screenmanager.js'

const store = createStore(reducers);

render(
 <Provider store={store}>
   <App />
 </Provider>,
 document.getElementById('root')
)

